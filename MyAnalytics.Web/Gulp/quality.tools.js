﻿"use strict";

var sequence = require("run-sequence");

var gulp = require("gulp");
var jshint = require("gulp-jshint");
var jscs = require("gulp-jscs");

var files = {
    js: {
        app: [
            "Content/Js/App/**/*.js"
        ]
    }
};

////////////////////////////////////////////////////////

function lint(reporter) {
    return gulp
        .src(files.js.app)
        .pipe(jshint())
        .pipe(jshint.reporter(reporter))
        .pipe(jshint.reporter("fail"));
}

function cs() {
    return gulp
        .src(files.js.app)
        .pipe(jscs());
}

gulp.task("jscs-dev", function() {
    return cs();
});

gulp.task("lint-dev", function() {
    return lint("default");
});

gulp.task("quality-tools-dev", ["lint-dev", "jscs-dev"], function(callback) {
    callback();
});