﻿g.namespace("ma");

(function($) {
    "use strict";

    ma.http.sendCommand = function(parameters) {
        var that = this;
        var deferred = new $.Deferred();

        var getParameters = $.extend({}, parameters, {
            data: parameters.getData
        });

        this.get(getParameters).done(function(command) {
            var postParameters = $.extend({}, parameters);

            if (!g.isNull(postParameters.postData)) {
                postParameters.data = $.extend(command, postParameters.postData);
            } else {
                postParameters.data = command;
            }

            var postPromise = that.post(postParameters);

            postPromise.done(function() {
                deferred.resolve();
            });

            postPromise.fail(function() {
                deferred.reject();
            });
        });

        return deferred.promise();
    };
})(jQuery);