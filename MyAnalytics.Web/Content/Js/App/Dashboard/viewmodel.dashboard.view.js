﻿g.namespace("ma");

(function($, http) {
    "use strict";

    ma.ViewDashboard = function(profileId) {
        g.ensure(profileId, "profileId").isNotNullOrEmpty();

        this.profileId = profileId;

        this.selectedStartDate = moment().subtract(20, "days").format("YYYY-MM-DD");
        this.selectedEndDate = moment().format("YYYY-MM-DD");
        this.previousStartDate = moment().subtract(40, "days").format("YYYY-MM-DD");
        this.previousEndDate = moment().subtract(20, "days").format("YYYY-MM-DD");
    };

    ma.ViewDashboard.prototype = $.extend({}, g.spa.viewModel, {
        onSynchronizeAnalyticsData: function() {
            var startTime = new Date().getTime();

            //var promise = http.sendCommand({
            //    url: "api/Synchronize",
            //    getData: {
            //        profileId: this.profileId
            //    }
            //});

            var promise = http.post({
                url: "api/Synchronize",
                data: {
                    profileId: this.profileId
                }
            });

            promise.done(function() {
                var endTime = new Date().getTime();

                console.log("Synchronization completed in: {0}ms".format(endTime - startTime));
            });
        }
    });
})(jQuery,
   ma.http);