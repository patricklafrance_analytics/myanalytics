﻿(function($) {
    "use strict";

    ko.bindingHandlers["dashbooard.lineChart"] = {
        init: function(element, valueAccessor) {
            var canvaContext = $(element).get(0).getContext("2d");
            var chartValues = ko.utils.unwrapObservable(valueAccessor());

            var data = {
                labels: [],
                datasets: [
                    {
                        fillColor: "#eaf8ee",
			            strokeColor: "#35b958",
                        data: chartValues
                    }
                ]
            };

            var options = {
                showScale: false,
                showTooltips: false,
                pointDot: false,
                datasetStrokeWidth : 4,
                bezierCurve: false
            };

            var chart = new Chart(canvaContext).Line(data, options);

            ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                chart.destroy();
            });
        }
    };
})(jQuery);