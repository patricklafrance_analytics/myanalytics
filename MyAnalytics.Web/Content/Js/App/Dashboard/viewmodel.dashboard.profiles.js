﻿g.namespace("ma");

(function($, http) {
    "use strict";

    ma.ProfilesViewModel = function() {
        this.profiles = ko.observable();
    };

    ma.ProfilesViewModel.prototype = $.extend({}, g.spa.viewModel, {
        _beforeBind: function() {
            this._fetchProfiles();
        },

        _fetchProfiles: function() {
            var that = this;

            var promise = http.get({
                url: "api/ViewProfiles"
            });

            promise.done(function(data) {
                that.profiles(data);
            });
        }
    });
})(jQuery,
   ma.http);