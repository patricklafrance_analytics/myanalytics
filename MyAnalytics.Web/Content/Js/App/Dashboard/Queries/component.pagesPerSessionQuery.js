﻿g.namespace("ma");

(function(http) {
    "use strict";

    var PagesPerSessionQueryComponent = function(params) {
        g.ensure(params, "params").isNotNull();
        g.ensure(params.profileId, "params.profileId").isNotNullOrEmpty();
        g.ensure(params.selectedStartDate, "params.selectedStartDate").isNotNullOrEmpty();
        g.ensure(params.selectedEndDate, "params.selectedEndDate").isNotNullOrEmpty();
        g.ensure(params.previousStartDate, "params.previousStartDate").isNotNullOrEmpty();
        g.ensure(params.previousEndDate, "params.previousEndDate").isNotNullOrEmpty();

        var that = this;

        this.queryResult = ko.observable();

        setTimeout(function() {
            var promise = http.get({
                url: "api/PagesPerSessionQuery",
                data: {
                    profileId: params.profileId,
                    selectedStartDate: params.selectedStartDate,
                    selectedEndDate: params.selectedEndDate,
                    previousStartDate: params.previousStartDate,
                    previousEndDate: params.previousEndDate
                },
                cache: true
            });

            promise.done(function(data) {
                that.queryResult(data);
            });
        }, 0);
    };

    // ---------------------------------

    ko.components.register("pages-per-session-query", {
        viewModel: PagesPerSessionQueryComponent,
        template: {
            element: "pages-per-session-query-component"
        }
    });
})(ma.http);