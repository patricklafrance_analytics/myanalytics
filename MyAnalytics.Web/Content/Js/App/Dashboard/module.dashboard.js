﻿g.namespace("ma");

(function() {
    "use strict";

    ma.dashboardModule = {
        activate: function(addRoute) {
            addRoute({
                name: "profiles",
                url: "#/profiles",
                viewUrl: "Dashboard/Profiles",
                viewModelBinder: new g.spa.SimpleViewModelBinder(function() {
                    return new ma.ProfilesViewModel();
                })
            });

            g.spa.shell.setApplicationRootRoute({
                module: "dashboard",
                name: "profiles"
            });

            addRoute({
                name: "view-dashboard",
                url: "#/dashboards/:profileId",
                viewUrl: "Dashboard/ViewDashboard",
                viewModelBinder: new g.spa.SimpleViewModelBinder(function(routeParameters) {
                    return new ma.ViewDashboard(routeParameters.profileId);
                })
            });
        }
    };
})();