﻿g.namespace("ma");

(function($) {
    "use strict";

    ma.http = {
        get: function(parameters) {
            g.ensure(parameters, "parameters").isNotNull();
            g.ensure(parameters.url, "parameters.url").isNotNullOrEmpty();

            var request = $.extend({}, parameters, {
                type: "GET",
                contentType: "application/json",
                cache: g.isNull(parameters.cache) ? false : parameters.cache
            });

            return $.ajax(request);
        },

        post: function(parameters) {
            g.ensure(parameters, "parameters").isNotNull();
            g.ensure(parameters.url, "parameters.url").isNotNullOrEmpty();

            var request = $.extend({}, parameters, {
                type: "POST"
            });

            return $.ajax(request);
        }
    };
})(jQuery);