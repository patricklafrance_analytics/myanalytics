﻿g.namespace("ma");

(function($, shell) {
    "use strict";

    ma.application = {
        start: function() {
            g.debug = true;

            shell.start(ma, $("[data-role=\"spa-container\"]")[0]);
        }
    };
})(jQuery,
   g.spa.shell);