﻿g.namespace("ma");

(function() {
    "use strict";

    ma.applicationModule = {
        activate: function(addRoute) {
            addRoute({
                name: "404",
                url: "#/404",
                viewUrl: "Application/NotFound"
            });
        }
    };
})();