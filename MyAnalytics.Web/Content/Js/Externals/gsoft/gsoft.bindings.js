// NOTE: The function documentation is based on the DOJO documentation: 
// https://dojotoolkit.org/reference-guide/1.7/util/doctools/markup.html

(function() {
    "use strict";

    (function() {
        // summary:
        //          Set the "href" attribute of the binding element.
        ko.bindingHandlers.href = {
            init: function(element, valueAccessor) {
                var value = valueAccessor();
                
                return ko.applyBindingsToNode(element, {
                    attr: {
                        href: value
                    }
                });
            }
        };
    })();

    (function() {
        // summary:
        //          Set the "src" attribute of the binding element.
        ko.bindingHandlers.src = {
            init: function(element, valueAccessor) {
                var value = valueAccessor();
                
                return ko.applyBindingsToNode(element, {
                    attr: {
                        src: value
                    }
                });
            }
        };
    })();

    (function() {
        // summary:
        //          Hide the binding element when the value is true.
        ko.bindingHandlers.hidden = {
            init: function(element, valueAccessor) {
                var value = valueAccessor();
                
                return ko.applyBindingsToNode(element, {
                    visible: ko.computed(function() {
                        return !ko.utils.unwrapObservable(value);
                    })
                });
            }
        };
    })();

    (function() {
        var nativeClickInit = ko.bindingHandlers.click.init;
        
        // summary:
        //          Enhance the native "click" binding by adding the ability to disable the click with the "enable" or "disable" bindings.
        //          The "click" binding is only enhanced if the binding element is a "A" element.
        ko.bindingHandlers.click.init = function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var valueAccessorProxy = null;
    
            if (element.tagName === "A") {
                if (allBindingsAccessor.has("enable") || allBindingsAccessor.has("disable")) {
                    valueAccessorProxy = function() {
                        return function() {
                            if (allBindingsAccessor.has("enable")) {
                                var enableBinding = allBindingsAccessor.get("enable");
    
                                if (!ko.utils.unwrapObservable(enableBinding)) {
                                    return false;
                                }
                            } else if (allBindingsAccessor.has("disable")) {
                                var disableBinding = allBindingsAccessor.get("disable");
    
                                if (ko.utils.unwrapObservable(disableBinding)) {
                                    return false;
                                }
                            }
    
                            return valueAccessor().apply(this, arguments);
                        };
                    };
                }
            }
    
            return nativeClickInit(element, (valueAccessorProxy || valueAccessor), allBindingsAccessor, viewModel, bindingContext);
        };
    })();

    (function() {
        // summary:
        //          Add a CSS class to the binding element when he is disabled. The binding element
        //          can be disabled with the "enable" or "disable" bindings.
        var binding = ko.bindingHandlers.disabledCssClass = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var disabledCssClass = valueAccessor();
                
                if (ko.isObservable(disabledCssClass)) {
                    throw new g.ArgumentError("The \"disabledCssClass\" binding value cannot be an observable.");
                }
                
                var bindingsToApply = {
                    css: {}
                };
                
                bindingsToApply.css[disabledCssClass] = ko.computed(function() {
                    return binding._isDisabled(allBindingsAccessor);
                });
                
                return ko.applyBindingsToNode(element, bindingsToApply);
            },
            
            _isDisabled: function(allBindingsAccessor) {
                if (allBindingsAccessor.has("enable")) {
                    var enableBinding = allBindingsAccessor.get("enable");
    
                    return !ko.utils.unwrapObservable(enableBinding);
                }
                
                if (allBindingsAccessor.has("disable")) {
                    var disableBinding = allBindingsAccessor.get("disable");
    
                    return ko.utils.unwrapObservable(disableBinding);
                }
                    
                throw new g.ArgumentError("The \"disabledCssClass\" binding require that you specify the \"enable\" or \"disable\" binding.");
            }
        };
    })();

    (function() {
        // summary:
        //          Modify the inner HTML of the binding element when he is disabled. The binding element
        //          can be disabled with the "enable" or "disable" bindings.
        var binding = ko.bindingHandlers.disabledText = {
            init: function(element, valueAccessor, allBindingsAccessor) {
                var disabledText = valueAccessor();
                
                if (ko.isObservable(disabledText)) {
                    throw new g.ArgumentError("The \"disabledText\" binding value cannot be an observable.");
                }
                
                return ko.applyBindingsToNode(element, {
                    text: ko.computed(function()  {
                        if (binding._isDisabled(allBindingsAccessor)) {
                            return disabledText;
                        }
                        
                        return binding._getEnabledText(element, allBindingsAccessor);
                    })
                });
            },
            
            _isDisabled: function(allBindingsAccessor) {
                if (allBindingsAccessor.has("enable")) {
                    var enableBinding = allBindingsAccessor.get("enable");
    
                    return !ko.utils.unwrapObservable(enableBinding);
                } 
                
                if (allBindingsAccessor.has("disable")) {
                    var disableBinding = allBindingsAccessor.get("disable");
    
                    return ko.utils.unwrapObservable(disableBinding);
                }
                    
                throw new g.ArgumentError("The \"disableText\" binding require that you specify the \"enable\" or \"disable\" binding.");
            },
            
            _getEnabledText: function(element, allBindingsAccessor) {
                var enabledText = "";
                
                if (allBindingsAccessor.has("text")) {
                    var textBinding = allBindingsAccessor.get("text");
                    
                    enabledText = ko.utils.unwrapObservable(textBinding);
                } else {
                    enabledText = ko.utils.domData.get(element, "enabledText");
                    
                    if (g.isNull(enabledText)) {
                        enabledText = element.innerHTML;
                        ko.utils.domData.set(element, "enabledText", enabledText);
                    }
                }
                
                return enabledText;
            }
        };
    })();

    (function() {
        // summary:
        //          Value binding that support options with boolean values as string.
        ko.bindingHandlers.optionsBooleanValue = {
            init: function(element, valueAccessor) {
                var value = valueAccessor();
    
                if (!ko.isObservable(value)) {
                    throw new g.ArgumentError("The \"optionsBooleanValue\" binding value must be an observable.");
                }
                
                return ko.applyBindingsToNode(element, { 
                    value: ko.computed({
                        read: function() {
                            var unwrappedValue = value();
    
                            if (g.isNull(unwrappedValue)) {
                                return null;
                            }
    
                            return unwrappedValue.toString();
                        },
                        write: function(newValue) {
                            if (g.isNull(newValue)) {
                                value(null);
                            } else {
                                value(newValue === "true");
                            }
                        }                   
                    }) 
                });
            }
        };
    })();

})();