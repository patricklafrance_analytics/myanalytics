// NOTE: The function documentation is based on the DOJO documentation: 
// https://dojotoolkit.org/reference-guide/1.7/util/doctools/markup.html

(function(factory) {
    "use strict";
    
    if (typeof require === "function" && typeof exports === "object" && typeof module === "object") {
        var instance = factory();

        // CommonJS.
        if (typeof exports === "object") {
            /* jshint -W020 */
            exports = instance;
            /* jshint +W020 */
        }
        
        // Node.js and Browserify.
        if (typeof module === "object") {
            module.exports = instance;
        }
    }
    else if (typeof define === "function" && define.amd) {
        // AMD.
        define("gsoft", [], factory);
    } 
    else {
        var target = null;
        
        if (typeof global === "object") {
            target = global;
        } else {
            target = window;
        }

        // Expose globally.
        target.gsoft = factory();
        
        // Map over the actual "g" in case of overwrite.
        var _g = target.g;

        // Expose as a "g".
        target.g = target.gsoft;

        // summary:
        //         Rollback the "g" alias and restore the old value to target.g.
        // description:
        //        This function is only available if the library is not loaded throught a module
        //        system since the library will not be exposed globally.
        // returns:
        //         The library.
        target.gsoft.noConflict = function() {
            if (target.g === target.gsoft) {
                target.g = _g;
            }

            return target.gsoft;
        };
    }
})(

function() {
    "use strict";
    
    var gsoft = {};
    
    // Namespace for private functions shared accross modules.
    var _ = gsoft._ = {};

    // Shared privates
    // ---------------------------------
    
    (function() {
        _.isBrowser = function() {
            return typeof document === "object";
        };
    })();

    // Metadata
    // ---------------------------------
    
    (function() {
        gsoft.version = "0.0.0";
        
        // When true, additional informations will be output to the console and additional function will be visible 
        // on the library objects.
        gsoft.debug = false;
        
        if (_.isBrowser()) {
            // When true, the shims are always use instead of the native functions. This is usefull to test all the shims in any browsers.
            // To active add an attribute "data-force-shims" with the value "true" to the script tag of this library.
            var scriptElement = document.querySelector("script[src*=\"gsoft.js\"][data-force-shims=\"true\"]");
    
            if (scriptElement) {
                gsoft.forceShims = true;
            }
        }
    })();

    // Utils
    // ---------------------------------
    
    (function(undefined) {
        var utils = gsoft.utils = {
            // summary:
            //         Determines wether @value is defined.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isDefined: function(value) {
                return !this.isUndefined(value);
            },
            
            // summary:
            //         Determines wether @value is undefined.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isUndefined: function(value) {
                return value === undefined;
            },
            
            // summary:
            //         Determines wether @value is not null and not undefined.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isNotNull: function(value) {
                return !this.isNull(value);
            },
                    
            // summary:
            //         Determines wether @value is null or undefined.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isNull: function(value) {
                return this.isUndefined(value) || value === null;
            },
                
            // summary:
            //         Determines wether @value is null, undefined or an empty string.
            // value: String
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isNullOrEmpty: function(value) {
                return this.isNull(value) || value === "";
            },
                
            // summary:
            //         Determines wether @value is a String object.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isString: function(value) {
                return Object.prototype.toString.call(value) === "[object String]";
            },
            
            // summary:
            //         Determines wether @value is a Number object.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isNumber: function(value) {
                return Object.prototype.toString.call(value) === "[object Number]";
            },
            
            // summary:
            //         Determines wether @value is a Boolean object.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isBoolean: function(value) {
                return Object.prototype.toString.call(value) === "[object Boolean]";
            },
    
            // summary:
            //         Determines wether @value is a Date object.
            // value: Object
            //         The value to perform the check against.  
            // returns:
            //         A boolean.
            isDate: function(value) {
                return Object.prototype.toString.call(value) === "[object Date]";
            },
            
            // summary:
            //         Determines wether @value is a RegExp object.
            // value: Object
            //         The value to perform the check against.  
            // returns:
            //         A boolean.
            isRegExp: function(value) {
                return Object.prototype.toString.call(value) === "[object RegExp]";
            },
    
            // summary:
            //         Determines wether @value is a Function object.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isFunction: function(value) {
                return Object.prototype.toString.call(value) === "[object Function]";
            },
            
            // summary:
            //         Determines wether @value is an Array object.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isArray: function(value) {
                return Object.prototype.toString.call(value) === "[object Array]";
            },
            
            // summary:
            //         Determines wether @value is an object.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            isObject: function(value) {
                if (this.isFunction(value) || this.isArray(value) || this.isDate(value) || this.isRegExp(value) || this.isNumber(value)) {
                    return false;
                }
                
                return value === Object(value);
            },
            
            // ---------------------------------
            
            // summary:
            //         Removes the item at position @index from the @array.
            // array: Array
            //         The array to remove an item from.
            // index: Integer
            //         The index of the item to remove from the array.
            arrayRemoveAt: function(array, index) {
                var removedItems  = array.splice(index, 1);
    
                return removedItems.length > 0 ? removedItems[0] : null;
            },
            
            // ---------------------------------
            
            // summary:
            //        Iterate over every properties of an object.
            // obj: Object
            //         The object to iterates over.
            // action: Function
            //         The function to call for every properties of the object.
            objectForEach: function(obj, action, context) {
                for (var propertyKey in obj) {
                    if (obj.hasOwnProperty(propertyKey)) {
                        action.call(context, obj[propertyKey], propertyKey);
                    }
                }
            },
            
            // ---------------------------------
            
            // summary:
            //         Clone a plain object. 
            // description:
            //        A plain object is an object created by using the object literal { } notation.
            // obj: Object
            //         The object to clone.
            // returns:
            //         A plain object.
            clonePlainObject: function(obj) {
                return JSON.parse(JSON.stringify(obj));
            },
                
            // summary:
            //         Clone an array.
            // array: Array
            //         The array to clone.
            // returns:
            //         An array.
            cloneArray: function(array) {
                return array.slice(0);
            },
            
            // ---------------------------------
            
            // summary:
            //         Write a message to the console if we are in debug.
            // message: String
            //         The message to write.
            trace: function(message) {
                if (gsoft.debug === true && !this.isNull(console.log)) {
                    console.log(message);
                }
            }
        };
        
        if (!utils.isUndefined(Array.isArray) && !gsoft.forceShims) {
            // The assertion is done with the native JavaScript Array object which support this feature 
            // since the JavaScript 1.8.5 (ECMAScript 5) specifications.
            utils.isArray = function(value) {
                return Array.isArray(value);
            };
        }
        
        if (_.isBrowser()) {
            // summary:
            //         Determines wether @value is a DOM element.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            utils.isDomElement = function(value) {
                try {
                    if (value.ownerDocument.documentElement.tagName.toLowerCase() === "html") {
                        return true;
                    }
                }
                catch (e) {
                }
    
                return false;
            };
        }
        
        // Define aliases for the most frequently used utils functions to allow a lighter syntax.
        gsoft.isDefined = utils.isDefined;
        gsoft.isUndefined = utils.isUndefined;
        gsoft.isNull = utils.isNull;
        gsoft.isNotNull = utils.isNotNull;
        gsoft.isNullOrEmpty = utils.isNullOrEmpty;
    })();

    // String
    // ---------------------------------
    
    (function(utils) {
        // summary:
        //         Format the string using the specified pattern and values.
        // [0..]: String
        //        Parts of formatted string.
        // returns:
        //         A string.
        // example:
        //         "Hello {0} from {1}!".format("John Doe", "GSoft");
        String.prototype.format = function() {
            var args = arguments;
    
            return this.replace(/\{(\d+)\}/g, function(m, n) {
                return args[n];
            });
        };
    
        if (utils.isUndefined(String.prototype.endsWith) || gsoft.forceShims) {
            String.prototype.endsWith = function(value, position) {
                if (utils.isNull(position)) {
                    position = this.length;
                }
    
                return this.lastIndexOf(value) === (position - value.length);
            };
        }
    
        if (utils.isUndefined(String.prototype.startsWith) || gsoft.forceShims) {
            String.prototype.startsWith = function(value, position) {
                if (utils.isNull(position)) {
                    position = 0;
                }
    
                return this.indexOf(value, position) === position;
            };
        }
    
        if (utils.isUndefined(String.prototype.contains) || gsoft.forceShims) {
            String.prototype.contains = function(value, position) {
                return this.indexOf(value, position) !== -1;
            };
        }
    })(gsoft.utils);

    // Array
    // ---------------------------------
    
    (function(utils) {
        if (utils.isUndefined(Array.isArray) || gsoft.forceShims) {
            Array.isArray = function(value) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                return utils.isArray(value);
            };
        }
        
        if (utils.isUndefined(Array.prototype.forEach) || gsoft.forceShims) {
            Array.prototype.forEach = function(callback, context) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                if (!utils.isFunction(callback)) {
                    throw new TypeError("{0} is not a function".format(callback));
                }
                
                for (var i = 0, max = this.length; i < max; i += 1) {
                    callback.call(context, this[i], i, this);
                }
            };
        }
        
        if (utils.isUndefined(Array.prototype.map) || gsoft.forceShims) {
            Array.prototype.map = function(mapper, context) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                var newArray = [];
                
                for (var i = 0, max = this.length; i < max; i += 1) {
                    newArray[i] = mapper.call(context, this[i], i, this);
                }
                
                return newArray;
            };
        }
        
        if (utils.isUndefined(Array.prototype.filter) || gsoft.forceShims) {
            Array.prototype.filter = function(predicate, context) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                if (!utils.isFunction(predicate)) {
                    throw new TypeError("{0} is not a function".format(predicate));
                }
                
                var newArray = [];
                
                for (var i = 0, max = this.length; i < max; i += 1) {
                    if (predicate.call(context, this[i], i, this)) {
                        newArray.push(this[i]);
                    }
                }
                
                return newArray;
            };
        }
        
        if (utils.isUndefined(Array.prototype.every) || gsoft.forceShims) {
            Array.prototype.every = function(predicate, context) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                if (!utils.isFunction(predicate)) {
                    throw new TypeError("{0} is not a function".format(predicate));
                }
                
                for (var i = 0, max = this.length; i < max; i += 1) {
                    if (predicate.call(context, this[i], i, this) !== true) {
                        return false;
                    }
                }
                
                return true;
            };
        }
        
        if (utils.isUndefined(Array.prototype.indexOf) || gsoft.forceShims) {
            Array.prototype.indexOf = function(value, fromIndex) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                var length = this.length;
                
                if (length === 0) {
                    return -1;
                }
                
                var startAt = fromIndex || 0;
                
                if (startAt > length) {
                    return -1;
                }
                
                for (var i = startAt; i < length; i += 1) {
                    if (this[i] === value) {
                        return i;
                    }
                }
                
                return -1;
            };
        }
        
        if (utils.isUndefined(Array.prototype.some) || gsoft.forceShims) {
            Array.prototype.some = function(predicate, context) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                if (!utils.isFunction(predicate)) {
                    throw new TypeError("{0} is not a function".format(predicate));
                }
                
                for (var i = 0, max = this.length; i < max; i += 1) {
                    if (predicate.call(context, this[i], i, this) === true) {
                        return true;
                    }
                }
                
                return false;
            };
        }
        
        if (utils.isUndefined(Array.prototype.reduce) ||  gsoft.forceShims) {
            Array.prototype.reduce = function(action, initialValue) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                if (!utils.isFunction(action)) {
                    throw new TypeError("{0} is not a function".format(action));
                }
                
                var index = 0;
                var accumulator = initialValue;
                
                if (utils.isUndefined(accumulator)) {
                    if (this.length === 0) {
                        throw new TypeError("Reduce of an empty array with no initial value");
                    }
                    
                    accumulator = this[index];
                    index += 1;
                }
                
                for (var max = this.length; index < max; index += 1) {
                    accumulator = action(accumulator, this[index], index, this);
                }
                
                return accumulator;
            };
        }
        
        if (utils.isUndefined(Array.prototype.reduceRight) ||  gsoft.forceShims) {
            Array.prototype.reduceRight = function(action, initialValue) {
                if (utils.isNull(this)) {
                    throw new TypeError("Array is null or undefined");
                }
                
                if (!utils.isFunction(action)) {
                    throw new TypeError("{0} is not a function".format(action));
                }
                
                var index = this.length - 1;
                var accumulator = initialValue;
                
                if (utils.isUndefined(accumulator)) {
                    if (this.length === 0) {
                        throw new TypeError("Reduce of an empty array with no initial value");
                    }
                    
                    accumulator = this[index];
                    index -= 1;
                }
                
                for (; index >= 0; index -= 1) {
                    accumulator = action(accumulator, this[index], index, this);
                }
                
                return accumulator;
            };
        }
    })(gsoft.utils);

    // Error
    // ---------------------------------
    
    (function(utils) {
        // summary:
        //         JavaScript error of type ArgumentError.
        // errorMessage: String
        //         An error message that will be part of the exception message.
        gsoft.ArgumentError = function(errorMessage) {
            this.message = utils.isNullOrEmpty(errorMessage) ? "ArgumentError" : "ArgumentError: \"{0}\"".format(errorMessage);
        };
    
        gsoft.ArgumentError.prototype = new Error();
            
        // summary:
        //         JavaScript error of type ArgumentNullError.
        // parameterName: String
        //         An error message that will be part of the exception message if
        //         @errorMessage is not specified.
        // errorMessage: String
        //         An error message that will be part of the exception message.
        gsoft.ArgumentNullError = function(parameterName, errorMessage) {
            this.parameterName = parameterName;
            
            if (utils.isNullOrEmpty(parameterName) && utils.isNullOrEmpty(errorMessage)) {
                this.message = "ArgumentNullError";
            }
            else if (utils.isNullOrEmpty(errorMessage)) {
                this.message = "ArgumentNullError: \"{0}\" cannot be null".format(parameterName);
            }
            else {
                this.message = "ArgumentNullError: \"{0}\"".format(errorMessage);
            }
        };
    
        gsoft.ArgumentNullError.prototype = new Error();
    
        // summary:
        //         JavaScript error of type InvalidOperationError.
        // errorMessage: String
        //         An error message that will be part of the exception message.
        gsoft.InvalidOperationError = function(errorMessage) {
            this.message = utils.isNullOrEmpty(errorMessage) ? "InvalidOperationError" : "InvalidOperationError: \"{0}\"".format(errorMessage);
        };
    
        gsoft.InvalidOperationError.prototype = new Error();
    })(gsoft.utils);

    // Ensure
    // ---------------------------------
    
    (function(utils) {
        // Caching the unshift array prototype function give a small performance boost.
        // This is used when manipulating function arguments. Since arguments is
        // an "Array like" it doesn't implements the native array manipulation functions.
        var unshift = Array.prototype.unshift;
    
        // summary:
        //         Ensure that a @parameter respect the specified assertions.
        // description:
        //         Ensure that a @parameter respect the specified assertions. Every assertions returns
        //         an object literal that contains all the assertions to allow chaining. Custom assertions
        //         can be added by extending "gsoft.ensure.assertions".
        // parameter: Object
        //         The parameter to verify.
        // parameterName: String
        //         An optional name of the parameter to verify.
        // context: String
        //         An optional identifier providing more information about the context of the call to ensure.
        // returns:
        //         An object that contains all the assertions functions.
        // example:
        //         gsoft.ensure(parameter, "Optional parameter name", "Optional context").isNotNull("Optional specific message").isNotEmpty();
        var ensure = gsoft.ensure = function(parameter, parameterName, context) {
            var assertions = {};
    
            var getAssertionProxy = function(property) {
                return function() {
                    unshift.apply(arguments, [parameter, parameterName, context]);
    
                    return property.apply(assertions, arguments);
                };
            };
            
            // Wrap all the assertions to append the default arguments to the function arguments.
            utils.objectForEach(gsoft.ensure.assertions, function(assertion, assertionKey) {
                assertions[assertionKey] = getAssertionProxy(assertion);
            });
    
            return assertions;
        };
            
        // summary:
        //         Build an error message.
        // description:
        //         Build an error message. 
        //         For the default message format:
        //              {0} is the context of the call to ensure
        //              {1} is the parameter name
        var getMessage = ensure._getMessage = function(assertionMessage, defaultMessageTemplate, parameterName, context) {
            var message = assertionMessage;
    
            if (utils.isNullOrEmpty(message)) {
                message = defaultMessageTemplate.format(
                    utils.isNullOrEmpty(context) ? "" : "{0} - ".format(context),
                    utils.isNullOrEmpty(parameterName) ? "Parameter" : parameterName);
            }
    
            return message;
        };
    
        ensure.assertions = {
            // summary:
            //         Assert that a @parameter is not null.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isNotNull: function(parameter, parameterName, context, assertionMessage) {
                if (utils.isNull(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} cannot be null.", parameterName, context);
                    throw new gsoft.ArgumentNullError(parameterName, message);
                }
    
                return this;
            },
    
            // summary:
            //         Assert that a @parameter is not empty.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isNotEmpty: function(parameter, parameterName, context, assertionMessage) {
                var isValid = false;
    
                if (utils.isArray(parameter)) {
                    isValid = parameter.length > 0;
                } else {
                    isValid = parameter !== "";
                }
    
                if (!isValid) {
                    var message = getMessage(assertionMessage, "{0}{1} cannot be empty.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
    
                return this;
            },
    
            // summary:
            //         Assert that a @parameter is not null or an empty string.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isNotNullOrEmpty: function(parameter, parameterName, context, assertionMessage) {
                var message = "";
    
                if (utils.isNull(parameter)) {
                    message = getMessage(assertionMessage, "{0}{1} cannot be null.", parameterName, context);
                    throw new gsoft.ArgumentNullError(parameterName, message);
                }
                    
                if (parameter === "") {
                    message = getMessage(assertionMessage, "{0}{1} cannot be empty.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
    
                return this;
            },
    
            // summary:
            //         Assert that a @parameter is a function.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isFunction: function(parameter, parameterName, context, assertionMessage) {
                if (!utils.isFunction(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} must be a function.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
                    
                return this;
            },
            
            // summary:
            //         Assert that a @parameter is a date.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isDate: function(parameter, parameterName, context, assertionMessage) {
                if (!utils.isDate(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} must be a date.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
                    
                return this;
            },
            
            // summary:
            //         Assert that a @parameter is a number.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isNumber: function(parameter, parameterName, context, assertionMessage) {
                if (!utils.isNumber(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} must be a number.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
                    
                return this;
            },
    
            // summary:
            //         Assert that a @parameter is an array.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isArray: function(parameter, parameterName, context, assertionMessage) {
                if (!utils.isArray(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} must be an array.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
    
                return this;
            },
    
            // summary:
            //         Assert that a @parameter is an object.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isObject: function(parameter, parameterName, context, assertionMessage) {
                if (!utils.isObject(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} must be an object.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
    
                return this;
            },
            
            // summary:
            //         Assert that a @parameter pass the specified evaluation.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // evaluator: Function
            //         A function that evaluate the @parameter.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isTrue: function(parameter, parameterName, context, evaluator, assertionMessage) {
                if (!utils.isFunction(evaluator) || !evaluator(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} is invalid.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
    
                return this;
            },
    
            // summary:
            //         Assert that a @parameter is a DOM element.
            // parameter: Object
            //         The parameter to verify.
            // parameterName: String
            //         An optional name of the parameter to verify.
            // context: String
            //         An optional identifier providing more information about the context of the call to ensure.
            // assertionMessage: String
            //         An optional message used when an assertion fail.
            // returns:
            //         An object that contains all the assertions functions.
            isDomElement: function(parameter, parameterName, context, assertionMessage) {
                if (!gsoft.utils.isDomElement(parameter)) {
                    var message = getMessage(assertionMessage, "{0}{1} must be a DOM element.", parameterName, context);
                    throw new gsoft.ArgumentError(message);
                }
    
                return this;
            }
        };
    })(gsoft.utils);

    // Namespace
    // ---------------------------------
    
    (function() {
        // summary:
        //          Create one or more new namespaces or extend existings. 
        // arguments:
        //          An infinite number of string representing the namespace to create. A namespace
        //          level is represent by a dot ('.').
        // example:
        //          gsoft.namespace("gsoft.administration")
        //          gsoft.namespace("gsoft.hr", "gsoft.hr.team", ...)
        gsoft.namespace = function() {
            var o = null;
            var i = null;
            var j = null;
            var d = null;
    
            for (i = 0; i < arguments.length; i = i + 1) {
                d = arguments[i].split(".");
                o = window[d[0]] = window[d[0]] || {};
    
                for (j = 1; j < d.length; j = j + 1) {
                    o[d[j]] = o[d[j]] || {};
                    o = o[d[j]];
                }
            }
    
            return o;
        };
    })();

    // Mediator
    // ---------------------------------
    
    (function(utils, ensure) {
        // summary:
        //          Publish / subscribe message pattern implementation.
        // example:
        //          gsoft.mediator.subscribe("channel");
        //          gsoft.mediator.publish("channel", "value");
        gsoft.mediator = {
            DEFAULT_PRIORITY: 4,
            
            _registry: {},
            
            // summary:
            //          Subscribe to a @channel. Everytime an event is published to that @channel 
            //          the @callback will be called with the event value.
            // channel: String
            //          A channel to suscribe to.
            // callback: Function
            //          An handler to call when an event is published on the @channel.
            // options: Object
            //          An optional object with options. Options can be:
            //             - priority: The subscriber priority. A subscriber with an higher will be notified
            //               before a subscriber with a lower priority.
            // returns:
            //         An object that contains informations about the subscription and a function to 
            //         unsuscribe from the @channel.
            subscribe: function(channel, callback /* options */) {
                ensure(channel, "channel", "Mediator.subscribe").isNotNullOrEmpty();
                ensure(callback, "callback", "Mediator.subscribe").isFunction();
                
                var that = this;
                var options = arguments[2];
                
                var subscription = {
                    callback: callback,
                    priority: this.DEFAULT_PRIORITY
                };
                
                if (utils.isObject(options)) {
                    subscription.priority = options.priority || subscription.priority;
                }
                
                if (!utils.isArray(this._registry[channel])) {
                    this._registry[channel] = [];
                }
    
                this._registry[channel].push(subscription);
                
                // Order subscriptions by priority (higher is better).
                this._registry[channel].sort(function(a, b) {
                    return b.priority - a.priority;
                });
    
                return {
                    channel: channel,
                    unsubscribe: function() {
                        that.unsubscribe(channel, callback);
                    }
                };
            },
            
            // summary:
            //          Subscribe to a @channel for a single event. When the first event is published, the subscriber
            //          will be automatically unsubscribed from the @channel.
            // channel: String
            //          A channel to suscribe to.
            // callback: Function
            //          An handler to call when an event is published on the @channel.
            // options: Object
            //          An optional object with options. Options can be:
            //             - priority: The subscriber priority. A subscriber with an higher will be notified
            //               before a subscriber with a lower priority.
            // returns:
            //         An object that contains informations about the subscription and a function to 
            //         unsuscribe from the @channel.
            subscribeOnce: function(channel, callback /* options */) {
                ensure(channel, "channel", "Mediator.subscribeOnce").isNotNullOrEmpty();
                ensure(callback, "callback", "Mediator.subscribeOnce").isFunction();
                
                var that = this;
                
                var proxy = function() {
                    that.unsubscribe(channel, proxy);    
                    callback.apply(this, arguments);
                };
                
                return this.subscribe(channel, proxy, arguments[2]);
            },
            
            // summary:
            //          unsubscribe from a @channel.
            // channel: String
            //          A channel to unsubscribe from.
            // callback: Function
            //          A callback function that was specified when the subscription to the @channel
            //          was made.
            unsubscribe: function(channel, callback) {
                ensure(channel, "channel", "Mediator.unsuscribe").isNotNullOrEmpty();
                ensure(callback, "callback", "Mediator.unsuscribe").isFunction();
                
                var registry = this._registry[channel];
    
                if (utils.isArray(registry)) {
                    registry.some(function(subscription, index) {
                        if (subscription.callback === callback) {
                            utils.arrayRemoveAt(registry, index);
                            
                            return true;
                        }
                        
                        return false;
                    });
                }
            },
            
            // summary:
            //          Publish a @value to a @channel.
            // channel: String
            //          A channel to publish to.
            // value(s): Object
            //          The value(s) to publish on the @channel.
            publish: function(channel /* [value1, value2, ...] */) {
                ensure(channel, "channel", "Mediator.publish").isNotNullOrEmpty();
                
                var registry = this._registry[channel];
    
                if (utils.isArray(registry)) {
                    // Cloning the array give the possibility to add or remove a subscription in a publish callback.
                    var subscriptions  = utils.cloneArray(registry);
                    
                    // Retrieve all the values to publish.
                    var values = Array.prototype.slice.call(arguments, 1);
                    
                    subscriptions.forEach(function(subscription) {
                        subscription.callback.apply(this, values);
                    }, this);
                }
            }
        };
    })(gsoft.utils,
       gsoft.ensure);

    return gsoft;
});