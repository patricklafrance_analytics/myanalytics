// NOTE: The function documentation is based on the DOJO documentation: 
// https://dojotoolkit.org/reference-guide/1.7/util/doctools/markup.html

(function() {
    "use strict";

    gsoft.spa = {};

    // Constants
    // --------------------------------
    
    (function() {
        gsoft.spa.Component = {
            Router: 0,
            ModuleActivator: 1,
            ViewProvider: 2,
            SimpleViewModelBinder: 3,
            CompositeViewModelBinder: 4,
            Shell: 5
        };
    
        gsoft.spa.Channel = {
            Error: "g.spa.error",
            PageChanging: "g.spa.pageChanging",
            PageChanged: "g.spa.pageChanged"
        };
    })();

    // Utils
    // ---------------------------------
    
    (function(utils) {
        function isImplementing(obj, propertyKeys) {
            if (propertyKeys.length === 0) {
                return false;
            }
            
            return propertyKeys.every(function(propertyKey) {
                return propertyKey in obj;           
            });
        }
        
        // Represents the properties that should be present on every object that claims to 
        // be a jQuery promise .
        var promiseDefinition = ["always", "then", "done", "fail"];
        
        utils.spa = {
            // summary:
            //         Determines wether @value is a jQuery element.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            _isjQueryElement: function(value) {
                return value instanceof jQuery;
            },
            
            // summary:
            //         Determines wether @value is a jQuery promise.
            // value: Object
            //         The value to perform the check against.
            // returns:
            //         A boolean.
            _isjQueryPromise: function(value) {
                return !utils.isNull(value) && isImplementing(value, promiseDefinition);
            },
    
            // summary:
            //         Retrieve the URL of the current page.
            // returns:
            //         An URL.
            _getCurrentUrl: function() {
                return window.location.href;
            },
    
            // summary:
            //         Navigate to the specified @url.
            // url:
            //         The URL to navigate to.
            _navigate: function(url) {
                window.location.href = url;
            },
    
            // summary:
            //         Open a new window (or tab) at the specified @url with the specified @options.
            // url:
            //         The URL to navigate to.
            // options:
            //         Windows options.
            _openWindow: function(url, options) {
                if (utils.isNull(options)) {
                    options = [];
                }
    
                window.open.apply(window, options.unshift(url));
            }
        };
    })(gsoft.utils);

    // Ensure
    // ---------------------------------
    
    (function(utils) {
        // summary:
        //          Assert that a @parameter implements the KO view model contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isViewModel = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.bind, parameterName, context).isFunction(assertionMessage);
                    
            return this;
        };
    
        // summary:
        //          Assert that a @parameter implements the CompositeViewModelBinder's binding object contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isBinding = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.viewModelAccessor, parameterName, context).isFunction(assertionMessage);
            gsoft.ensure(parameter.bindingElementAccessor, parameterName, context).isFunction(assertionMessage);
    
            return this;
        };
    
        // summary:
        //          Assert that a @parameter implements the view model binder contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isViewModelBinder = function(parameter, parameterName, context, assertionMessage) {
            if (!utils.isNull(parameter)) {
                gsoft.ensure(parameter.bind, parameterName, context).isFunction(assertionMessage);
                gsoft.ensure(parameter.unbind, parameterName, context).isFunction(assertionMessage);
                gsoft.ensure(parameter.isBound, parameterName, context).isFunction(assertionMessage);
            }
    
            return this;
        };
    
        // summary:
        //          Assert that a @parameter implements the view provider contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isViewProvider = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.get, parameterName, context).isFunction(assertionMessage);
        };
    
        // summary:
        //          Assert that a @parameter implements the view renderer contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isViewRenderer = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.render, parameterName, context).isFunction(assertionMessage);
            gsoft.ensure(parameter.clear, parameterName, context).isFunction(assertionMessage);
        };
    
        // summary:
        //          Assert that a @parameter implements the module activator contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isModuleActivator = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.activate, parameterName, context).isFunction(assertionMessage);
        };
    
        // summary:
        //          Assert that a @parameter implements the route registry contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isRouteRegistry = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.add, parameterName, context).isFunction(assertionMessage);
            gsoft.ensure(parameter.find, parameterName, context).isFunction(assertionMessage);
        };
    
        // summary:
        //          Assert that a @parameter implements the route URL resolver contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isRouteUrlResolver = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.getRouteUrl, parameterName, context).isFunction(assertionMessage);
        };
    
        // summary:
        //          Assert that a @parameter implements a route definition contract.
        // parameter: Object
        //         The parameter to verify.
        // caller: String
        //         An optional identifier for the source calling the ensure function.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isRouteDefinition = function(parameter, parameterName, context, assertionMessage) {
            gsoft.ensure(parameter, parameterName, context).isNotNull(assertionMessage);
            gsoft.ensure(parameter.module, parameterName, context).isNotNullOrEmpty(assertionMessage);
            gsoft.ensure(parameter.name, parameterName, context).isNotNullOrEmpty(assertionMessage);
        };
        
        // summary:
        //          Assert that a @parameter implements the jQuery's promise contract.
        // parameter: Object
        //         The parameter to verify.
        // parameterName: String
        //         An optional name of the parameter to verify.
        // context: String
        //         An optional identifier providing more information about the context of the call to ensure.
        // returns:
        //         An object that contains all the assertions functions.
        gsoft.ensure.assertions._isjQueryPromise = function(parameter, parameterName, context, assertionMessage) {
            if (!utils.spa._isjQueryPromise(parameter)) {
                var message = gsoft.ensure._getMessage(assertionMessage, "{0}{1} Parameter must be a jQuery promise.", parameterName, context);
                throw new gsoft.ArgumentError(message);
            }
                    
            return this;
        };
    })(gsoft.utils);

    // Mediator extensions
    // --------------------------------
    
    (function(ensure, mediator) {
        // summary:
        //         Publish an error to a @channel. This is the same as gsoft.mediator.publish but
        //         it enforce a structure for an error event value.
        // channel: String
        //         A channel to publish to.
        // source: Object
        //         The source of the error.
        // errorType: Object
        //         The type of error.
        // value: Object
        //         An optional value.
        gsoft.mediator.publishError = function(channel, source, errorType, value) {
            ensure(channel, "channel", "Mediator.publishError").isNotNullOrEmpty();
            ensure(source, "source", "Mediator.publishError").isNotNullOrEmpty();
            ensure(errorType, "errorType", "Mediator.publishError").isNotNullOrEmpty();
    
            mediator.publish(channel, {
                source: source,
                errorType: errorType,
                data: value
            });
        };
    })(gsoft.ensure, 
       gsoft.mediator);

    // Route
    // ---------------------------------
    
    (function(utils, ensure) {
        gsoft.spa.Route = function(moduleName, routeName, url) {
            ensure(moduleName, "moduleName", "Route").isNotNullOrEmpty();
            ensure(routeName, "routeName", "Route").isNotNullOrEmpty();
            ensure(url, "url", "Route").isNotNullOrEmpty();
    
            this.module = moduleName;
            this.name = routeName;
            this.url = url;
        };
    
        gsoft.spa.Route.prototype = {
            getUrl: function(parameters) {
                var url = this._removeUnspecifiedOptionalParameters(this._replaceParameters(this.url, parameters));
    
                if (url.indexOf("/:") !== -1) {
                    throw new gsoft.ArgumentError("Route.getUrl - You must specify a value for every required parameters of the route {0}.".format(this.url));
                }
    
                return url;
            },
    
            _replaceParameters: function(url, parameters) {
                if (utils.isObject(parameters)) {
                    utils.objectForEach(parameters, function(parameter, parameterKey) {
                        // Do not change the replacement order.
                        url = url.replace("(/:{0})".format(parameterKey), "/{0}".format(parameter));
                        url = url.replace(":{0}".format(parameterKey), parameter);
                    });
                }
    
                return url;
            },
    
            _removeUnspecifiedOptionalParameters: function(url) {
                url = url.replace(/(\(\/:.*\))/g, "");
    
                return url;
            }
        };
    })(gsoft.utils,
       gsoft.ensure);

    // Route registry
    // ---------------------------------
    
    (function(utils, ensure, Route) {
        gsoft.spa.routeRegistry = {
            _routes: {},
    
            add: function(moduleName, routeName, url) {
                var route = new Route(moduleName, routeName, url);
    
                if (utils.isUndefined(this._routes[moduleName])) {
                    this._routes[moduleName] = {};
                }   
    
                this._routes[moduleName][routeName] = route;
            },
    
            find: function(moduleName, routeName) {
                try {
                    return this._routes[moduleName][routeName];
                }
                catch (e) {
                    return null;
                }
            }
        };
    })(gsoft.utils,
       gsoft.ensure,
       gsoft.spa.Route);

    // Route URL resolver
    // ---------------------------------
    
    (function(utils) {
        gsoft.spa.routeUrlResolver = {
            getRouteUrl: function(routes, moduleName, routeName, routeParameters) {
                var route = routes.find(moduleName, routeName);
    
                if (utils.isNull(route)) {
                    throw new gsoft.ArgumentError("RouteUrlResolver.getRouteUrl - Cannot find a route named {0} in module {1}".format(routeName, moduleName));
                }
    
                return route.getUrl(routeParameters);
            }
        };
    })(gsoft.utils);

    // Router
    // ---------------------------------
    
    (function(utils, ensure, mediator) {
        gsoft.spa.router = {
            // summary:
            //         Ensure that a @parameter respect the specified assertions.
            // url: String
            //         The URL of the route.
            // callback: Function
            //         An handler to call everytime the route is triggered.
            // options: Object
            //         An optional object that contains additional informations about the route.
            //
            //         isDefault: Determine if the route is the landing route.
            //         onEnter: An handler to call everytime the route is entered.
            //         onExit: An handler to call everytime the route is left.
            // returns:
            //         An object that contains informations about the route and a function to 
            //         execute the route.
            addRoute: function(url, callback, options) {
                ensure(url, "url", "Router.addRoute").isNotNullOrEmpty();
                ensure(callback, "callback", "Router.addRoute").isFunction();
    
                var route = Path.map(url).to(function() {
                    utils.trace("Entered route {0}".format(url));
    
                    mediator.publish("g.spa.enteredRoute", {
                        url: url,
                        parameters: this.params
                    });
    
                    callback.apply(null, [url, this.params]);
                });
                    
                utils.trace("{0} route has been registred".format(url));
                
                options = options || {};
    
                route.exit(function(canExitCallback) {
                    utils.trace("Exited route {0}".format(url));
    
                    mediator.publish("g.spa.exitedRoute", url);
    
                    if (utils.isFunction(options.onExit)) {
                        return options.onExit(url, canExitCallback);
                    }
                    
                    return false;
                });
                
                return {
                    url: url,
                    run: function() {
                        route.run();
                    }
                };
            },
    
            // summary:
            //         Triggered the route matching the @url.
            // url: String
            //         The URL of the route.
            runRoute: function(url) {
                ensure(url, "url", "Router.runRoute").isNotNullOrEmpty();
    
                var route = Path.match(url, true);
    
                if (utils.isNull(route)) {
                    throw new gsoft.ArgumentError("Router.runRoute - {0} does not match any registered route".format(url));
                }
    
                route.run();
            },
    
            // summary:
            //         Set a @route as the application root.
            // route: String
            //         A route.
            setRoot: function(route) {
                ensure(route, "route", "Router.setDefaultRoute").isNotNullOrEmpty();
    
                Path.root(route);
    
                utils.trace("{0} is the default route".format(route));
            },
    
            // summary:
            //         Set an @handler to call everytime an unregistered URL is entered.
            // handler: Function
            //         An handler.
            set404Handler: function(handler) {
                ensure(handler, "handler", "Router.set404Handler").isFunction();
    
                Path.rescue(handler);
            },
            
            // summary:
            //         Starts the routing engine.
            start: function() {
                utils.trace("Starting router");
    
                Path.listen();
            }
        };
    })(gsoft.utils, 
       gsoft.ensure, 
       gsoft.mediator);

    // View provider
    // ---------------------------------
    
    (function($, utils, ensure, mediator) {
        gsoft.spa.viewProvider = {
            _cache: {},
    
            // summary:
            //         Retrieve a view from an @url.
            // url: String
            //         The URL of the review to fetch.
            // useCache: Boolean
            //         An optional boolean indicating if the view should be cache.
            // returns:
            //         A jQuery promise.
            get: function(url, useCache) {
                ensure(url, "url", "ViewProvider.get").isNotNullOrEmpty();
    
                var that = this;
    
                // Use a "proxy" deferred to handle the view caching feature.
                var deferred = $.Deferred();
                
                // Try to get the view from the cache.
                var view = this._getFromCache(url);
    
                if (!utils.isNullOrEmpty(view)) {
                    deferred.resolveWith(this, [view]);
                } else {
                    var promise = this._getFromServer(url);
                    
                    promise.done(function(response) {
                        if (utils.isNullOrEmpty(response)) {
                            deferred.rejectWith(this, [null, "", "Invalid view."]);
    
                            that._publishInvalidViewError(url, null, "", "");
                        }
                        
                        if (useCache === true) {
                            that._cacheView(url, response);
                        }
                        
                        deferred.resolveWith(this, [response]);
                    });
                
                    promise.fail(function(jqxhr, textStatus, errorThrown) {
                        deferred.rejectWith(this, [jqxhr, textStatus, errorThrown]);
    
                        // Handle the request failure.
                        that._onRequestFailed(url, jqxhr, textStatus, errorThrown);
                    });
                }
                
                return deferred.promise();
            },
            
            _cacheView: function(url, content) {
                this._cache[url] = content;
            },
            
            _getFromCache: function(url) {
                return this._cache[url];
            },
            
            _getFromServer: function(url) {
                return $.ajax({
                    url: url,
                    cache: false,
                    dataType: "html"
                });
            },
            
            _onRequestFailed: function(url, jqxhr, textStatus, errorThrown) {
                this._writeErrorTrace(url, jqxhr, textStatus, errorThrown);
                this._publishInvalidViewError(url, jqxhr, textStatus, errorThrown);
            },
            
            _writeErrorTrace: function(url, jqxhr, textStatus, errorThrown) {
                var exception = utils.isNullOrEmpty(textStatus) ? "{NO_EXCEPTION}" : textStatus;
                var httpError = utils.isNullOrEmpty(errorThrown) ? "{NO_HTTP_ERROR}" : errorThrown;
    
                var message = "An error occured while fetching view \"{0}\".\nStatus code: {1}\nStatus text: {2}\nException: {3}\nHTTP error: {4}"
                    .format(url, jqxhr.status, jqxhr.statusText, exception, httpError);
    
                utils.trace(message);
            },
    
            _publishInvalidViewError: function(url, jqxhr, textStatus, errorThrown) {
                this._publishError("InvalidView", {
                    url: url,
                    jqxhr: jqxhr,
                    textStatus: textStatus,
                    errorThrown: errorThrown
                });
            },
    
            _publishError: function(errorType, data) {
                mediator.publishError(gsoft.spa.Channel.Error, gsoft.spa.Component.ViewProvider, errorType, data);
            }
        };
    })(jQuery, gsoft.utils, gsoft.ensure, gsoft.mediator);

    // View renderer
    // ---------------------------------
    
    (function(ensure) {
        gsoft.spa.viewRenderer = {
            render: function(targetElement, view) {
                ensure(targetElement, "targetElement", "ViewRenderer.renderView").isDomElement();
    
                targetElement.innerHTML = view;
            },
    
            clear: function(/* targetElement */) {
            }
        };
    })(gsoft.ensure);

    // Base ViewModel binder
    // ---------------------------------
    
    (function(utils, ensure) {
        gsoft.spa.ViewModelBinder = {
            _getViewModelFromAccessor: function(viewModelAccessor, viewModelParameters) {
                // An accessor function is used to retrieve the view model to let the user create a view model based
                // on dynamic parameters (for example: the route parameters).
                var viewModel = viewModelAccessor(viewModelParameters);
    
                ensure(viewModel)._isViewModel("ViewModelBinder._getViewModelFromAccessor - \"viewModelAccessor\" must return a valid view model.");
    
                return viewModel;
            },
            
            // summary:
            //         Retrieve the identification of an @element.
            // description:
            //         Retrieve the identification of an @element. An identification can be the
            //         id of the @element or the CSS class of the @element.
            // element: DOM element
            //         The element to retrieve the identification from.
            // returns:
            //         An element identification is available, otherwise "{ANONYMOUS}".
            _getElementIdentification: function(element) {
                var identitier = element.getAttribute("id");
    
                if (utils.isNullOrEmpty(identitier)) {
                    identitier = element.getAttribute("class");
                }
    
                return !utils.isNullOrEmpty(identitier) ? identitier : "{ANONYMOUS}";
            },
    
            _cleanElement: function(element) {
                ensure(element, "element", "ViewModelBinder._cleanElement").isDomElement();
    
                // The trace call has been wrapped inside a check for "debug" mode because it involves extra processing
                // to compute the log message.
                if (gsoft.debug === true) {
                    utils.trace("Clearing knockout bindings for DOM element \"{0}\"".format(this._getElementIdentification(element)));
                }
    
                if (!utils.isNull(ko.dataFor(element))) {
                    ko.cleanNode(element);
                }
            },
    
            _disposeViewModel: function(viewModel) {
                if (utils.isFunction(viewModel.dispose)) {
                    viewModel.dispose();
                } else {
                    utils.trace("Cannot dispose view model because a \"dispose\" function is not available.");
                }
            }
        };
    })(gsoft.utils, 
       gsoft.ensure);

    // Dummy ViewModel binder
    // ---------------------------------
    
    (function($) {
        // summary:
        //         Dummy binder to use when a route is specified without a view model binder.
        gsoft.spa.DummyViewModelBinder = function() {
        };
    
        gsoft.spa.DummyViewModelBinder.prototype = {
            bind: function() {
                var deferred = $.Deferred();
                deferred.resolveWith(this);
    
                return deferred.promise();
            },
    
            unbind: function() {
            },
    
            isBound: function() {
                return true;
            }
        };
    })(jQuery);

    // Composite ViewModel binder
    // ---------------------------------
    
    (function($, utils, ensure, mediator, undefined) {
        // summary:
        //         A view model binder handling routes that contains multiple KO view models.
        // bindings: Array
        //         An array of bindings.
        gsoft.spa.CompositeViewModelBinder = function(bindings) {
            ensure(bindings, "bindings", "CompositeViewModelBinder.ctor").isArray().isNotEmpty();
    
            bindings.forEach(function(binding) {
                ensure(binding)._isBinding("CompositeViewModelBinder.ctor - \"bindings\" array contains at least an invalid binding.");
            });
    
            this._element = null;
            this._bindings = bindings;
            this._isBound = false;
        };
    
        gsoft.spa.CompositeViewModelBinder.prototype = $.extend({}, gsoft.spa.ViewModelBinder, {
            // summary:
            //         Bind the KO view models.
            // element: jQuery element
            //         An element to bind the @viewModel to.
            // viewModelParameters: Object
            //         An objet thats contains parameters for the view model accessor.
            // returns:
            //         A jQuery promise.
            bind: function(element, viewModelParameters) {
                ensure(element, "element", "CompositeViewModelBinder.bind").isDomElement();
    
                this._element = element;
    
                var that = this;
                var deferred = $.Deferred();
                var bindingPromises = this._applyBindings(element, viewModelParameters);
    
                function whenDone() {
                    that._isBound = true;
    
                    deferred.resolveWith(that, [that.bindings]);
                }
    
                function whenFail() {
                    deferred.rejectWith(that, [that.bindings]);
                    
                    // Handle failure.
                    that._onBindingFailed();
                }
    
                $.when.apply($, bindingPromises).done(whenDone).fail(whenFail);
    
                return deferred.promise();
            },
    
            // summary:
            //         Unbind KO the view models.
            // description:
            //         Unbind the view models by cleaning the DOM elements from their KO bindings
            //         and calling the dispose function on every view models.
            unbind: function() {
                var that = this;
                
                return this._tryClearBindings(function() {
                    that._clearBindings(that._element);
                    
                    that._isBound = false;
                    that._element = null;
                });
            },
    
            // summary:
            //         Indicate if the view model is bound to the view.
            // returns:
            //         A boolean.
            isBound: function() {
                return this._isBound;
            },
    
            _applyBindings: function(element, viewModelParameters) {
                var bindingPromises = [];
                
                this._bindings.forEach(function(binding) {
                    // Augments the binding with the resolved view model for further used.
                    binding.viewModel = this._getViewModelFromAccessor(binding.viewModelAccessor, viewModelParameters);
    
                    var bindingElement = this._getBindingElement(binding.bindingElementAccessor, element);
                    var bindingPromise = this._bindViewModel(binding.viewModel, bindingElement);
                        
                    ensure(bindingPromise)._isjQueryPromise("CompositeViewModelBinder._applyBindings - View model \"bind\" function must return a jQuery promise.");
                    bindingPromises.push(bindingPromise);
                }, this);
    
                return bindingPromises;
            },
    
            _getBindingElement: function(bindingElementAccessor, containerElement) {
                var bindingElement = bindingElementAccessor(containerElement);
                    
                if (utils.isNull(bindingElement)) {
                    throw new gsoft.InvalidOperationError("CompositeViewModelBinder._getBindingElement - Binding accessor must return valid element.");
                }
    
                return bindingElement;
            },
    
            _bindViewModel: function(viewModel, element) {
                this._cleanElement(element);
    
                return viewModel.bind(element);
            },
    
            _tryClearBindings: function(clearBindings) {
                var result = this._evaluateIfViewModelsCanBeDisposed(this._bindings);
                
                if (result.cannotDisposeAllViewModels()) {
                    return false;
                }
                
                if (result.hasAsynchronousCanDisposeEvaluation()) {
                    return $.when.apply($, result.canDisposePromises).done(function() {
                        // All the promises resolved successfully, all the view models can be disposed, we can clear the bindings.
                        // Otherwise, we do not clear any bindings.
                        clearBindings();
                    });
                }
                
                // All the view models that had the "canDispose" function defined, returned true, we can clear the bindings.
                clearBindings();
            
                if (result.canDisposeFunctionIsNotImplementedOnAnyViewModels()) {
                    return undefined;
                }
    
                return true;
            },
            
            _evaluateIfViewModelsCanBeDisposed: function(bindings) {
                var that = this;
                var canDisposePromises = [];
                var unimplementedCount = 0;
                
                // If at least one view model that implements the "canDispose" function returned false, 
                // none of the bindings can be cleared.
                var canDisposeAllViewModels = bindings.every(function(binding) {
                    if (utils.isFunction(binding.viewModel.canDispose)) {
                        var result = binding.viewModel.canDispose();
    
                        if (utils.isBoolean(result)) {
                            if (result === false) {
                                // Break the loop.
                                return false;
                            }
                        } else if (utils.spa._isjQueryPromise(result)) {
                            canDisposePromises.push(result);
                        } else {
                            throw new gsoft.InvalidOperationError("CompositeViewModelBinder.unbind - The \"canDispose\" function must return a boolean value or a jQuery promise.");
                        }
                    } else {
                        unimplementedCount += 1;
                    }
                    
                    // Evaluate next binding if any.
                    return true;
                }, this);
                
                return {
                    cannotDisposeAllViewModels: function() {
                        return canDisposeAllViewModels === false;
                    },
                    canDisposeFunctionIsNotImplementedOnAnyViewModels: function() {
                        return unimplementedCount === that._bindings.length;
                    },
                    hasAsynchronousCanDisposeEvaluation: function() {
                        return canDisposePromises.length > 0;
                    },
                    canDisposePromises: canDisposePromises
                };
            },
            
            _clearBindings: function(element) {
                this._bindings.forEach(function(binding) {
                    var bindingElement = this._getBindingElement(binding.bindingElementAccessor, element);
    
                    if (!gsoft.isNull(bindingElement)) {
                        this._cleanElement(bindingElement);
                    }
                   
                    this._disposeViewModel(binding.viewModel);
                }, this);
            },
            
            _onBindingFailed: function() {
                utils.trace("An error occured while binding a view model to a view with the \"CompositeViewModelBinder\"");
    
                mediator.publishError(
                    gsoft.spa.Channel.Error,
                    gsoft.spa.Component.CompositeViewModelBinder,
                    "BindingFailed");
            }
        });
    })(jQuery, 
       gsoft.utils, 
       gsoft.ensure, 
       gsoft.mediator);

    // Single ViewModel binder
    // ---------------------------------
    
    (function($, utils, ensure, mediator, undefined) {
        // summary:
        //         A view model binder that handle routes with a single view model.
        // viewModelAccessor: Function
        //         A function that returns a KO view model to bind.
        gsoft.spa.SimpleViewModelBinder = function(viewModelAccessor) {
            ensure(viewModelAccessor, "viewModelAccessor", "SimpleViewModelBinder.ctor").isFunction();
    
            this._element = null;
            this._viewModel = null;
            this._viewModelAccessor = viewModelAccessor;
            this._isBound = false;
        };
    
        gsoft.spa.SimpleViewModelBinder.prototype = $.extend({}, gsoft.spa.ViewModelBinder, {
            // summary:
            //         Bind the KO view model.
            // element: jQuery element
            //         An element to bind the @viewModel to.
            // viewModelParameters: Object
            //         An objet thats contains parameters for the view model accessor.
            // returns:
            //         A jQuery promise.
            bind: function(element, viewModelParameters) {
                ensure(element, "element", "SimpleViewModelBinder.bind").isDomElement();
    
                this._viewModel = this._getViewModelFromAccessor(this._viewModelAccessor, viewModelParameters);
                this._element = element;
    
                var that = this;
                var deferred = $.Deferred();
                var bindingPromise = this._bindViewModel(this._viewModel, element);
    
                ensure(bindingPromise)._isjQueryPromise("SimpleViewModelBinder.bind - View model \"bind\" function must return a jQuery promise.");
    
                bindingPromise.done(function() {
                    that._isBound = true;
    
                    deferred.resolveWith(that, [that._viewModel, element]);
                });
    
                bindingPromise.fail(function() {
                    deferred.rejectWith(that, [that._viewModel, element]);
    
                    // Handle failure.
                    that._onBindingFailed();
                });
                
                return deferred.promise();
            },
    
            // summary:
            //         Unbind the KO view model.
            // description:
            //         Unbind the view model by cleaning the DOM element from his KO bindings
            //         and call the dispose function on the view model.
            unbind: function() {
                var that = this;
    
                return this._tryDisposeViewModel(function() {
                    that._disposeViewModel(that._viewModel);
                    
                    that._isBound = false;
    
                    that._cleanElement(that._element);
                    that._element = null;
                });
            },
    
            // summary:
            //         Indicate if the view model is bound to the view.
            // returns:
            //         A boolean.
            isBound: function() {
                return this._isBound;
            },
        
            _bindViewModel: function(viewModel, element) {
                this._cleanElement(element);
    
                return viewModel.bind(element);
            },
            
            _tryDisposeViewModel: function(disposeViewModel) {
                if (utils.isFunction(this._viewModel.canDispose)) {
                    var result = this._viewModel.canDispose();
                    
                    if (utils.isBoolean(result)) {
                        if (result === true) {
                            disposeViewModel();
                        }
                        
                        return result;
                    } 
                    
                    if (utils.spa._isjQueryPromise(result)) {
                        result.done(function() {
                            disposeViewModel();
                        });
                        
                        return result;
                    }
                    
                    throw new gsoft.InvalidOperationError("SimpleViewModelBinder.unbind - The \"canDispose\" function must return a boolean value or a jQuery promise.");
                }
                
                disposeViewModel();
                
                return undefined;
            },
    
            _onBindingFailed: function() {
                utils.trace("An error occured while binding a view model to a view with the \"SimpleViewModelBinder\"");
    
                mediator.publishError(
                    gsoft.spa.Channel.Error,
                    gsoft.spa.Component.SimpleViewModelBinder,
                    "BindingFailed");
            }
        });
    })(jQuery, 
       gsoft.utils, 
       gsoft.ensure, 
       gsoft.mediator);

    // Module activator
    // ---------------------------------
    
    (function(utils, ensure) {
        gsoft.spa.moduleActivator = {
            // summary:
            //         Retrieve all the modules in @root that meet the criterias for registration.
            // description:
            //         Retrieve all the modules in @root that meet the criterias for registration. 
            //         To be a candidate for registration a module property key must ends with "Module"
            //         and have an "activate" property.
            // root: Object
            //         An object that contains the modules to activate.
            // activateModule: Function
            //         A function that will activate the specified module.
            // example:
            //         gsoft.spa.moduleActivator(gsoft.my.modules, function(module, moduleName) { ... });
            activate: function(root, activateModule) {
                ensure(root, "root", "ModuleActivator.activate").isNotNull();
                ensure(activateModule, "activateModule", "ModuleActivator.activate").isFunction();
    
                var targets = this._findModules(root);
                
                utils.objectForEach(targets, function(target, name) {
                    utils.trace("Activating module \"{0}\"".format(name));
    
                    activateModule(target, name.replace("Module", ""));
    
                    utils.trace("Module \"{0}\" activated".format(name));
                });
            },
            
            _findModules: function(root) {
                var modules = {};
                
                function add(property, propertyKey) {
                    modules[propertyKey] = property;
                }
                
                utils.objectForEach(root, function(property, propertyKey) {
                    // If this a module, add it to the list and stop the search as we do not support nested modules.
                    // Otherwise, try to find modules at the next level.
                    if (this._isModule(propertyKey, property)) {
                        add(property, propertyKey);
                    } 
                    else if (utils.isObject(property) && !utils.spa._isjQueryElement(property) && !utils.isDomElement(property)) {
                        utils.objectForEach(this._findModules(property), add);
                    }
                }, this);
    
                return modules;
            },
            
            _isModule: function(propertyKey, property) {
                // By convention a module name must end with the suffix "Module".
                if (propertyKey.endsWith("Module") && utils.isObject(property)) {
                    // By convention a module must have an "activate" function.
                    if (!utils.isNull(property) && utils.isFunction(property.activate)) {
                        return true;
                    }
                    
                    utils.trace("* Module \"{0}\" doesn't meet all the criterias for registration.".format(propertyKey));
                }
    
                return false;
            }
        };
    })(gsoft.utils, 
       gsoft.ensure);

    // Shell
    // ---------------------------------
    
    (function($, router, defaultModuleActivator, defaultViewProvider, defaultViewRenderer, defaultRouteRegistry, defaultRouteUrlResolver, utils, ensure, mediator) {
        gsoft.spa.shell = {
            _moduleActivator: null,
            _viewProvider: null,
            _viewRenderer: null,
            _routeRegistry: null,
            _routeUrlResolver: null,
    
            _started: false,
            _containerElement: null,
    
            // summary:
            //         Starts the application.
            // moduleNamespace: Object
            //         An object that contains the modules to register.
            // containerElement: A jQuery element
            //         An element that will act as a placeholder for the views content.
            // callback: Function
            //         An optional callback that is call when the application is started. 
            start: function(moduleNamespace, containerElement, callback) {
                ensure(moduleNamespace, "moduleNamespace", "Shell.start").isNotNull();
                ensure(containerElement, "containerElement", "Shell.Start").isDomElement();
    
                if (this._started) {
                    throw new gsoft.InvalidOperationError("Shell.start - The KO SPA shell has already been started.");
                }
    
                this._containerElement = containerElement;
                this._registerModules(moduleNamespace);
                this._startRouter();
    
                this._started = true;
    
                if (utils.isFunction(callback)) {
                    callback();
                }
            },
    
            _registerModules: function(moduleNamespace) {
                this._moduleActivator.activate(moduleNamespace, this._activateModule.bind(this));
            },
    
            _activateModule: function(moduleInstance, moduleName) {
                var that = this;
    
                // Handles backward compatibility for the old function signature (url, viewUrl, viewModelBinder).
                var addRouteProxy = function(/* (options) or (url, viewUrl, viewModelBinder) */) {
                    var options = null;
    
                    if (arguments.length === 1) {
                        options = arguments[0];
                    } else if (arguments.length === 2 || arguments.length === 3) {
                        options = {
                            url: arguments[0],
                            viewUrl: arguments[1],
                            viewModelBinder: arguments[2]
                        };
                    } else if (arguments.length > 3) {
                        throw new gsoft.ArgumentError("Shell.addRoute - To specify a route module or name you must use an object literal as parameter.");
                    }
    
                    that.addRoute(options, moduleName);
                };
              
                moduleInstance.activate.apply(moduleInstance, [addRouteProxy, this.addAliasRoute.bind(this)]);            
            },
    
            // summary:
            //         Add a route to the application.
            // options: Object
            //         module: String
            //              An optional module name.
            //         name: String
            //              An optional route name.
            //         url: String
            //              A route URL.
            //         viewUrl: String
            //              A view URL.
            //         viewModelBinder: ViewModelBinder
            //              An optional view model binder.
            // returns:
            //         An object that represents the route.
            addRoute: function(options) {
                var that = this;
                var moduleName = arguments[1];
    
                // jscs:disable requireBlocksOnNewline
                ensure(options, "options", "Shell.addRoute").isObject();
                ensure(options.module, "module").isTrue(function(x) { return !utils.isNullOrEmpty(x) || !utils.isNullOrEmpty(moduleName); }, "Shell.addRoute module cannot be empty.");
                ensure(options.url, "url", "Shell.addRoute").isNotNullOrEmpty();
                ensure(options.viewUrl, "viewUrl").isTrue(function(x) { return !utils.isNullOrEmpty(x) || utils.isFunction(x); }, "Shell.addRoute viewUrl must be a non empty string or a function.");
                ensure(options.viewModelBinder, "viewModelBinder", "Shell.addRoute")._isViewModelBinder();  
                // jscs:enable requireBlocksOnNewline
                
                // By convention the route module is defaulted to the module that the route has been registered with if it is not specified.
                if (!utils.isNull(options) && utils.isNullOrEmpty(options.module)) {
                    options.module = arguments[1];
                }
    
                if (utils.isNullOrEmpty(options.name)) {
                    options.name = options.url;
                }
    
                if (utils.isNull(options.viewModelBinder)) {
                    options.viewModelBinder = new gsoft.spa.DummyViewModelBinder();
                }
    
                var onEnter = function(url, routeParameters) {
                    var viewUrl = options.viewUrl;
    
                    if (utils.isFunction(viewUrl)) {
                        viewUrl = viewUrl(routeParameters);
                    }
    
                    that._onEnterRoute(options.url, viewUrl, options.viewModelBinder, routeParameters);
                };
                
                var onExit = function(exitedCallback, canExitCallback) {
                    // The path.js exit event take an optionnal boolean return value to indicate if the exit
                    // will be done asynchronously with the callback. The exit will be considered asynchronous, if true is returned.
                    return that._onExitRoute(options.viewModelBinder, canExitCallback);
                };
    
                var route = router.addRoute(options.url, onEnter, {
                    onExit: onExit
                });
    
                switch (options.url) {
                    case "#/":
                        this.setApplicationRootRoute("#/");
                        break;
                    case "#/404":
                        this.set404Route("#/404");
                        break;
                }
    
                // Add the route to the shell route registry to support URL resolution.
                this._routeRegistry.add(options.module, options.name, options.url);
    
                return route;
            },
    
            _onEnterRoute: function(url, viewUrl, viewModelBinder, routeParameters) {
                var that = this;
    
                this._publishPageChanging(url, viewUrl, routeParameters);
    
                // 1- Fetch the view and inject the response HTML into the DOM.
                // 2- Bind the KO view model to the view.
                // 3- Publish an event telling that the page transition is done.
                this._fetchView(viewUrl, function() {
                    viewModelBinder.bind(that._containerElement, routeParameters).done(function() {
                        that._publishPageChanged(url, viewUrl, routeParameters);
                    });
                });
            },
    
            _onExitRoute: function(viewModelBinder, canExitCallback) {
                // When the browser "Back button" is click before the view model is bound to the view, it results in unbindding an unbound view model
                // which cause a failure. This check prevent that case.
                if (viewModelBinder.isBound()) {
                    var that = this;
                    
                    var clear = function() {
                        that._viewRenderer.clear(that._containerElement);
                    };
                    
                    // If the return value is undefined, the route will be exited normally.
                    // If unbind return a value, it means that we must deal with the possibility that the route exit might be canceled by the view model. 
                    // In that case the return value must be a boolean value or a promise. 
                    var result = viewModelBinder.unbind();
                    
                    if (utils.isDefined(result)) {
                        var canExit = function() {
                            clear();
                            canExitCallback(true);
                        };
                        
                        var cannotExit = function() {
                            canExitCallback(false);
                        };
                        
                        if (utils.isBoolean(result)) {
                            if (result === true) {
                                canExit();
                            } else {
                                cannotExit();
                            }
                        } else {
                            ensure(result, "viewModelBinder.unbind", "Shell._onExitRoute")._isjQueryPromise();
    
                            // Exit the route.
                            result.done(function() {
                                canExit();
                            });
    
                            // Do not exit the route.
                            result.fail(function() {
                                cannotExit();
                            });
                        }
                        
                        return true;
                    } else {
                        clear();
                    }
                }
                
                return false;
            },
    
            _fetchView: function(viewUrl, callback) {
                var that = this;
                var cacheView = true;   
                var viewParameters = this._getViewParametersFromUrl();
    
                if (!utils.isNullOrEmpty(viewParameters)) {
                    cacheView = false;
                    viewUrl = "{0}?{1}".format(viewUrl, viewParameters);
                } else if (viewUrl.indexOf("?") !== -1) {
                    cacheView = false;
                }
                
                this._viewProvider.get(viewUrl, cacheView).done(function(view) {
                    that._viewRenderer.render(that._containerElement, view);
                    callback.call(that, view);
                });
            },
    
            _getViewParametersFromUrl: function() {
                var currentUrl = utils.spa._getCurrentUrl();
                
                // Everything that is after the "@" delimiter is by convention a string of parameters that is append to the view URL.
                var index = currentUrl.indexOf("@");
    
                if (index !== -1) {
                    return currentUrl.substring(index + 1);
                }
    
                return "";
            },
    
            // summary:
            //         Add a route to the application that will redirect to another route when activated.
            // options: Object
            //         url: String
            //              A route URL.
            //         redirectTo: String | Object | Function
            //              A route to redirect to.
            // returns:
            //         An object that represents the route.
            addAliasRoute: function(options) {
                ensure(options, "options", "Shell.addAliasRoute").isObject();
                ensure(options.url, "options.url", "Shell.addAliasRoute").isNotNullOrEmpty();
                ensure(options.redirectTo, "options.redirectTo", "Shell.addAliasRoute").isNotNull();
    
                var route = null;
    
                if (utils.isString(options.redirectTo)) {
                    route = this._addAliasRouteFromString(options);
                } else if (utils.isFunction(options.redirectTo)) {
                    route = this._addAliasRouteFromFunction(options);
                } else if (utils.isObject(options.redirectTo)) {
                    route = this._addAliasRouteFromRouteObject(options);
                } else {
                    throw new gsoft.InvalidOperationError("Shell.addAliasRoute - The redirect URL for the alias {0} is invalid.".format(options.url));
                }
    
                return route;
            },
    
            _addAliasRouteFromFunction: function(options) {
                return router.addRoute(options.url, function(url, routeParameters) {
                    var redirectUrl = options.redirectTo(routeParameters);
    
                    if (utils.isNullOrEmpty(redirectUrl)) {
                        throw new gsoft.InvalidOperationError("Shell.addAliasRoute - The redirect URL for the alias {0} is invalid.".format(options.url));
                    }
    
                    utils.spa._navigate(redirectUrl);
                });
            },
    
            _addAliasRouteFromRouteObject: function(options) {
                ensure(options.redirectTo, "options.redirectTo.module", "Shell.addAliasRoute")._isRouteDefinition();
    
                var that = this;
    
                if (utils.isNull(this._routeRegistry.find(options.redirectTo.module, options.redirectTo.name))) {
                    throw new gsoft.InvalidOperationError("Shell.addAliasRoute - The redirect route for the alias {0} is invalid.".format(options.url));
                }
    
                return router.addRoute(options.url, function(url, routeParameters) {
                    var parameters = $.extend({}, routeParameters, options.redirectTo.parameters);
                    var redirectUrl = that.getRouteUrl(options.redirectTo.module, options.redirectTo.name, parameters);
    
                    utils.spa._navigate(redirectUrl);
                });
            },
    
            _addAliasRouteFromString: function(options) {
                ensure(options.redirectTo, "options.redirectTo", "Shell.addAliasRoute").isNotEmpty();
    
                return router.addRoute(options.url, function() {
                    utils.spa._navigate(options.redirectTo);
                });
            },
    
            _startRouter: function() {
                router.start();
            },
    
            _publishPageChanging: function(targetUrl, targetViewUrl, targetParameters) {
                mediator.publish(gsoft.spa.Channel.PageChanging, {
                    pageUrl: targetUrl,
                    viewUrl: targetViewUrl,
                    parameters: targetParameters
                });
            },
    
            _publishPageChanged: function(url, viewUrl, routeParameters) {
                mediator.publish(gsoft.spa.Channel.PageChanged, {
                    pageUrl: url,
                    viewUrl: viewUrl,
                    routeParameters: routeParameters
                });
            },
    
            // summary:
            //         Set a @route as the application root.
            // route: String | Object
            //         A route that will be run everytime the root of the application is requested.
            setApplicationRootRoute: function(route) {
                ensure(route, "route", "Shell.setApplicationRootRoute").isNotNull();
    
                var routeUrl = null;
    
                if (utils.isString(route)) {
                    ensure(route, "route", "Shell.setApplicationRootRoute").isNotEmpty();
    
                    routeUrl = route;
                } else if (utils.isObject(route)) {
                    ensure(route, "route", "Shell.setApplicationRootRoute")._isRouteDefinition();
    
                    routeUrl = this.getRouteUrl(route.module, route.name, route.parameters);
                } else {
                    throw new gsoft.InvalidOperationError("Shell.setApplicationRootRoute - The application root URL is invalid.");
                }
    
                router.setRoot(routeUrl);    
    
                utils.trace("{0} is the root route.".format(routeUrl));
            },
    
            // summary:
            //         Set a @route as the 404 route.
            // route: String | Object
            //         A route that will be run everytime an unregistered route is requested.
            set404Route: function(route) {
                ensure(route, "route", "Shell.set404Route").isNotNull();
    
                var routeUrl = null;
    
                if (utils.isString(route)) {
                    ensure(route, "route", "Shell.set404Route").isNotEmpty();
    
                    routeUrl = route;
                } else if (utils.isObject(route)) {
                    ensure(route, "route", "Shell.set404Route")._isRouteDefinition();
    
                    routeUrl = this.getRouteUrl(route.module, route.name, route.parameters);
                } else {
                    throw new gsoft.InvalidOperationError("Shell.set404Route - The 404 URL is invalid.");
                }
    
                router.set404Handler(function() {
                    router.runRoute(routeUrl);
                });
    
                utils.trace("{0} is the 404 route.".format(routeUrl));
            },
    
            // summary:
            //         Adds an handler that is called when an error occurs.
            // callback: Function
            //         An event handler.
            // predicate: Function
            //          An optional function to filter the published events.
            onError: function(callback, predicate) {
                mediator.subscribe(gsoft.spa.Channel.Error, callback, predicate);
            },
    
            // summary:
            //         Adds an handler that is called before a page change.
            // callback: Function
            //         An event handler.
            // predicate: Function
            //          An optional function to filter the published events.
            onPageChanging: function(callback, predicate) {
                mediator.subscribe(gsoft.spa.Channel.PageChanging, callback, predicate);
            },
    
            // summary:
            //         Adds an handler that is called after a page changed.
            // callback: Function
            //         An event handler
            // predicate: Function
            //          An optional function to filter the published events.
            onPageChanged: function(callback, predicate) {
                mediator.subscribe(gsoft.spa.Channel.PageChanged, callback, predicate);
            },
    
            // summary:
            //         Gets the routes URL that match the combinaison of the specified @module, @name and @routeParameters.
            // module: String
            //         A module name.
            // name: String
            //         A route name.
            // routeParameters: Object
            //         An object that contains key / value associations for every parameters of the routes.
            getRouteUrl: function(module, name, routeParameters) {
                ensure(module, "module", "Shell.getRouteUrl").isNotNullOrEmpty();
                ensure(name, "name", "Shell.getRouteUrl").isNotNullOrEmpty();
    
                return this._routeUrlResolver.getRouteUrl(this._routeRegistry, module, name, routeParameters);
            },
    
            // summary:
            //         Replace the native module activator by @newModuleActivator.
            // newModuleActivator: Object
            //         A module activator instance.
            setModuleActivator: function(newModuleActivator) {
                ensure(newModuleActivator, "newModuleActivator", "Shell.setModuleActivator")._isModuleActivator();
    
                this._moduleActivator = newModuleActivator;
            },
    
            // summary:
            //         Replace the native view provider by @newViewProvider.
            // newViewProvider: Object
            //         A view provider instance.
            setViewProvider: function(newViewProvider) {
                ensure(newViewProvider, "newViewProvider", "Shell.setViewProvider")._isViewProvider();
    
                this._viewProvider = newViewProvider;
            },
    
            // summary:
            //         Replace the native view renderer by @newViewRenderer.
            // newViewRenderer: Object
            //         A view renderer instance.
            setViewRenderer: function(newViewRenderer) {
                ensure(newViewRenderer, "newViewRenderer", "Shell.setViewRenderer")._isViewRenderer();
    
                this._viewRenderer = newViewRenderer;
            },
    
            // summary:
            //         Replace the native route registry by @newRouteRegistry.
            // newRouteRegistry: Object
            //         A route registry instance.
            setRouteRegistry: function(newRouteRegistry) {
                ensure(newRouteRegistry, "newRouteRegistry", "Shell.setRouteRegistry")._isRouteRegistry();
    
                this._routeRegistry = newRouteRegistry;
            },
    
            // summary:
            //         Replace the native route URL resolver by @newRouteUrlResolver.
            // newRouteUrlResolver: Object
            //         A route URL resolver instance.
            setRouteUrlResolver: function(newRouteUrlResolver) {
                ensure(newRouteUrlResolver, "newRouteUrlResolver", "Shell.setRouteUrlResolver")._isRouteUrlResolver();
    
                this._routeUrlResolver = newRouteUrlResolver;
            }
        };
    
        gsoft.spa.shell.setModuleActivator(defaultModuleActivator);
        gsoft.spa.shell.setViewProvider(defaultViewProvider);
        gsoft.spa.shell.setViewRenderer(defaultViewRenderer);
        gsoft.spa.shell.setRouteRegistry(defaultRouteRegistry);
        gsoft.spa.shell.setRouteUrlResolver(defaultRouteUrlResolver);
    
        // Define an alias to retrieve a route URL.
        gsoft.spa.action = function() {
            return gsoft.spa.shell.getRouteUrl.apply(gsoft.spa.shell, arguments);
        };
    })(jQuery, 
       gsoft.spa.router,
       gsoft.spa.moduleActivator,
       gsoft.spa.viewProvider,
       gsoft.spa.viewRenderer,
       gsoft.spa.routeRegistry,
       gsoft.spa.routeUrlResolver,
       gsoft.utils,
       gsoft.ensure,
       gsoft.mediator);

    // Action binding
    // --------------------------------
    
    (function(utils) {
        var action = ko.bindingHandlers.action = {
            init: function(element, valueAccessor) {
                var options = valueAccessor();
    
                if (!utils.isObject(options)) {
                    throw new gsoft.ArgumentError("Action binding - You must specify an object with the route options.");
                }
            },
    
            update: function(element, valueAccessor) {
                var options = valueAccessor();
    
                // This is the call to ko.toJS that unwrap all the observables and create the dependencies that will force the binding handler
                // to reevaluate and call the update function when a parameter change.
                var url = gsoft.spa.shell.getRouteUrl(ko.utils.unwrapObservable(options.module), ko.utils.unwrapObservable(options.name), ko.toJS(options.parameters));
                
                if (element.tagName === "A") {
                    element.setAttribute("href", url);
                } else {
                    var onClickProxy = function(event) {
                        action._onClick(event, url, options);
                    };
                    
                    element.addEventListener("click", onClickProxy);
                    
                    ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
                        element.removeEventListener("click", onClickProxy);
                    });  
                }   
            },
            
            _onClick: function(event, url, options) {
                event.preventDefault();
    
                var delay = options.redirectDelay;
    
                if (utils.isNull(delay)) {
                    delay = action._redirectDelay;
                }
    
                if (delay > 0) {
                    setTimeout(function() {
                        action._navigate(url, options);
                    }, delay);
                } else {
                    action._navigate(url, options);
                }
            },
    
            _navigate: function(url, options) {
                if (utils.isUndefined(options.newWindow)) {
                    utils.spa._navigate(url);
                } else {
                    utils.spa._openWindow(url, options.newWindow);
                }
            },
    
            _redirectDelay: 0
        };
    })(gsoft.utils);

    (function($, utils) {
        gsoft.spa.viewModel = {
            // summary:
            //         When defined, the function will be called before applying the bindings.
            // returns:
            //         void or a jQuery promise.
            _beforeBind: null,
            
            // summary:
            //         When defined, the function will be called after the bindings are applyied.
            _afterBind: null,
            
            // summary:
            //         Bind the view model to the specified @element. This function is automatically called by the SPA
            //         when the route associated to the view model is entered.
            // description:
            //         If the "_beforeBind" function is defined, it will be called before applying the bindings.
            //         If the "_afterBind" function is defined, it will be called after ther binding are applyied.
            // element: DOM element
            //         The element to bind the view model to.
            // returns:
            //         A jQuery promise.
            bind: function(element) {
                gsoft.ensure(element, "element").isDomElement();
                
                this._element = element;
    
                if (utils.isFunction(this._beforeBind)) {
                    var promise = this._beforeBind.call(this);
                    
                    if (utils.spa._isjQueryPromise(promise)) {
                        var that = this;
                        
                        promise.done(function() {
                            that._applyBindings();
                        });
                        
                        return promise;
                    }
                }
                
                this._applyBindings();
                
                return this._createResolvingPromise();
            },
            
            _applyBindings: function() {
                ko.applyBindings(this, this._element);
                
                if (utils.isFunction(this._afterBind)) {
                    this._afterBind.call(this);
                }
            },
            
            _createResolvingPromise: function() {
                var deferred = new $.Deferred();
                deferred.resolve();
                
                return deferred.promise();
            },
            
            // summary:
            //         Validate if the view model can be disposed. When defined, this function is automatically called by the SPA
            //         when the route associated to the view model is exited.  
            // description:
            //         If true is returned, the route will be exited.
            //         If false is returned, the route will not be exited.
            //         If a promise is returned
            //              - when the promise is successfully resolved, the route will be exited.
            //              - when the promise fail, the route will not be exited.
            // returns:
            //         A boolean or a jQuery promise.
            canDispose: null,
            
            // summary:
            //         Dispose the view model resources. When defined, this function is automatically called by the SPA
            //         when the route associated to the view model is exited.
            dispose: null
        };
    })(jQuery,
       gsoft.utils);

})();