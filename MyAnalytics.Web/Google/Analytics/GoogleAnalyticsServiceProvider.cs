﻿using System;
using System.Security.Authentication;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;

using MyAnalytics.Web.Google.Authentication;

namespace MyAnalytics.Web.Google.Analytics
{
    /// <summary>
    /// Provide Google analytics service instances.
    /// </summary>
    public class GoogleAnalyticsServiceProvider
    {
        #region Fields

        private readonly GoogleAuthenticationService _authenticationService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleAnalyticsServiceProvider" /> class.
        /// </summary>
        /// <param name="authenticationService">The authentication service.</param>
        public GoogleAnalyticsServiceProvider(GoogleAuthenticationService authenticationService)
        {
            this._authenticationService = authenticationService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets an analytics service authorized for the specified username.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>An analytic service instance.</returns>
        public AnalyticsService Get(string userIdentifier)
        {
            var credential = this._authenticationService.Authorize(userIdentifier);
            if (credential != null)
            {
                return CreateAnalyticsService(credential);
            }

            throw new AuthenticationException("Could not get access to the user data.");
        }

        /// <summary>
        /// Gets an analytics service authorized for the specified username.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>An analytic service instance.</returns>
        public async Task<AnalyticsService> GetAsync(string userIdentifier)
        {
            var credential = await this._authenticationService.AuthorizeAsync(userIdentifier);
            if (credential != null)
            {
                return CreateAnalyticsService(credential);
            }

            throw new AuthenticationException("Could not get access to the user data.");
        }

        private static AnalyticsService CreateAnalyticsService(UserCredential credential)
        {
            return new AnalyticsService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = GoogleApiConstants.ApplicationName
            });
        }

        #endregion
    }
}