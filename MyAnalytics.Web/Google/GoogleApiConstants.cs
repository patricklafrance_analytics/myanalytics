﻿using System;

namespace MyAnalytics.Web.Google
{
    /// <summary>
    /// Google API constants.
    /// </summary>
    public static class GoogleApiConstants
    {
        #region Fields

        /// <summary>
        /// The default cancellation delay for a Google API call.
        /// </summary>
        public static readonly int DefaultApiCallCancellationDelay = 15 * 1000;

        /// <summary>
        /// The application name.
        /// </summary>
        public static readonly string ApplicationName = "My analytics";

        #endregion
    }
}