﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Plus.v1;

using Raven.Client;

namespace MyAnalytics.Web.Google.Authentication
{
    /// <summary>
    /// Google analytics authentication service
    /// </summary>
    public class GoogleAuthenticationService
    {
        #region Fields

        private readonly GoogleAuthenticationTokenRepository _googleAuthenticationTokenRepository;

        private readonly string _clientId;
        private readonly string _clientSecret;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleAuthenticationService" /> class.
        /// </summary>
        /// <param name="googleAuthenticationTokenRepository">The google authentication token repository.</param>
        /// <param name="clientId">The OAuth client id.</param>
        /// <param name="clientSecret">The OAuth client secret.</param>
        public GoogleAuthenticationService(GoogleAuthenticationTokenRepository googleAuthenticationTokenRepository, string clientId, string clientSecret)
        {
            this._googleAuthenticationTokenRepository = googleAuthenticationTokenRepository;
            this._clientId = clientId;
            this._clientSecret = clientSecret;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Authorize the specified user with Google.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>The user credential if he login sucessfully.</returns>
        public UserCredential Authorize(string userIdentifier)
        {
            return this.AuthorizeAsync(userIdentifier).Result;
        }

        /// <summary>
        /// Authorize the specified user with Google.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>The user credential if he login sucessfully.</returns>
        public Task<UserCredential> AuthorizeAsync(string userIdentifier)
        {
            return this.AuthorizeAsync(userIdentifier, GoogleApiConstants.DefaultApiCallCancellationDelay);
        }

        /// <summary>
        /// Authorize the specified user with Google.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="cancellationDelay">The delay to cancel the operation.</param>
        /// <returns>The user credential if he login sucessfully.</returns>
        public Task<UserCredential> AuthorizeAsync(string userIdentifier, int cancellationDelay)
        {
            try
            {
                return GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets { ClientId = this._clientId, ClientSecret = this._clientSecret },
                    new[] { AnalyticsService.Scope.AnalyticsReadonly, PlusService.Scope.UserinfoEmail },
                    userIdentifier,
                    new CancellationTokenSource(cancellationDelay).Token,
                    new AzureTableDataStore(this._googleAuthenticationTokenRepository));
            }
            catch (TaskCanceledException)
            {
                throw new TimeoutException("The login attempt took too long.");
            }
        }

        /// <summary>
        /// Revokes access to the specified user.
        /// </summary>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <returns>A TPL task.</returns>
        public async Task RevokeAsync(string userIdentifier)
        {
            var credential = await this.AuthorizeAsync(userIdentifier);

            if (credential != null)
            {
                try
                {
                    await credential.RevokeTokenAsync(new CancellationTokenSource(GoogleApiConstants.DefaultApiCallCancellationDelay).Token);
                }
                catch
                {
                    // Eat the exception, we dont care, the user is logging off anyway.
                }
            }
        }

        #endregion
    }
}