﻿//using System;
//using System.Threading.Tasks;

//using Google.Apis.Util.Store;

//using GSoft.G;

//using Raven.Abstractions.Commands;
//using Raven.Abstractions.Data;
//using Raven.Client;

//namespace MyAnalytics.Web.Google.Authentication
//{
//    /// <summary>
//    /// Raven DB data store for Google OAuth 2.0 authentication token.
//    /// </summary>
//    public class RavenDbDataStore : IDataStore
//    {
//        #region Fields

//        private readonly IDocumentStore _documentStore;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="RavenDbDataStore"/> class.
//        /// </summary>
//        /// <param name="documentStore">The document store.</param>
//        public RavenDbDataStore(IDocumentStore documentStore)
//        {
//            this._documentStore = documentStore;
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        /// Asynchronously stores the given value for the given key (replacing any existing value).
//        /// </summary>
//        /// <typeparam name="T">The type to store in the data store.</typeparam>
//        /// <param name="key">The key.</param>
//        /// <param name="value">The value to store.</param>
//        /// <returns>A task.</returns>
//        public Task StoreAsync<T>(string key, T value)
//        {
//            key.Ensure("key").IsNotNullOrEmpty("Key must have a value.");

//            using (var session = this._documentStore.OpenAsyncSession())
//            {
//                var documentKey = GoogleAuthenticationToken.GenerateKey(session, key);

//                session.StoreAsync(new GoogleAuthenticationToken
//                {
//                    Id = documentKey, 
//                    Value = value
//                });

//                return session.SaveChangesAsync();
//            }
//        }

//        /// <summary>
//        /// Asynchronously deletes the given key. The type is provided here as well because the "real" saved key should
//        /// contain type information as well, so the data store will be able to store the same key for different types.
//        /// </summary>
//        /// <typeparam name="T">The type to delete from the data store.</typeparam>
//        /// <param name="key">The key to delete.</param>
//        /// <returns>A task.</returns>
//        public Task DeleteAsync<T>(string key)
//        {
//            key.Ensure("key").IsNotNullOrEmpty("Key must have a value.");

//            using (var session = this._documentStore.OpenAsyncSession())
//            {
//                var documentKey = GoogleAuthenticationToken.GenerateKey(session, key);
//                session.Advanced.Defer(new DeleteCommandData { Key = documentKey });

//                return session.SaveChangesAsync();
//            }
//        }

//        /// <summary>
//        /// Asynchronously returns the stored value for the given key or <c>null</c> if not found.
//        /// </summary>
//        /// <typeparam name="T">The type to retrieve from the data store.</typeparam>
//        /// <param name="key">The key to retrieve its value.</param>
//        /// <returns>The stored object.</returns>
//        public async Task<T> GetAsync<T>(string key)
//        {
//            key.Ensure("key").IsNotNullOrEmpty("Key must have a value.");

//            using (var session = this._documentStore.OpenAsyncSession())
//            {
//                var documentKey = GoogleAuthenticationToken.GenerateKey(session, key);
//                var token = await session.LoadAsync<GoogleAuthenticationToken>(documentKey);

//                if (token != null)
//                {
//                    return (T)token.Value;
//                }

//                return default(T);
//            }
//        }

//        /// <summary>
//        /// Asynchronously clears all values in the data store.
//        /// </summary>
//        /// <returns>A task.</returns>
//        public Task ClearAsync()
//        {
//            using (var session = this._documentStore.OpenAsyncSession())
//            {
//                session.Advanced.DocumentStore.DatabaseCommands.DeleteByIndex("Raven/DocumentsByEntityName", new IndexQuery { Query = "Tag:" + typeof(GoogleAuthenticationToken).Name }).WaitForCompletionAsync();

//                return session.SaveChangesAsync();
//            }
//        }

//        #endregion
//    }
//}