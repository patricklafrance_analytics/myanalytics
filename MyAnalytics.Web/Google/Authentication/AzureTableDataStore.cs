﻿using System;
using System.Threading.Tasks;

using Google.Apis.Util.Store;

using GSoft.G;
using GSoft.G.Serialization.Json;

namespace MyAnalytics.Web.Google.Authentication
{
    public class AzureTableDataStore : IDataStore
    {
        #region Fields

        private readonly IJsonSerializer _jsonSerializer;
        private readonly GoogleAuthenticationTokenRepository _googleAuthenticationTokenRepository;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AzureTableDataStore"/> class.
        /// </summary>
        /// <param name="googleAuthenticationTokenRepository">The google authentication token table.</param>
        public AzureTableDataStore(GoogleAuthenticationTokenRepository googleAuthenticationTokenRepository)
        {
            this._jsonSerializer = JsonSerializerFactory.Create();
            this._googleAuthenticationTokenRepository = googleAuthenticationTokenRepository;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Asynchronously stores the given value for the given key (replacing any existing value).
        /// </summary>
        /// <typeparam name="T">The type to store in the data store.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value to store.</param>
        /// <returns>A task.</returns>
        public async Task StoreAsync<T>(string key, T value)
        {
            key.Ensure("key").IsNotNullOrEmpty("Key must have a value.");

            await this._googleAuthenticationTokenRepository.InsertAsync(new GoogleAuthenticationToken
            {
                PartitionKey = key, 
                RowKey = key, 
                Value = this._jsonSerializer.Serialize(value)
            });
        }

        /// <summary>
        /// Asynchronously deletes the given key. The type is provided here as well because the "real" saved key should
        /// contain type information as well, so the data store will be able to store the same key for different types.
        /// </summary>
        /// <typeparam name="T">The type to delete from the data store.</typeparam>
        /// <param name="key">The key to delete.</param>
        /// <returns>A task.</returns>
        public async Task DeleteAsync<T>(string key)
        {
            key.Ensure("key").IsNotNullOrEmpty("Key must have a value.");

            await this._googleAuthenticationTokenRepository.DeleteAsync(new GoogleAuthenticationToken
            {
                PartitionKey = key, 
                RowKey = key
            });
        }

        /// <summary>
        /// Asynchronously returns the stored value for the given key or <c>null</c> if not found.
        /// </summary>
        /// <typeparam name="T">The type to retrieve from the data store.</typeparam>
        /// <param name="key">The key to retrieve its value.</param>
        /// <returns>The stored object.</returns>
        public async Task<T> GetAsync<T>(string key)
        {
            key.Ensure("key").IsNotNullOrEmpty("Key must have a value.");

            var token = await this._googleAuthenticationTokenRepository.FetchAsync(key, key);
            if (token != null)
            {
                return this._jsonSerializer.Deserialize<T>(token.Value);
            }

            return default(T);
        }

        /// <summary>
        /// Asynchronously clears all values in the data store.
        /// </summary>
        /// <returns>A task.</returns>
        public Task ClearAsync()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}