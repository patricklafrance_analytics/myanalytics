﻿using System;

using MyAnalytics.Web.Common.Azure;

namespace MyAnalytics.Web.Google.Authentication
{
    public class GoogleAuthenticationTokenRepository : AzureTableRepository<GoogleAuthenticationToken>
    {
    }
}