﻿using System;

using Microsoft.WindowsAzure.Storage.Table;

using MyAnalytics.Web.Common.RavenDb;

using Raven.Client;

namespace MyAnalytics.Web.Google.Authentication
{
    ///// <summary>
    ///// Document to store a google analytics authentication token.
    ///// </summary>
    //public class GoogleAuthenticationToken
    //{
    //    #region Properties

    //    /// <summary>
    //    /// Gets or sets the id.
    //    /// </summary>
    //    public string Id { get; set; }

    //    /// <summary>
    //    /// Gets or sets the value.
    //    /// </summary>
    //    public object Value { get; set; }

    //    #endregion

    //    #region Methods

    //    /// <summary>
    //    /// Generates a key.
    //    /// </summary>
    //    /// <param name="session">The session.</param>
    //    /// <param name="id">The id.</param>
    //    /// <returns>A document key.</returns>
    //    public static string GenerateKey(IAsyncDocumentSession session, string id)
    //    {
    //        return DocumentKeyHelpers.GenerateTypedStringKey<GoogleAuthenticationToken>(session, id);
    //    }

    //    #endregion
    //}

    public class GoogleAuthenticationToken : TableEntity
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public string Value { get; set; }
    }
}