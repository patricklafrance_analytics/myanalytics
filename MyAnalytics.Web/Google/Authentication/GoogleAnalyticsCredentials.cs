﻿using System;

namespace MyAnalytics.Web.Google.Authentication
{
    /// <summary>
    /// Google analytics credentials. Those credentials are managed at https://console.developers.google.com.
    /// </summary>
    public static class GoogleAnalyticsCredentials
    {
        #region Nested Types

        /// <summary>
        /// Credentials for OAuth 2.0 authentication.
        /// </summary>
        public static class OAuth
        {
            #region Fields

            /// <summary>
            /// The client id.
            /// </summary>
            public const string ClientId = "698359944673-of9309fedeq7bmfgcjebujg9jl0h5vhg.apps.googleusercontent.com";

            /// <summary>
            /// The client secret.
            /// </summary>
            public const string ClientSecret = "GdoyFZpNwWKW1uWD2e_f7Bqy";

            #endregion
        }

        #endregion
    }
}