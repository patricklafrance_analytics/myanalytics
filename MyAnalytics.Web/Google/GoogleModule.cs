﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;
using MyAnalytics.Web.Google.Analytics;
using MyAnalytics.Web.Google.Authentication;
using MyAnalytics.Web.Google.Plus;

namespace MyAnalytics.Web.Google
{
    /// <summary>
    /// Bootstraps Google module.
    /// </summary>
    public class GoogleModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterType<GoogleAuthenticationService>(
                new ContainerControlledLifetimeManager(), 
                new InjectionFactory(c => new GoogleAuthenticationService(c.Resolve<GoogleAuthenticationTokenRepository>(), GoogleAnalyticsCredentials.OAuth.ClientId, GoogleAnalyticsCredentials.OAuth.ClientSecret)));

            container.RegisterType<GoogleAnalyticsServiceProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<GooglePlusServiceProvider>(new ContainerControlledLifetimeManager());
            container.RegisterType<GoogleAuthenticationTokenRepository>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}