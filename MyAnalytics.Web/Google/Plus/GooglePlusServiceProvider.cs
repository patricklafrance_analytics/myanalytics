﻿using System;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Plus.v1;
using Google.Apis.Services;

namespace MyAnalytics.Web.Google.Plus
{
    /// <summary>
    /// Provide Google plus service instances.
    /// </summary>
    public class GooglePlusServiceProvider
    {
        #region Methods

        /// <summary>
        /// Gets a plus service authorized for the specified crendetials.
        /// </summary>
        /// <param name="credential">The credential.</param>
        /// <returns>An analytic service instance.</returns>
        public PlusService Get(UserCredential credential)
        {
            return new PlusService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential, 
                ApplicationName = GoogleApiConstants.ApplicationName
            });
        }

        #endregion
    }
}