﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Google.Apis.Plus.v1;
using Google.Apis.Plus.v1.Data;

namespace MyAnalytics.Web.Google.Plus
{
    /// <summary>
    /// Plus service extension methods.
    /// </summary>
    public static class PlusServiceExtensions
    {
        #region Methods

        public static async Task<Person> GetUserAsync(this PlusService plusService)
        {
            return await plusService.People.Get("me").ExecuteAsync(new CancellationTokenSource(GoogleApiConstants.DefaultApiCallCancellationDelay).Token);
        }

        public static string GetAccountEmail(this Person person)
        {
            var email = person.Emails.First(a => a.Type == "account");

            return email.Value;
        }

        ///// <summary>
        ///// Gets the logged in account email address.
        ///// </summary>
        ///// <param name="plusService">The plus service instance.</param>
        ///// <returns>An email address.</returns>
        //public static async Task<string> GetAccountEmailAsync(this PlusService plusService)
        //{
        //    var person = await plusService.People.Get("me").ExecuteAsync(new CancellationTokenSource(GoogleApiConstants.DefaultApiCallCancellationDelay).Token);
        //    var email = person.Emails.First(a => a.Type == "account");

        //    return email.Value;
        //}

        //public static async Task<string> GetAccountIdAsync(this PlusService plusService)
        //{
        //    var person = await plusService.People.Get("me").ExecuteAsync(new CancellationTokenSource(GoogleApiConstants.DefaultApiCallCancellationDelay).Token);

        //    return person.Id;
        //}

        #endregion
    }
}