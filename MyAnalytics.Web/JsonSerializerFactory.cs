﻿using System;

using GSoft.G.Serialization.Json;

using Newtonsoft.Json;

namespace MyAnalytics.Web
{
    /// <summary>
    /// Create JSON serializer instances.
    /// </summary>
    public static class JsonSerializerFactory
    {
        #region Fields

        private static Func<JsonSerializerSettings> _settingsProvider;

        #endregion

        #region Methods

        /// <summary>
        /// Sets a provider for the JSON serializer settings.
        /// </summary>
        /// <param name="settingsProvider">The settings provider.</param>
        public static void SetJsonSettingsProvider(Func<JsonSerializerSettings> settingsProvider)
        {
            _settingsProvider = settingsProvider;
        }

        /// <summary>
        /// Creates a JSON serializer instance.
        /// </summary>
        /// <returns>A JSON serializer instance.</returns>
        public static IJsonSerializer Create()
        {
            return new JsonNetSerializer(GetSettings());
        }

        /// <summary>
        /// Creates a JSON serializer instance with debug settings.
        /// </summary>
        /// <returns>A JSON serialize instance.</returns>
        public static IJsonSerializer CreateForDebug()
        {
            var settings = GetSettings();
            settings.Formatting = Formatting.Indented;

            return new JsonNetSerializer(settings);
        }

        private static JsonSerializerSettings GetSettings()
        {
            if (_settingsProvider == null)
            {
                throw new InvalidOperationException("The settings provider has not been set.");
            }

            return _settingsProvider();
        }

        #endregion
    }
}