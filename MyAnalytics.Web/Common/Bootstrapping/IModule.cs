﻿using System;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Common.Bootstrapping
{
    /// <summary>
    /// Module to bootstrap an assembly.
    /// </summary>
    public interface IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        void Load(IUnityContainer container);

        #endregion
    }
}