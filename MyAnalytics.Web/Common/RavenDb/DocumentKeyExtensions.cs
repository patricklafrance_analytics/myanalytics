﻿using System;

using GSoft.G.Extensions;

using Raven.Client;

namespace MyAnalytics.Web.Common.RavenDb
{
    /// <summary>
    /// Raven DB document key generation utility functions.
    /// </summary>
    public static class DocumentKeyExtensions
    {
        #region Methods

        /// <summary>
        /// Builds the document key.
        /// </summary>
        /// <typeparam name="T">The document type.</typeparam>
        /// <param name="session">The session.</param>
        /// <param name="id">The document id.</param>
        /// <returns>A document key.</returns>
        public static string BuildDocumentKey<T>(this IDocumentSession session, object id)
        {
            return "{0}/{1}".FormatInvariant(session.Advanced.DocumentStore.Conventions.FindTypeTagName(typeof(T)), id);
        }

        /// <summary>
        /// Builds the document key.
        /// </summary>
        /// <typeparam name="T">The document type.</typeparam>
        /// <param name="session">The session.</param>
        /// <param name="id">The document id.</param>
        /// <returns>A document key.</returns>
        public static string BuildDocumentKey<T>(this IAsyncDocumentSession session, object id)
        {
            return "{0}/{1}".FormatInvariant(session.Advanced.DocumentStore.Conventions.FindTypeTagName(typeof(T)), id);
        }

        #endregion
    }
}