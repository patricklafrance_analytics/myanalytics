﻿using System;

using Raven.Client;

namespace MyAnalytics.Web.Common.RavenDb
{
    /// <summary>
    /// Document key helpers.
    /// </summary>
    public static class DocumentKeyHelpers
    {
        #region Methods

        /// <summary>
        /// Generates a document typed string key.
        /// </summary>
        /// <typeparam name="T">The document type</typeparam>
        /// <param name="session">The session.</param>
        /// <param name="id">The id.</param>
        /// <returns>A document key.</returns>
        public static string GenerateTypedStringKey<T>(IAsyncDocumentSession session, string id)
        {
            return session.BuildDocumentKey<T>(id.ToLowerInvariant());
        }

        #endregion
    }
}