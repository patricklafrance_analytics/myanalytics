﻿using System;

using Raven.Client.Document;
using Raven.Imports.Newtonsoft.Json;

namespace MyAnalytics.Web.Common.RavenDb
{
    /// <summary>
    /// Provider access to a Raven DB JSON serializer. This JSON serialize is instanciated with the document convention that has been configured
    /// for the Raven DB document store.
    /// </summary>
    public static class RavenDbJsonSerializerProvider
    {
        // Creating a RavenDb JSON serializer is not that trivial because of the all conventions, we use a Lazy to save some computing.
        #region Fields

        private static readonly Lazy<JsonSerializer> JsonSerializer = new Lazy<JsonSerializer>(() => new DocumentConvention().CreateSerializer());

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value.
        /// </summary>
        public static JsonSerializer Value
        {
            get
            {
                return JsonSerializer.Value;
            }
        }

        #endregion
    }
}