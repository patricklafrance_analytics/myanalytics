﻿using System;
using System.IO;
using System.Threading.Tasks;

using Raven.Client;
using Raven.Imports.Newtonsoft.Json;
using Raven.Json.Linq;

namespace MyAnalytics.Web.Common.RavenDb
{
    /// <summary>
    /// Raven DB load utility functions.
    /// </summary>
    public static class LoadExtensions
    {
        #region Methods

        /// <summary>
        /// Loads a Raven DB document and project it into the specified type.
        /// </summary>
        /// <typeparam name="T">The type of the resulting object.</typeparam>
        /// <param name="session">The Raven DB session.</param>
        /// <param name="documentKey">The document id.</param>
        /// <returns>An instance of T.</returns>
        public static async Task<T> LoadProjectionAsync<T>(this IAsyncDocumentSession session, string documentKey)
            where T : class
        {
            var document = await session.LoadAsync<RavenJObject>(documentKey);
            if (document != null)
            {
                // Why should we track changes for a read only document?
                session.Advanced.Evict(document);

                using (var textReader = new StringReader(document.ToString()))
                {
                    using (var jsonReader = new JsonTextReader(textReader))
                    {
                        var obj = RavenDbJsonSerializerProvider.Value.Deserialize<T>(jsonReader);

                        return obj;
                    }
                }
            }

            return null;
        }

        #endregion
    }
}