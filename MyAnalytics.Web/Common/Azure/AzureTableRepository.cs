﻿using System;
using System.Configuration;
using System.Threading.Tasks;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace MyAnalytics.Web.Common.Azure
{
    public abstract class AzureTableRepository<T>
        where T : ITableEntity
    {
        #region Fields

        private readonly Lazy<CloudTable> _cloudTable;

        #endregion

        #region Constructors

        protected AzureTableRepository()
        {
            var storageAccount = CloudStorageAccount.Parse(ConfigurationManager.ConnectionStrings["AzureTableStorage"].ConnectionString);
            var tableClient = storageAccount.CreateCloudTableClient();

            this._cloudTable = new Lazy<CloudTable>(() =>
            {
                var instance = tableClient.GetTableReference(typeof(T).Name);
                instance.CreateIfNotExists();

                return instance;
            });
        }

        #endregion

        #region Methods

        public CloudTable GetTable()
        {
            return this._cloudTable.Value;
        }

        public async Task InsertAsync(T entity)
        {
            await this.GetTable().ExecuteAsync(TableOperation.InsertOrReplace(entity));
        }

        public async Task DeleteAsync(T entity)
        {
            if (string.IsNullOrEmpty(entity.ETag))
            {
                entity.ETag = "*";
            }

            await this.GetTable().ExecuteAsync(TableOperation.Delete(entity));
        }

        public async Task<T> FetchAsync(string partitionKey, string rowKey)
        {
            var result = await this.GetTable().ExecuteAsync(TableOperation.Retrieve<T>(partitionKey, rowKey));

            if (result.Result != null)
            {
                return (T)result.Result;
            }

            return default(T);
        }

        #endregion
    }
}