﻿using System;

using GSoft.G.Extensions;

using MyAnalytics.Web.Common.Extensions;

namespace MyAnalytics.Web.Common.Security
{
    /// <summary>
    /// Authenticated user session DTO.
    /// </summary>
    public class UserSession
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UserSession"/> class.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <param name="userEmail">The user email.</param>
        public UserSession(string sessionId, string userEmail)
        {
            this.SessionId = sessionId;
            this.UserEmail = userEmail;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the user email.
        /// </summary>
        public string UserEmail { get; private set; }

        /// <summary>
        /// Gets or sets the session id.
        /// </summary>
        public string SessionId { get; private set; }

        /// <summary>
        /// Generates a session id.
        /// </summary>
        /// <returns>A session id.</returns>
        public static string GenerateSessionId()
        {
            return Guid.NewGuid().ToInvariantString();
        }

        #endregion
    }
}