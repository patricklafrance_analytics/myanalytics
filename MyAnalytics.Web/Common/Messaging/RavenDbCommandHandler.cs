﻿using System;

using Raven.Client;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Base command handler for handlers that use RavenDb as its data store.
    /// </summary>
    /// <typeparam name="TCommand">The type of the command.</typeparam>
    public abstract class RavenDbCommandHandler<TCommand> : IHandleCommand<TCommand>
        where TCommand : ICommand
    {
        #region Fields

        private readonly IDocumentSessionProvider _documentSessionProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RavenDbCommandHandler{TCommand}" /> class.
        /// </summary>
        /// <param name="documentSessionProvider">The document session manager.</param>
        protected RavenDbCommandHandler(IDocumentSessionProvider documentSessionProvider)
        {
            this._documentSessionProvider = documentSessionProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        public void Handle(TCommand command)
        {
            var session = this._documentSessionProvider.CurrentSession(command.CommandId);

            this.HandleCommand(command, session);
        }

        /// <summary>
        /// Handles the command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="session">The session.</param>
        protected abstract void HandleCommand(TCommand command, IDocumentSession session);

        #endregion
    }
}