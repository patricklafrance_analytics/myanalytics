﻿using System;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Manages RavenDb sessions.
    /// </summary>
    public interface IDocumentSessionManager
    {
        #region Methods

        /// <summary>
        /// Starts the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        void StartSession(Guid sessionId);

        /// <summary>
        /// Completes the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        void CompleteSession(Guid sessionId);

        /// <summary>
        /// Disposes the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        void DisposeSession(Guid sessionId);

        #endregion
    }
}