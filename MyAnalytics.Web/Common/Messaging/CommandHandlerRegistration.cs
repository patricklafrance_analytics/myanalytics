﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Infrastructure.Messaging;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Command handler registration facilities.
    /// </summary>
    public static class CommandHandlerRegistration
    {
        #region Methods

        /// <summary>
        /// Registers a command to Unity.
        /// </summary>
        /// <typeparam name="TCommand">The type of the event.</typeparam>
        /// <typeparam name="TCommandHandler">The type of the command handler.</typeparam>
        /// <param name="container">The container.</param>
        public static void RegisterCommandType<TCommand, TCommandHandler>(this IUnityContainer container)
            where TCommand : ICommand
            where TCommandHandler : IHandleCommand<TCommand>
        {
            container.RegisterType<IHandleCommand<TCommand>, TCommandHandler>(new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Registers a command that use Raven DB to Unity.
        /// </summary>
        /// <typeparam name="TCommand">The type of the event.</typeparam>
        /// <typeparam name="TCommandHandler">The type of the command handler.</typeparam>
        /// <param name="container">The container.</param>
        public static void RegisterRavenDbCommandType<TCommand, TCommandHandler>(this IUnityContainer container)
            where TCommand : ICommand
            where TCommandHandler : IHandleCommand<TCommand>
        {
            container.RegisterType<IHandleCommand<TCommand>>(
                new ContainerControlledLifetimeManager(), 
                new InjectionFactory(c => new RavenDbTransactionnalHandler<TCommand>(c.Resolve<IDocumentSessionManager>(), c.Resolve<TCommandHandler>())));
        }

        #endregion
    }
}