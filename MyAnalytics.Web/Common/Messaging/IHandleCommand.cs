﻿using System;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Base command handler interface.
    /// </summary>
    /// <typeparam name="TCommand">The type of the command.</typeparam>
    public interface IHandleCommand<in TCommand>
        where TCommand : ICommand
    {
        #region Methods

        /// <summary>
        /// Handles the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        void Handle(TCommand command);

        #endregion
    }
}