﻿using System;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// The base command.
    /// </summary>
    public abstract class Command : ICommand
    {
        #region Fields

        private Guid _commandId = Guid.NewGuid();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the command id.
        /// </summary>
        public Guid CommandId
        {
            get
            {
                return this._commandId;
            }

            set
            {
                this._commandId = value;
            }
        }

        #endregion
    }
}