﻿using System;

using Raven.Client;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Manages the document sessions throughout the handling of commands.
    /// </summary>
    public interface IDocumentSessionProvider
    {
        #region Methods

        /// <summary>
        /// Retreives the current session based on the session id.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <returns>The document session.</returns>
        IDocumentSession CurrentSession(Guid sessionId);

        #endregion
    }
}