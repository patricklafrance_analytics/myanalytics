﻿using System;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Interface to send command to the appropriate handler.
    /// </summary>
    public interface ICommandDispatcher
    {
        #region Methods

        /// <summary>
        /// Sends the command to the appropriate command handler.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <typeparam name="TCommand">The type of the command.</typeparam>
        void Send<TCommand>(TCommand command) where TCommand : class, ICommand;

        #endregion
    }
}