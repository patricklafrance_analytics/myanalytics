﻿using System;

namespace MyAnalytics.Web.Common.Messaging
{
    /// <summary>
    /// Base command interface.
    /// </summary>
    public interface ICommand
    {
        #region Properties

        /// <summary>
        /// Gets the command id.
        /// </summary>
        Guid CommandId { get; }

        #endregion
    }
}