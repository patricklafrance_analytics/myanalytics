﻿using System;

namespace MyAnalytics.Web.Common.Extensions
{
    /// <summary>
    /// Extends the string object.
    /// </summary>
    public static class StringExtensions
    {
        #region Methods

        /// <summary>
        /// Transform an object to his invariant string representation.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns>An invariant string.</returns>
        public static string ToInvariantString(this object target)
        {
            return target.ToString();
        }

        #endregion
    }
}