﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;

namespace MyAnalytics.Web.Accounts
{
    /// <summary>
    /// Boostraps the accounts module.
    /// </summary>
    public class AccountsModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterType<SignUpService>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}