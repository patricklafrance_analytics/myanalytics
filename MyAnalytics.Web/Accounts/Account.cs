﻿using System;

using MyAnalytics.Web.Common.RavenDb;

using Raven.Client;

namespace MyAnalytics.Web.Accounts
{
    /// <summary>
    /// Document to store an application account.
    /// </summary>
    public class Account
    {
        #region Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        #endregion
    }
}