﻿using System;
using System.Threading.Tasks;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Plus.v1.Data;

using MyAnalytics.Web.Common.RavenDb;
using MyAnalytics.Web.Common.Security;
using MyAnalytics.Web.Google.Authentication;
using MyAnalytics.Web.Google.Plus;

using Raven.Client;

namespace MyAnalytics.Web.Accounts
{
    /// <summary>
    /// Sign up service.
    /// </summary>
    public class SignUpService
    {
        #region Fields

        private readonly IDocumentStore _documentStore;
        private readonly GoogleAuthenticationService _googleAuthenticationService;
        private readonly GooglePlusServiceProvider _googlePlusServiceProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SignUpService" /> class.
        /// </summary>
        /// <param name="documentStore">The document store.</param>
        /// <param name="googleAuthenticationService">The google authentication service.</param>
        /// <param name="googlePlusServiceProvider">The google plus service provider.</param>
        public SignUpService(IDocumentStore documentStore, GoogleAuthenticationService googleAuthenticationService, GooglePlusServiceProvider googlePlusServiceProvider)
        {
            this._documentStore = documentStore;
            this._googleAuthenticationService = googleAuthenticationService;
            this._googlePlusServiceProvider = googlePlusServiceProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sign up a new user.
        /// </summary>
        /// <returns>A session if the user has been signed up. False if the user could not be authentified.</returns>
        public async Task<UserSession> SignUpAsync()
        {
            var credential = await this.TryAuthenticateToGoogle(UserSession.GenerateSessionId());
            if (credential != null)
            {
                // var accountEmail = await this.RetrieveAccountEmail(credential);
                var user = await this.RetrieveUser(credential);
                var accountEmail = user.GetAccountEmail();

                using (var documentSession = this._documentStore.OpenAsyncSession())
                {
                    var documentKey = DocumentKeyHelpers.GenerateTypedStringKey<Account>(documentSession, user.Id.ToLowerInvariant());
                    var existingAccount = await documentSession.LoadAsync<Account>(documentKey);

                    if (existingAccount == null)
                    {
                        await documentSession.StoreAsync(new Account
                        {
                            Id = documentKey, 
                            Email = accountEmail
                        });

                        await documentSession.SaveChangesAsync();
                    }
                }

                return new UserSession(credential.UderId, accountEmail);
            }

            return null;
        }

        private async Task<Person> RetrieveUser(UserCredential credential)
        {
            var googlePlusService = this._googlePlusServiceProvider.Get(credential);

            return await googlePlusService.GetUserAsync();
        }

        private async Task<UserCredential> TryAuthenticateToGoogle(string userIdentifier)
        {
            var cancellationDelay = 120 * 1000;

            return await this._googleAuthenticationService.AuthorizeAsync(userIdentifier, cancellationDelay);
        }

        #endregion
    }
}