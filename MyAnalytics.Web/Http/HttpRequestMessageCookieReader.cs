﻿using System;
using System.Linq;
using System.Net.Http;

using GSoft.G;

using MyAnalytics.Web.Infrastructure.Http;

namespace MyAnalytics.Web.Http
{
    /// <summary>
    /// System.Net stack cookie reader.
    /// </summary>
    public class HttpRequestMessageCookieReader : ICookieReader
    {
        #region Fields

        private readonly HttpRequestMessage _request;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpRequestMessageCookieReader"/> class.
        /// </summary>
        /// <param name="request">The HTTP request.</param>
        public HttpRequestMessageCookieReader(HttpRequestMessage request)
        {
            this._request = request;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reads a cookie.
        /// </summary>
        /// <param name="cookieName">The name of the cookie.</param>
        /// <returns>The cookie value.</returns>
        public string Read(string cookieName)
        {
            cookieName.Ensure("cookieName").IsNotNullOrEmpty();

            var cookie = this._request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie == null)
            {
                return string.Empty;
            }

            return cookie[cookieName].Value;
        }

        #endregion
    }
}