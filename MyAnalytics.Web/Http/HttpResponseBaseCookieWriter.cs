﻿using System;
using System.Web;

using GSoft.G;

using MyAnalytics.Web.Infrastructure.Http;

namespace MyAnalytics.Web.Http
{
    /// <summary>
    /// System.Web stack cookie writer.
    /// </summary>
    public class HttpResponseBaseCookieWriter : ICookieWriter
    {
        #region Fields

        private readonly HttpResponseBase _response;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpResponseBaseCookieWriter"/> class.
        /// </summary>
        /// <param name="response">The HTTP response.</param>
        public HttpResponseBaseCookieWriter(HttpResponseBase response)
        {
            this._response = response;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Writes a cookie.
        /// </summary>
        /// <param name="cookieName">The name of the cookie.</param>
        /// <param name="value">The cookie value.</param>
        public void Write(string cookieName, string value)
        {
            cookieName.Ensure("cookieName").IsNotNullOrEmpty();
            value.Ensure("value").IsNotNullOrEmpty();

            var cookie = new HttpCookie(cookieName)
            {
                HttpOnly = true, 
                Value = value, 
                Expires = DateTime.Now.AddYears(1)
            };

            this._response.Cookies.Add(cookie);
        }

        #endregion
    }
}