﻿using System;
using System.Web;

using GSoft.G;

using MyAnalytics.Web.Infrastructure.Http;

namespace MyAnalytics.Web.Http
{
    /// <summary>
    /// System.Web stack cookie reader.
    /// </summary>
    public class HttpRequestBaseCookieReader : ICookieReader
    {
        #region Fields

        private readonly HttpRequestBase _request;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpRequestBaseCookieReader"/> class.
        /// </summary>
        /// <param name="request">The HTTP request.</param>
        public HttpRequestBaseCookieReader(HttpRequestBase request)
        {
            this._request = request;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reads a cookie.
        /// </summary>
        /// <param name="cookieName">The name of the cookie.</param>
        /// <returns>The cookie value.</returns>
        public string Read(string cookieName)
        {
            cookieName.Ensure("cookieName").IsNotNullOrEmpty();

            var cookie = this._request.Cookies[cookieName];
            if (cookie == null)
            {
                return string.Empty;
            }

            return cookie.Value;
        }

        #endregion
    }
}