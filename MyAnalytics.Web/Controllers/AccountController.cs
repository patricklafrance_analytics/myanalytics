﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;

using MyAnalytics.Web.Accounts;
using MyAnalytics.Web.Common.Security;
using MyAnalytics.Web.Core.Authentication;
using MyAnalytics.Web.Http;
using MyAnalytics.Web.Infrastructure.Security;
using MyAnalytics.Web.Security;

namespace MyAnalytics.Web.Controllers
{
    /// <summary>
    /// Controller responsible of authentication.
    /// </summary>
    public class AccountController : Controller
    {
        #region Fields

        private readonly AuthenticationService _authenticationService;
        private readonly SignUpService _signUpService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController" /> class.
        /// </summary>
        /// <param name="authenticationService">The authentication service.</param>
        /// <param name="signUpService">The sign up service.</param>
        public AccountController(AuthenticationService authenticationService, SignUpService signUpService)
        {
            this._authenticationService = authenticationService;
            this._signUpService = signUpService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sign up a new user.
        /// </summary>
        /// <returns>A view.</returns>
        [AllowAnonymous]
        public ActionResult SignUp()
        {
            return this.View("SignUp", new SignUpModel());
        }

        /// <summary>
        /// Sign up a new user.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns>A view.</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> SignUp(SignUpModel model)
        {
            if (this.ModelState.IsValid)
            {
                if (await this.PerformSignUp())
                {
                    return this.Redirect("/");
                }
            }

            return this.View("SignUp", model);
        }

        /// <summary>
        /// Gets the login form.
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns>A view.</returns>
        [AllowAnonymous]
        public ActionResult LogOn(Uri returnUrl)
        {
            this.ViewBag.ReturnUrl = returnUrl;

            return this.View("LogOn");
        }

        /// <summary>
        /// Log on a user.
        /// </summary>
        /// <param name="model">The login model used for authenticat.</param>
        /// <param name="returnUrl">The return URL to redirect to if the authentication is successful.</param>
        /// <returns>A view.</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> LogOn(LogOnModel model, Uri returnUrl)
        {
            if (this.ModelState.IsValid)
            {
                if (await this.PerformLogOn())
                {
                    return this.RedirectUserAfterLogOn(returnUrl, model.ReturnPath);
                }

                this.ModelState.AddModelError("error", "Invalid credentials");
            }

            this.ViewBag.ReturnUrl = returnUrl;

            return this.View("LogOn", model);
        }

        /// <summary>
        /// Logs out the logged in user.
        /// </summary>
        /// <returns>A view.</returns>
        [AllowAnonymous]
        public async Task<ActionResult> LogOff()
        {
            var authenticationCookie = new FormsAuthenticationCookie(new HttpRequestBaseCookieReader(this.Request));

            await this._authenticationService.LogOff(authenticationCookie.Session);

            FormsAuthentication.SignOut();

            return this.Redirect("/");
        }

        private void CreateAuthenticationCookie(UserSession session)
        {
            var cookie = new FormsAuthenticationCookie(session);
            cookie.Write(new HttpResponseBaseCookieWriter(this.Response));
        }

        private async Task<bool> PerformSignUp()
        {
            var session = await this._signUpService.SignUpAsync();
            if (session != null)
            {
                this.CreateAuthenticationCookie(session);

                return true;
            }

            return false;
        }

        private async Task<bool> PerformLogOn()
        {
            var session = await this._authenticationService.LogOnAsync();
            if (session != null)
            {
                this.CreateAuthenticationCookie(session);

                return true;
            }

            return false;
        }

        private ActionResult RedirectUserAfterLogOn(Uri returnUrl, string returnPath)
        {
            if (returnUrl != null && !string.IsNullOrEmpty(returnUrl.OriginalString))
            {
                if (string.IsNullOrEmpty(returnPath))
                {
                    return this.Redirect(returnUrl.OriginalString);
                }

                return this.Redirect(returnUrl.OriginalString + returnPath);
            }

            return this.Redirect("~/");
        }

        #endregion
    }
}