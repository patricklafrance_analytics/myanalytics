﻿using System;
using System.Web.Mvc;

using MyAnalytics.Web.Security;

namespace MyAnalytics.Web.Controllers
{
    /// <summary>
    /// Base MVC controller.
    /// </summary>
    [Authorize]
    [SetRequestContext]
    public abstract class ControllerBase : Controller
    {
    }
}