﻿using System;
using System.Web.Mvc;

namespace MyAnalytics.Web.Controllers
{
    /// <summary>
    /// Provides templates for the dashboard context.
    /// </summary>
    public class DashboardController : ControllerBase
    {
        #region Methods

        /// <summary>
        /// Gets the template for profiles page.
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult Profiles()
        {
            return this.View("Profiles");
        }

        /// <summary>
        /// Views the dashboard.
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult ViewDashboard()
        {
            return this.View("ViewDashboard");
        }

        #endregion
    }
}