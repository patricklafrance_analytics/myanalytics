﻿using System;
using System.Web.Mvc;

using MyAnalytics.Web.Http;
using MyAnalytics.Web.Infrastructure.Security;

namespace MyAnalytics.Web.Controllers
{
    /// <summary>
    /// The application controller.
    /// </summary>
    public class ApplicationController : ControllerBase
    {
        #region Methods

        /// <summary>
        /// Gets the template for the index.
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult Index()
        {
            var authenticationCookie = new FormsAuthenticationCookie(new HttpRequestBaseCookieReader(this.Request));

            this.ViewBag.UserEmail = authenticationCookie.Session.UserEmail;

            return this.View("Index");
        }

        /// <summary>
        /// Gets the template for the 404 page.
        /// </summary>
        /// <returns>A view.</returns>
        public ActionResult NotFound()
        {
            return this.View("NotFound");
        }

        #endregion
    }
}