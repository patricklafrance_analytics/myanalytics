﻿using System;

namespace MyAnalytics.Web.Security
{
    /// <summary>
    /// Login form DTO.
    /// </summary>
    public class LogOnModel
    {
        #region Properties

        /// <summary>
        /// The return path for the SPA.
        /// </summary>
        public string ReturnPath { get; set; }

        #endregion
    }
}