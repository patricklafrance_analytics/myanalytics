﻿using System;
using System.Web.Mvc;

using MyAnalytics.Web.Http;
using MyAnalytics.Web.Infrastructure.Security;

namespace MyAnalytics.Web.Security
{
    /// <summary>
    /// Load the request context and set it in the view bag.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public sealed class SetRequestContextAttribute : ActionFilterAttribute
    {
        #region Methods

        /// <summary>
        /// Called by the ASP.NET MVC framework before the action method executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var authenticationCookie = new FormsAuthenticationCookie(new HttpRequestBaseCookieReader(filterContext.RequestContext.HttpContext.Request));

            filterContext.Controller.ViewBag.User = authenticationCookie.Session;
        }

        #endregion
    }
}