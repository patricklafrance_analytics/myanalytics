﻿using System;
using System.Web.Security;

using GSoft.G;

using MyAnalytics.Web.Common.Security;
using MyAnalytics.Web.Infrastructure.Http;

namespace MyAnalytics.Web.Infrastructure.Security
{
    /// <summary>
    /// Cookie for the user form authentification.
    /// </summary>
    public class FormsAuthenticationCookie
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FormsAuthenticationCookie"/> class.
        /// </summary>
        /// <param name="reader">The cookie reader.</param>
        public FormsAuthenticationCookie(ICookieReader reader)
        {
            reader.Ensure("reader").IsNotNull();

            this.Session = Read(reader);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FormsAuthenticationCookie"/> class.
        /// </summary>
        /// <param name="session">The session informations.</param>
        public FormsAuthenticationCookie(UserSession session)
        {
            session.Ensure("session").IsNotNull();

            this.Session = session;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the session informations.
        /// </summary>
        public UserSession Session { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Writes the cookie.
        /// </summary>
        /// <param name="writer">The cookie writer.</param>
        public void Write(ICookieWriter writer)
        {
            var cookie = FormsAuthentication.GetAuthCookie(this.Session.UserEmail, true);
            var ticket = this.CreateNewTicket(FormsAuthentication.Decrypt(cookie.Value));

            writer.Write(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
        }

        private static UserSession Read(ICookieReader reader)
        {
            var cookieValue = reader.Read(FormsAuthentication.FormsCookieName);

            if (!string.IsNullOrEmpty(cookieValue))
            {
                var ticket = FormsAuthentication.Decrypt(cookieValue);
                if (ticket != null)
                {
                    if (!ticket.Expired)
                    {
                        var serializer = JsonSerializerFactory.Create();

                        return serializer.Deserialize<UserSession>(ticket.UserData);
                    }
                }
            }

            return null;
        }

        private FormsAuthenticationTicket CreateNewTicket(FormsAuthenticationTicket oldTicket)
        {
            var serializer = JsonSerializerFactory.Create();

            var newTicket = new FormsAuthenticationTicket(
                oldTicket.Version, 
                oldTicket.Name, 
                oldTicket.IssueDate, 
                oldTicket.Expiration, 
                true, 
                serializer.Serialize(this.Session));

            return newTicket;
        }

        #endregion
    }
}