﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;

namespace MyAnalytics.Web.Infrastructure.Mvc
{
    /// <summary>
    /// Bootstrapper for MVC.
    /// </summary>
    public static class MvcBootstrapper
    {
        #region Methods

        /// <summary>
        /// Starts bootstrapping.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void Bootstrap(IUnityContainer container)
        {
            ConfigureRoutes(RouteTable.Routes);

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static void ConfigureRoutes(RouteCollection routes)
        {
            routes.LowercaseUrls = true;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default", 
                url: "{controller}/{action}/{id}", 
                defaults: new { controller = "Application", action = "Index", id = UrlParameter.Optional });
        }

        #endregion
    }
}