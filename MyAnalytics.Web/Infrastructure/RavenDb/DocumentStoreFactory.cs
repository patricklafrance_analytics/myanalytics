﻿using System;

using Raven.Client;
using Raven.Client.Document;

namespace MyAnalytics.Web.Infrastructure.RavenDb
{
    /// <summary>
    /// Raven DB document store factory.
    /// </summary>
    public static class DocumentStoreFactory
    {
        #region Methods

        /// <summary>
        /// Creates the document store using the config file connection string name.
        /// </summary>
        /// <returns>The document store</returns>
        public static IDocumentStore CreateDocumentStore()
        {
            return new DocumentStore { ConnectionStringName = "MyAnalytics" };
        }

        #endregion
    }
}