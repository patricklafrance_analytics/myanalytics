﻿using System;
using System.Collections.Generic;
using System.Reflection;

using GSoft.G;

using Raven.Client;
using Raven.Client.Indexes;

namespace MyAnalytics.Web.Infrastructure.RavenDb
{
    /// <summary>
    /// Raven DB document store initialize.
    /// </summary>
    public static class DocumentStoreInitializer
    {
        #region Methods

        /// <summary>
        /// Initializes the specified document store.
        /// </summary>
        /// <param name="documentStore">The document store.</param>
        /// <param name="indexAssemblies">The index assemblies.</param>
        /// <returns>An initialized document store.</returns>
        public static IDocumentStore Initialize(IDocumentStore documentStore, IEnumerable<Assembly> indexAssemblies)
        {
            documentStore.Ensure("documentStore").IsNotNull();
            indexAssemblies.Ensure("indexAssemblies").IsNotNull();

            documentStore.Initialize();

            RegisterIndexes(documentStore, indexAssemblies);

            return documentStore;
        }

        private static void RegisterIndexes(IDocumentStore documentStore, IEnumerable<Assembly> assemblies)
        {
            foreach (var assembly in assemblies)
            {
                IndexCreation.CreateIndexes(assembly, documentStore);
            }
        }

        #endregion
    }
}