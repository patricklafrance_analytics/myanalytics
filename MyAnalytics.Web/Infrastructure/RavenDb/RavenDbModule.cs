﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;
using MyAnalytics.Web.Infrastructure.Bootstrapping;

using Raven.Client;

namespace MyAnalytics.Web.Infrastructure.RavenDb
{
    /// <summary>
    /// Bootstraps the Raven DB infrastructure.
    /// </summary>
    public class RavenDbModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterType<IDocumentStore>(
                new ContainerControlledLifetimeManager(), 
                new InjectionFactory(x => DocumentStoreInitializer.Initialize(DocumentStoreFactory.CreateDocumentStore(), AssemblyLocator.GetAssemblies())));
        }

        #endregion
    }
}