﻿using System;
using System.Reflection;

using Newtonsoft.Json.Serialization;

namespace MyAnalytics.Web.Infrastructure.WebApi
{
    /// <summary>
    /// JSON.NET custom string value provider to write null string as ""  instead of null.
    /// </summary>
    public class StringValueProvider : IValueProvider
    {
        #region Fields

        private readonly IValueProvider _dynamicValueProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StringValueProvider" /> class.
        /// </summary>
        /// <param name="memberInfo">The member info.</param>
        public StringValueProvider(MemberInfo memberInfo)
        {
            this._dynamicValueProvider = new DynamicValueProvider(memberInfo);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="target">The target to get the value from.</param>
        /// <returns>
        /// The value.
        /// </returns>
        public object GetValue(object target)
        {
            return this._dynamicValueProvider.GetValue(target) ?? string.Empty;
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="target">The target to set the value on.</param>
        /// <param name="value">The value to set on the target.</param>
        public void SetValue(object target, object value)
        {
            var str = value as string;

            this._dynamicValueProvider.SetValue(target, string.IsNullOrEmpty(str) ? string.Empty : value);
        }

        #endregion
    }
}