﻿using System;
using System.Web.Http.Dependencies;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Infrastructure.WebApi
{
    /// <summary>
    /// Web API dependency resolver.
    /// </summary>
    public class WebApiUnityDependencyResolver : WebApiUnityDependencyScope, IDependencyResolver
    {
        #region Fields

        private readonly IDependencyScope _dependencyScope;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiUnityDependencyResolver"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public WebApiUnityDependencyResolver(IUnityContainer container)
            : base(container)
        {
            this._dependencyScope = new WebApiUnityDependencyScope(container);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts a resolution scope.
        /// </summary>
        /// <returns>
        /// The dependency scope.
        /// </returns>
        public IDependencyScope BeginScope()
        {
            return this._dependencyScope;
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool managed)
        {
            base.Dispose(managed);

            if (this._dependencyScope != null)
            {
                this._dependencyScope.Dispose();
            }
        }

        #endregion
    }
}