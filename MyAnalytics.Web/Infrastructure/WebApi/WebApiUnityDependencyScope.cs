﻿using System;
using System.Collections.Generic;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Infrastructure.WebApi
{
    /// <summary>
    /// Web API unity dependency scope.
    /// </summary>
    public class WebApiUnityDependencyScope : IDependencyScope
    {
        #region Fields

        private readonly IUnityContainer _container;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="WebApiUnityDependencyScope"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public WebApiUnityDependencyScope(IUnityContainer container)
        {
            this._container = container;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Retrieves a service from the scope.
        /// </summary>
        /// <param name="serviceType">The service to be retrieved.</param>
        /// <returns>
        /// The retrieved service.
        /// </returns>
        public object GetService(Type serviceType)
        {
            // HACK: If the type requested is a controller, we resolve it directly, so that we don't have to register the controllers
            // individually, otherwise, we resolve the requested type only if we have a registration for it.
            // See Guillaume Roy or David Létourneau for more details.
            if (typeof(IHttpController).IsAssignableFrom(serviceType))
            {
                return this._container.Resolve(serviceType);
            }

            if (this.CanResolve(serviceType))
            {
                return this._container.Resolve(serviceType);
            }

            return null;
        }

        /// <summary>
        /// Retrieves a collection of services from the scope.
        /// </summary>
        /// <param name="serviceType">The collection of services to be retrieved.</param>
        /// <returns>
        /// The retrieved collection of services.
        /// </returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this._container.ResolveAll(serviceType);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="managed"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool managed)
        {
        }

        private bool CanResolve(Type type)
        {
            // Non abstract classes are always resolved. For interfaces and abstract classes we must first validate
            // that they are registered in the container otherwise the an exception would be thrown when we try to resolve
            // the type.
            if (type.IsInterface || type.IsAbstract)
            {
                var isRegistered = this._container.IsRegistered(type);

                // Maybe this is a generic type ? Validate if the generic type is registered in the container.
                if (!isRegistered && type.IsGenericType)
                {
                    isRegistered = this._container.IsRegistered(type.GetGenericTypeDefinition());
                }

                return isRegistered;
            }

            return true;
        }

        #endregion
    }
}