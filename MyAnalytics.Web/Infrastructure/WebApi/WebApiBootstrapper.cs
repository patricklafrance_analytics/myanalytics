﻿using System;
using System.Web.Http;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Infrastructure.WebApi
{
    /// <summary>
    /// Bootstrapper for Web API.
    /// </summary>
    public static class WebApiBootstrapper
    {
        #region Methods

        /// <summary>
        /// Starts bootstrapping.
        /// </summary>
        /// <param name="httpConfiguration">The HTTP configuration.</param>
        /// <param name="container">The container.</param>
        public static void Bootstrap(HttpConfiguration httpConfiguration, IUnityContainer container)
        {
            RegisterRoutes(httpConfiguration);
            RegisterFilters(httpConfiguration);
            ConfigureJsonSerializer(httpConfiguration);

            httpConfiguration.DependencyResolver = new WebApiUnityDependencyResolver(container);
        }

        private static void RegisterRoutes(HttpConfiguration httpConfiguration)
        {
            httpConfiguration.Routes.MapHttpRoute(
                name: "PublicApi", 
                routeTemplate: "api/{controller}/{id}", 
                defaults: new { id = RouteParameter.Optional });
        }

        private static void RegisterFilters(HttpConfiguration httpConfiguration)
        {
            httpConfiguration.Filters.Add(new AuthorizeAttribute());
        }

        private static void ConfigureJsonSerializer(HttpConfiguration httpConfiguration)
        {
            // Remove the default XML formatter. Default formatter is now JSON.
            httpConfiguration.Formatters.Remove(httpConfiguration.Formatters.XmlFormatter);

            // Ensures that the JSON output is in camelCase and handles empty string well.
            httpConfiguration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new EmptyStringContractResolver();
        }

        #endregion
    }
}