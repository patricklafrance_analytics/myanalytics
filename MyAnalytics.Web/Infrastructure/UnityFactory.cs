﻿using System;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Infrastructure
{
    /// <summary>
    /// The factory for creating Unity containers.
    /// </summary>
    public static class UnityFactory
    {
        #region Methods

        /// <summary>
        /// Creates a new Unity container.
        /// </summary>
        /// <returns>The new Unity container.</returns>
        public static IUnityContainer Create()
        {
            var container = new UnityContainer();

            return container;
        }

        #endregion
    }
}