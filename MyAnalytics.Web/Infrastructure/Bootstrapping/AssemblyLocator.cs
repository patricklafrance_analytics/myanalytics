﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Web.Compilation;

namespace MyAnalytics.Web.Infrastructure.Bootstrapping
{
    /// <summary>
    /// Assembly locator.
    /// http://stackoverflow.com/questions/3552223/asp-net-appdomain-currentdomain-getassemblies-assemblies-missing-after-app
    /// </summary>
    public static class AssemblyLocator
    {
        #region Fields

        private static readonly ReadOnlyCollection<Assembly> AllAssemblies = new ReadOnlyCollection<Assembly>(BuildManager.GetReferencedAssemblies().Cast<Assembly>().ToList());

        #endregion

        #region Methods

        /// <summary>
        /// Gets the assemblies.
        /// </summary>
        /// <returns>All the assemblies</returns>
        public static IEnumerable<Assembly> GetAssemblies()
        {
            return AllAssemblies;
        }

        #endregion
    }
}