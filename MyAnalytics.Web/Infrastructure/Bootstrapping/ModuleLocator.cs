﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyAnalytics.Web.Infrastructure.Bootstrapping
{
    /// <summary>
    /// Locates the application modules.
    /// </summary>
    public static class ModuleLocator
    {
        #region Methods

        /// <summary>
        /// Locates the application modules.
        /// </summary>
        /// <typeparam name="TModule">The type of the module.</typeparam>
        /// <returns>
        /// A list of module.
        /// </returns>
        public static IEnumerable<TModule> Locate<TModule>()
        {
            var assemblies = AssemblyLocator.GetAssemblies();

            var moduleTypes = assemblies
                .Where(x => !x.IsDynamic)
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => x != typeof(TModule) && typeof(TModule).IsAssignableFrom(x));

            return moduleTypes.Select(x => x.GetConstructor(new Type[0]).Invoke(new object[0])).Cast<TModule>();
        }

        #endregion
    }
}