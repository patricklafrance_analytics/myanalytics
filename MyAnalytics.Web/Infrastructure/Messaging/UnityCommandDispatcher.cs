﻿using System;

using GSoft.G.Extensions;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Messaging;

namespace MyAnalytics.Web.Infrastructure.Messaging
{
    /// <summary>
    /// Dispatcher that looks into Unity for command handlers.
    /// </summary>
    public class UnityCommandDispatcher : ICommandDispatcher
    {
        #region Fields

        private readonly IUnityContainer _container;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityCommandDispatcher"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public UnityCommandDispatcher(IUnityContainer container)
        {
            this._container = container;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sends the command to the appropriate command handler.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <typeparam name="TCommand">
        /// The type of the command.
        /// </typeparam>
        public void Send<TCommand>(TCommand command)
            where TCommand : class, ICommand
        {
            var commandHandler = this._container.Resolve<IHandleCommand<TCommand>>();

            if (commandHandler == null)
            {
                throw new InvalidOperationException("The command of type {0} was sent but no handlers were registered for this type of command. Please check your command registrations.".FormatInvariant(typeof(TCommand)));
            }

            commandHandler.Handle(command);
        }

        #endregion
    }
}