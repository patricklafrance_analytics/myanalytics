﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;
using MyAnalytics.Web.Common.Messaging;

namespace MyAnalytics.Web.Infrastructure.Messaging
{
    /// <summary>
    /// Bootstraps the messaging infrastructure.
    /// </summary>
    public class MessagingModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterType<ICommandDispatcher, UnityCommandDispatcher>(new ContainerControlledLifetimeManager());

            container.RegisterType<IDocumentSessionProvider, RavenDbDocumentSessionManager>();
            container.RegisterType<IDocumentSessionManager, RavenDbDocumentSessionManager>();
        }

        #endregion
    }
}