﻿using System;
using System.Collections.Concurrent;

using MyAnalytics.Web.Common.Messaging;

using Raven.Client;

namespace MyAnalytics.Web.Infrastructure.Messaging
{
    /// <summary>
    /// Manages the lifetime of Raven DB documents sessions during the processing of commands and events based on session ids.
    /// </summary>
    public class RavenDbDocumentSessionManager : IDocumentSessionProvider, IDocumentSessionManager
    {
        #region Fields

        private readonly IDocumentStore _documentStore;
        private readonly ConcurrentDictionary<Guid, IDocumentSession> _sessionDatabase = new ConcurrentDictionary<Guid, IDocumentSession>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RavenDbDocumentSessionManager"/> class.
        /// </summary>
        /// <param name="documentStore">The document store.</param>
        public RavenDbDocumentSessionManager(IDocumentStore documentStore)
        {
            this._documentStore = documentStore;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        public void StartSession(Guid sessionId)
        {
            var documentSession = this._documentStore.OpenSession();

            if (!this._sessionDatabase.TryAdd(sessionId, documentSession))
            {
                throw new ArgumentException("Could not start a session with the id {0}. A session with the same id already exists.");
            }
        }

        /// <summary>
        /// Completes the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        public void CompleteSession(Guid sessionId)
        {
            IDocumentSession documentSession;

            if (this._sessionDatabase.TryRemove(sessionId, out documentSession))
            {
                if (documentSession != null)
                {
                    documentSession.SaveChanges();
                    documentSession.Dispose();
                }
            }
        }

        /// <summary>
        /// Disposes the session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        public void DisposeSession(Guid sessionId)
        {
            IDocumentSession documentSession;

            if (this._sessionDatabase.TryRemove(sessionId, out documentSession))
            {
                if (documentSession != null)
                {
                    documentSession.Dispose();
                }
            }
        }

        /// <summary>
        /// Gets the current session.
        /// </summary>
        /// <param name="sessionId">The session id.</param>
        /// <returns>A document session.</returns>
        public IDocumentSession CurrentSession(Guid sessionId)
        {
            IDocumentSession documentSession;

            if (!this._sessionDatabase.TryGetValue(sessionId, out documentSession))
            {
                throw new ArgumentException("Could not get a session with the id {0}. No session were registered with this id. Check that you used the correlation id of your event to get the session.");
            }

            return documentSession;
        }

        #endregion
    }
}