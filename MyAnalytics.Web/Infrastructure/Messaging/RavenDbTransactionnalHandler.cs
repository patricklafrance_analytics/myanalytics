﻿using System;

using MyAnalytics.Web.Common.Messaging;

namespace MyAnalytics.Web.Infrastructure.Messaging
{
    /// <summary>
    /// Transaction handler that manages the creation of a Raven DB session.
    /// </summary>
    /// <typeparam name="TCommand">The type of the command.</typeparam>
    public class RavenDbTransactionnalHandler<TCommand> : IHandleCommand<TCommand>
        where TCommand : ICommand
    {
        #region Fields

        private readonly IDocumentSessionManager _documentSessionManager;
        private readonly IHandleCommand<TCommand> _next;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RavenDbTransactionnalHandler{TCommand}" /> class.
        /// </summary>
        /// <param name="documentSessionManager">The document session manager.</param>
        /// <param name="next">The next handler.</param>
        public RavenDbTransactionnalHandler(IDocumentSessionManager documentSessionManager, IHandleCommand<TCommand> next)
        {
            this._documentSessionManager = documentSessionManager;
            this._next = next;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the specified command.
        /// </summary>
        /// <param name="command">The command.</param>
        public void Handle(TCommand command)
        {
            this._documentSessionManager.StartSession(command.CommandId);

            try
            {
                this._next.Handle(command);
                this._documentSessionManager.CompleteSession(command.CommandId);
            }
            finally
            {
                this._documentSessionManager.DisposeSession(command.CommandId);
            }
        }

        #endregion
    }
}