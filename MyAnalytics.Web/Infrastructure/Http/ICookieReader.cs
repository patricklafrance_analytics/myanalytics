﻿using System;

namespace MyAnalytics.Web.Infrastructure.Http
{
    /// <summary>
    /// Represents a cookie reader.
    /// </summary>
    public interface ICookieReader
    {
        #region Methods

        /// <summary>
        /// Reads a cookie.
        /// </summary>
        /// <param name="cookieName">The name of the cookie.</param>
        /// <returns>The cookie value.</returns>
        string Read(string cookieName);

        #endregion
    }
}