﻿using System;

namespace MyAnalytics.Web.Infrastructure.Http
{
    /// <summary>
    /// Represents a cookie writer.
    /// </summary>
    public interface ICookieWriter
    {
        #region Methods

        /// <summary>
        /// Writes a cookie.
        /// </summary>
        /// <param name="cookieName">The name of the cookie.</param>
        /// <param name="value">The cookie value.</param>
        void Write(string cookieName, string value);

        #endregion
    }
}