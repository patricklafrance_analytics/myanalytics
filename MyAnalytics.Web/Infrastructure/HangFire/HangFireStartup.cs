﻿using System;
using System.Configuration;

using Hangfire;
using Hangfire.SqlServer;

using Owin;

namespace MyAnalytics.Web.Infrastructure.HangFire
{
    public class HangFireStartup
    {
        #region Methods

        /// <summary>
        /// Configurations the specified app builder.
        /// </summary>
        /// <param name="app">The app builder.</param>
        public void Configuration(IAppBuilder app)
        {
            app.UseHangfire(config =>
            {
                config.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["AzureSqlStorage"].ConnectionString);
                config.UseServer();
            });
        }

        #endregion
    }
}