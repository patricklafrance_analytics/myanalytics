﻿using System;
using System.Web.Http;

using Microsoft.Owin;
using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;
using MyAnalytics.Web.Core.Authentication;
using MyAnalytics.Web.Infrastructure;
using MyAnalytics.Web.Infrastructure.Bootstrapping;
using MyAnalytics.Web.Infrastructure.HangFire;
using MyAnalytics.Web.Infrastructure.Mvc;
using MyAnalytics.Web.Infrastructure.WebApi;

[assembly: OwinStartup(typeof(HangFireStartup))]

namespace MyAnalytics.Web
{
    /// <summary>
    /// Creates the unity container and bootstrap MVC and Web API
    /// </summary>
    public static class ApplicationBootstrapper
    {
        #region Methods

        /// <summary>
        /// Bootstraps the application.
        /// </summary>
        public static void Bootstrap()
        {
            var container = UnityFactory.Create();

            RegisterWebComponents(container);

            JsonSerializerFactory.SetJsonSettingsProvider(() => GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings);

            container.RegisterType<AuthenticationService>(new ContainerControlledLifetimeManager());

            LoadModules(container);
        }

        private static void RegisterWebComponents(IUnityContainer container)
        {
            GlobalConfiguration.Configure(x => WebApiBootstrapper.Bootstrap(x, container));
            MvcBootstrapper.Bootstrap(container);
        }

        private static void LoadModules(IUnityContainer container)
        {
            var modules = ModuleLocator.Locate<IModule>();

            foreach (var module in modules)
            {
                module.Load(container);
            }
        }

        #endregion
    }
}