﻿using System;
using System.Web;

namespace MyAnalytics.Web
{
    /// <summary>
    /// Application bootstrapper.
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        #region Methods

        /// <summary>
        /// Bootstrap everything when the Web application start.
        /// </summary>
        protected void Application_Start()
        {
            ApplicationBootstrapper.Bootstrap();

            // MessagingBootstrapper.Bootstrap();

            // DashboardBootstrapper.Bootstrap();
        }

        #endregion
    }
}