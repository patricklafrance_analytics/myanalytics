﻿using System;

using MyAnalytics.Web.Common.Messaging;

namespace MyAnalytics.Web.Core.DataSynchronization
{
    /// <summary>
    /// Command to synchronize the analytics data with the remote data hosted at Google analytics for the specified profile.
    /// </summary>
    public class SynchronizeAnalyticsDataCommand : Command
    {
        #region Properties

        /// <summary>
        /// Gets or sets the profile id.
        /// </summary>
        public string ProfileId { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        #endregion
    }
}