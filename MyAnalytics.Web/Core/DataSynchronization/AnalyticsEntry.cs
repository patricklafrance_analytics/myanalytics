﻿using System;

namespace MyAnalytics.Web.Core.DataSynchronization
{
    /// <summary>
    /// Document that store the analytics data for a day.
    /// </summary>
    public class AnalyticsEntry
    {
        #region Properties

        /// <summary>
        /// Gets or sets the profile id.
        /// </summary>
        public string ProfileId { get; set; }

        /// <summary>
        /// Gets or sets the year.
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Gets or sets the month.
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// Gets or sets the day.
        /// </summary>
        public string Day { get; set; }

        /// <summary>
        /// Gets or sets the sessions.
        /// </summary>
        public string Sessions { get; set; }

        #endregion
    }
}