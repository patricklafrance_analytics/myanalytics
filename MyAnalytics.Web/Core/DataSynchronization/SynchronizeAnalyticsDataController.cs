﻿using System;
using System.Web.Http;

using MyAnalytics.Web.Common.Messaging;

namespace MyAnalytics.Web.Core.DataSynchronization
{
    /// <summary>
    /// Controller responsible of synchronizing the analytics data with the remote data hosted at Google analytics for the specified profile.
    /// </summary>
    public class SynchronizeAnalyticsDataController : ApiController
    {
        #region Fields

        private readonly ICommandDispatcher _commandDispatcher;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizeAnalyticsDataController" /> class.
        /// </summary>
        /// <param name="commandDispatcher">The command dispatcher.</param>
        public SynchronizeAnalyticsDataController(ICommandDispatcher commandDispatcher)
        {
            this._commandDispatcher = commandDispatcher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets an instance of SynchronizeAnalyticsDataCommand.
        /// </summary>
        /// <param name="profileId">The profile id.</param>
        /// <returns>A command.</returns>
        public IHttpActionResult Get(string profileId)
        {
            return this.Ok(new SynchronizeAnalyticsDataCommand
            {
                ProfileId = profileId,
                Username = this.User.Identity.Name
            });
        }

        /// <summary>
        /// Synchrnize the analytics data.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>A command result.</returns>
        public IHttpActionResult Post(SynchronizeAnalyticsDataCommand command)
        {
            this._commandDispatcher.Send(command);

            return this.Ok();
        }

        #endregion
    }
}