﻿using System;
using System.Configuration;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;
using MyAnalytics.Web.Common.Messaging;

namespace MyAnalytics.Web.Core.DataSynchronization
{
    /// <summary>
    /// Bootstraps synchronization context.
    /// </summary>
    public class SynchronizationModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterCommandType<SynchronizeAnalyticsDataCommand, SynchronizeAnalyticsDataCommandHandler>();
            container.RegisterType<AnalyticsDataSynchronizer>(new ContainerControlledLifetimeManager());

            container.RegisterType<AnalyticsDataRepository>(new ContainerControlledLifetimeManager());
            container.RegisterType<AnalyticsDataCheckpointRepository>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}