﻿using System;

namespace MyAnalytics.Web.Core.DataSynchronization
{
    /// <summary>
    /// Document that store informations about an analytics profile.
    /// </summary>
    public class AnalyticsProfile
    {
        #region Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the synchronization checkpoint.
        /// </summary>
        public DateTime? SynchronizationCheckpoint { get; set; }

        #endregion
    }
}