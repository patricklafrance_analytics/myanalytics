﻿using System;
using System.Collections.Generic;
using System.Linq;

using Google.Apis.Analytics.v3;

using GSoft.G.Extensions;

using MyAnalytics.Web.Common.Messaging;
using MyAnalytics.Web.Common.RavenDb;
using MyAnalytics.Web.Google.Analytics;

using Raven.Abstractions.Data;
using Raven.Client;

namespace MyAnalytics.Web.Core.DataSynchronization
{
    /// <summary>
    /// Command handler responsible of synchronizing the analytics data with the remote data hosted at Google analytics for the specified profile.
    /// </summary>
    public class SynchronizeAnalyticsDataCommandHandler : IHandleCommand<SynchronizeAnalyticsDataCommand>
    {
        #region Fields

        /// <summary>
        /// Google analytics API will return an error if the start date is lower.
        /// </summary>
        private static readonly string LowestAllowedStartDate = "2005-01-01";

        private readonly IDocumentStore _documentStore;
        private readonly GoogleAnalyticsServiceProvider _googleAnalyticsServiceProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SynchronizeAnalyticsDataCommandHandler" /> class.
        /// </summary>
        /// <param name="documentStore">The document store.</param>
        /// <param name="googleAnalyticsServiceProvider">The google analytics service provider.</param>
        public SynchronizeAnalyticsDataCommandHandler(IDocumentStore documentStore, GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider)
        {
            this._documentStore = documentStore;
            this._googleAnalyticsServiceProvider = googleAnalyticsServiceProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the command.
        /// </summary>
        /// <param name="command">The command.</param>
        public void Handle(SynchronizeAnalyticsDataCommand command)
        {
            using (var session = this._documentStore.OpenSession())
            {
                using (var analyticsService = this._googleAnalyticsServiceProvider.Get(command.Username))
                {
                    var startDate = LowestAllowedStartDate;

                    var profileDocumentId = session.BuildDocumentKey<AnalyticsEntry>(command.ProfileId);
                    var profile = session.Load<AnalyticsProfile>(profileDocumentId);

                    if (profile == null)
                    {
                        profile = new AnalyticsProfile { Id = profileDocumentId };

                        session.Store(profile);
                    }
                    else
                    {
                        if (profile.SynchronizationCheckpoint.HasValue)
                        {
                            startDate = profile.SynchronizationCheckpoint.Value.ToString("yyyy-MM-dd");
                        }
                        else
                        {
                            if (session.Query<AnalyticsEntry>().Any(x => x.ProfileId == command.ProfileId))
                            {
                                throw new InvalidOperationException("The synchronization for profile {0} is broken. The synchronization checkpoint is null but there is analytics entries associated to the profile.");
                            }
                        }
                    }

                    // Make sure the synchronization checkpoint can be saved before synchronizing the analytics entries because
                    // Raven DB bulk insert operations do not support DTC transactions and will not be reverted if there is an
                    // error while saving the synchronization checkpoint.
                    profile.SynchronizationCheckpoint = DateTime.Now;
                    session.SaveChanges();

                    var entries = this.FetchAnalyticsData(analyticsService, command.ProfileId, startDate);
                    if (entries.Any())
                    {
                        this.StoreEntries(entries);
                    }
                }
            }
        }

        private static AnalyticsEntry CreateAnalyticsEntry(string profileId, IList<string> feedRow)
        {
            return new AnalyticsEntry
            {
                ProfileId = profileId, 
                Year = feedRow[0], 
                Month = feedRow[1], 
                Day = feedRow[2], 
                Sessions = feedRow[3]
            };
        }

        private IList<AnalyticsEntry> FetchAnalyticsData(AnalyticsService analyticsService, string profileId, string startDate)
        {
            var analyticsEntries = new List<AnalyticsEntry>();
            var request = analyticsService.Data.Ga.Get("ga:{0}".FormatInvariant(profileId), startDate, "today", "ga:sessions");

            request.Dimensions = "ga:year,ga:month,ga:day";
            request.MaxResults = 5000;

            var feed = request.Execute();

            while (feed.Rows != null)
            {
                // Omit the entries with 0 sessions.
                analyticsEntries.AddRange(feed.Rows.Where(x => x[3] != "0").Select(x => CreateAnalyticsEntry(profileId, x)));

                // If there is more entries that the specified max results, the data will be available on multiples pages. 
                // To retrieve all the data, we must iterate on every pages.
                if (!string.IsNullOrEmpty(feed.NextLink))
                {
                    request.StartIndex = request.StartIndex + request.MaxResults;
                    feed = request.Execute();
                }
                else
                {
                    break;
                }
            }

            return analyticsEntries;
        }

        private void StoreEntries(IList<AnalyticsEntry> entries)
        {
            var options = new BulkInsertOptions
            {
                BatchSize = entries.Count()
            };

            using (var operation = this._documentStore.BulkInsert(options: options))
            {
                foreach (var entry in entries)
                {
                    operation.Store(entry);
                }
            }
        }

        #endregion
    }
}