﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3.Data;

using GSoft.G;
using GSoft.G.Extensions;

using Microsoft.WindowsAzure.Storage.Table;

using MyAnalytics.Web.Common.Extensions;
using MyAnalytics.Web.Google.Analytics;

namespace MyAnalytics.Web.Core
{
    public class AnalyticsDataSynchronizer
    {
        #region Fields

        private readonly GoogleAnalyticsServiceProvider _googleAnalyticsServiceProvider;
        private readonly AnalyticsDataRepository _analyticsDataRepository;
        private readonly AnalyticsDataCheckpointRepository _analyticsDataCheckpointRepository;

        #endregion

        #region Constructors

        public AnalyticsDataSynchronizer(GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider, AnalyticsDataRepository analyticsDataRepository, AnalyticsDataCheckpointRepository analyticsDataCheckpointRepository)
        {
            this._googleAnalyticsServiceProvider = googleAnalyticsServiceProvider;
            this._analyticsDataRepository = analyticsDataRepository;
            this._analyticsDataCheckpointRepository = analyticsDataCheckpointRepository;
        }

        #endregion

        #region Methods

        public async Task Synchronize(string profileId, string userIdentifier, string userEmail)
        {
            profileId.Ensure("profileId").IsNotNullOrEmpty();
            userIdentifier.Ensure("userIdentifier").IsNotNullOrEmpty();
            userEmail.Ensure("userEmail").IsNotNullOrEmpty();

            var startDate = DateTime.Today.AddMonths(-3);

            var checkpoint = await this.GetCurrentCheckpoint(profileId);
            if (checkpoint != null)
            {
                startDate = checkpoint.Value;
            }

            if (startDate != DateTime.Today)
            {
                var analyticsData = await this.FetchAnalyticsData(profileId, userIdentifier, userEmail, startDate);

                await this.StoreAnalyticsData(profileId, analyticsData);
                await this.SaveNewCheckpoint(profileId);
            }
        }

        private async Task<GaData> FetchAnalyticsData(string profileId, string userIdentifier, string userEmail, DateTime startDate)
        {
            using (var analyticsService = await this._googleAnalyticsServiceProvider.GetAsync(userIdentifier))
            {
                var request = analyticsService.Data.Ga.Get("ga:{0}".FormatInvariant(profileId), startDate.ToString("yyyy-MM-dd"), DateTime.Today.ToString("yyyy-MM-dd"), "ga:pageViewsPerSession");
                request.Dimensions = "ga:year,ga:month,ga:day";

                var batchRequest = new RandomIntervalRetryPolicy(new GoogleAnalyticsBatchRequest(analyticsService, new[] { request }, userEmail));
                var batchResult = await batchRequest.ExecuteAsync();

                return batchResult.ElementAt(0);
            }
        }

        private async Task StoreAnalyticsData(string profileId, GaData data)
        {
            if (data.Rows != null && data.Rows.Count > 0)
            {
                var table = this._analyticsDataRepository.GetTable();
                var batchOperation = new TableBatchOperation();

                foreach (var row in data.Rows)
                {
                    var entryDate = new DateTime(row[0].ToInt32(), row[1].ToInt32(), row[2].ToInt32());

                    var entry = new AnalyticsData
                    {
                        PartitionKey = profileId, 
                        RowKey = entryDate.Ticks.ToInvariantString(), 
                        Date = entryDate, 
                        PageViewsPerSession = row[3].ToDouble()
                    };

                    batchOperation.InsertOrReplace(entry);
                }

                await table.ExecuteBatchAsync(batchOperation);
            }
        }

        private async Task<AnalyticsDataCheckpoint> GetCurrentCheckpoint(string profileId)
        {
            return await this._analyticsDataCheckpointRepository.FetchAsync(profileId, profileId);
        }

        private async Task SaveNewCheckpoint(string profileId)
        {
            await this._analyticsDataCheckpointRepository.InsertAsync(new AnalyticsDataCheckpoint
            {
                PartitionKey = profileId, 
                RowKey = profileId, 
                Value = DateTime.Today
            });
        }

        #endregion
    }
}