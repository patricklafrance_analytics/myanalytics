﻿using System;

using Microsoft.WindowsAzure.Storage.Table;

namespace MyAnalytics.Web.Core
{
    public class AnalyticsDataCheckpoint : TableEntity
    {
        #region Properties

        public DateTime Value { get; set; }

        #endregion
    }
}