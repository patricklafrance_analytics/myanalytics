﻿using System;

using MyAnalytics.Web.Common.Azure;

namespace MyAnalytics.Web.Core
{
    public class AnalyticsDataCheckpointRepository : AzureTableRepository<AnalyticsDataCheckpoint>
    {
    }
}