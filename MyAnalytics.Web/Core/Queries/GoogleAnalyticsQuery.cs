﻿using System;

using GSoft.G.Extensions;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Base Google analytics query.
    /// </summary>
    public abstract class GoogleAnalyticsQuery : Query, IGoogleAnalyticsQuery
    {
        #region Properties

        /// <summary>
        /// Gets the user identifier.
        /// </summary>
        public string UserIdentifier { get; set; }

        /// <summary>
        /// Gets the profile id.
        /// </summary>
        public string ProfileId { get; private set; }

        /// <summary>
        /// Gets or sets the actual period.
        /// </summary>
        public Period SelectedPeriod { get; set; }

        /// <summary>
        /// Gets or sets the previous period.
        /// </summary>
        public Period PreviousPeriod { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the user profile id.
        /// </summary>
        /// <param name="profileId">The profile id.</param>
        public void SetProfileId(string profileId)
        {
            this.ProfileId = "ga:{0}".FormatInvariant(profileId);
        }

        #endregion
    }
}