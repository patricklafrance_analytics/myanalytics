﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Requests;

using MyAnalytics.Web.Google.Analytics;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Base query handler for handlers that for a Google analytics query.
    /// </summary>
    /// <typeparam name="TQuery">The type of the query.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public abstract class GoogleAnalyticsQueryHandler<TQuery, TResult> : IHandleQuery<TQuery, TResult>
        where TQuery : IGoogleAnalyticsQuery
    {
        private static Random RandomIntervalGenerator = new Random();

        #region Fields

        private readonly GoogleAnalyticsServiceProvider _googleAnalyticsServiceProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleAnalyticsQueryHandler{TQuery, TResult}"/> class.
        /// </summary>
        /// <param name="googleAnalyticsServiceProvider">The google analytics service provider.</param>
        protected GoogleAnalyticsQueryHandler(GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider)
        {
            this._googleAnalyticsServiceProvider = googleAnalyticsServiceProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        public async Task<TResult> Handle(TQuery query)
        {
            using (var analyticsService = await this._googleAnalyticsServiceProvider.GetAsync(query.UserIdentifier))
            {
                return await this.HandleQuery(query, analyticsService);
            }
        }

        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        /// <param name="analyticsService">The analytics service.</param>
        /// <returns>A query result.</returns>
        protected abstract Task<TResult> HandleQuery(TQuery query, AnalyticsService analyticsService);

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A request result.</returns>
        protected async Task<GaData> ExecuteRequest(DataResource.GaResource.GetRequest request)
        {
            try
            {
                request.QuotaUser = "John Doe";

                return await request.ExecuteAsync(new CancellationToken());
            }
            catch (TaskCanceledException)
            {
            }

            return null;
        }

        /// <summary>
        /// Executes a batch request.
        /// </summary>
        /// <param name="requests">The requests.</param>
        /// <param name="userIdentifier">The user identifier.</param>
        /// <param name="analyticsService">The analytics service.</param>
        /// <returns>A list of request results.</returns>
        protected async Task<IEnumerable<GaData>> ExecuteBatchRequest(IEnumerable<DataResource.GaResource.GetRequest> requests, string userIdentifier, AnalyticsService analyticsService)
        {
            // TODO: Faire un objet GoogleAnalyticsBatchRequestResults contenant le Add pour une BatchRequest ainsi qu'une méthode pour vérifier s'il y a des erreurs.
            // TODO: Faire un objet GoogleAnalyticsBatchRequest qui fait tout ça avec le exponential backoff.
            var batchResults = new List<GoogleAnalyticsBatchRequestResult>();

            for (var i = 0; i < 30; i++)
            {
                var batch = new BatchRequest(analyticsService);

                foreach (var request in requests)
                {
                    request.QuotaUser = userIdentifier;

                    batch.Queue<GaData>(request, (data, requestError, index, message) => AppendBatchResult(data, requestError, index, batchResults));
                }

                await batch.ExecuteAsync(new CancellationToken());

                if (HasBatchError(batchResults))
                {
                    batchResults.Clear();

                    // Kind of exponential backoff implementation.
                    await Task.Delay(RandomIntervalGenerator.Next(100, 500));
                }
                else
                {
                    return batchResults.Select(x => x.Data);
                }
            }

            return null;
        }

        private static void AppendBatchResult(GaData data, RequestError requestError, int index, List<GoogleAnalyticsBatchRequestResult> batchResults)
        {
            batchResults.Add(new GoogleAnalyticsBatchRequestResult
            {
                Data = data, 
                RequestError = requestError, 
                Index = index
            });
        }

        private static bool HasBatchError(IEnumerable<GoogleAnalyticsBatchRequestResult> batchResults)
        {
            return batchResults.Any(x =>
                x.RequestError != null &&
                x.RequestError.Errors.Any(y => y.Reason == "rateLimitExceeded" || y.Reason == "userRateLimitExceeded"));
        }

        #endregion
    }
}