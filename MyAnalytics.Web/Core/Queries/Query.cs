﻿using System;

using MyAnalytics.Web.Common.Extensions;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Base query.
    /// </summary>
    public abstract class Query : IQuery
    {
        #region Properties

        /// <summary>
        /// Gets the id.
        /// </summary>
        public virtual string Id
        {
            get
            {
                return Guid.NewGuid().ToInvariantString();
            }
        }

        #endregion
    }
}