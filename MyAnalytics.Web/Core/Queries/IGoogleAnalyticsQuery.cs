﻿using System;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Base Google analytics query interface.
    /// </summary>
    public interface IGoogleAnalyticsQuery
    {
        /// <summary>
        /// Gets the username.
        /// </summary>
        string UserIdentifier { get; }
    }
}
