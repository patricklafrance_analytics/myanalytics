﻿using System;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Represents a date range.
    /// </summary>
    public class Period
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Period"/> struct.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        public Period(DateTime startDate, DateTime endDate)
        {
            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the starting date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the ending date.
        /// </summary>
        public DateTime EndDate { get; set; }

        #endregion
    }
}