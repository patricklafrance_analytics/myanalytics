﻿using System;
using System.Threading.Tasks;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Base query handler interface.
    /// </summary>
    /// <typeparam name="TQuery">The type of the query.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public interface IHandleQuery<in TQuery, TResult>
    {
        #region Methods

        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        Task<TResult> Handle(TQuery query);

        #endregion
    }
}