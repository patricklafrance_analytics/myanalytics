﻿using System;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Base query interface.
    /// </summary>
    public interface IQuery
    {
        #region Properties

        /// <summary>
        /// Gets the id.
        /// </summary>
        string Id { get; }

        #endregion
    }
}