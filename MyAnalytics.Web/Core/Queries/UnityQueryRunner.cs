﻿using System;
using System.Threading.Tasks;

using GSoft.G.Extensions;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Ruunner that looks into Unity for query handlers.
    /// </summary>
    public class UnityQueryRunner : IQueryRunner
    {
        #region Fields

        private readonly IUnityContainer _container;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityQueryRunner"/> class.
        /// </summary>
        /// <param name="container">The container.</param>
        public UnityQueryRunner(IUnityContainer container)
        {
            this._container = container;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Runs the specified query with the appropriate handler.
        /// </summary>
        /// <typeparam name="TQuery">The type of the query.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        public Task<TResult> Run<TQuery, TResult>(TQuery query)
            where TQuery : IQuery
        {
            var queryHandler = this._container.Resolve<IHandleQuery<TQuery, TResult>>();
            if (queryHandler == null)
            {
                throw new InvalidOperationException("The query of type {0} was sent but no handlers were registered for this type of query. Please check your query registrations.".FormatInvariant(typeof(TQuery)));
            }

            return queryHandler.Handle(query);
        }

        #endregion
    }
}