﻿using System;
using System.Threading.Tasks;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Interface to run a query with the appropriate handler.
    /// </summary>
    public interface IQueryRunner
    {
        #region Methods

        /// <summary>
        /// Runs the specified query with the appropriate handler.
        /// </summary>
        /// <typeparam name="TQuery">The type of the query.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        Task<TResult> Run<TQuery, TResult>(TQuery query) where TQuery : IQuery;

        #endregion
    }
}