﻿using System;

using Microsoft.Practices.Unity;

namespace MyAnalytics.Web.Core.Queries
{
    /// <summary>
    /// Query registration facilities.
    /// </summary>
    public static class QueryHandlerRegistration
    {
        #region Methods

        /// <summary>
        /// Registers a query to Unity.
        /// </summary>
        /// <typeparam name="TQuery">The type of the query.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <typeparam name="THandler">The type of the handler.</typeparam>
        /// <param name="container">The container.</param>
        public static void RegisterQueryType<TQuery, TResult, THandler>(this IUnityContainer container)
            where TQuery : IQuery
            where THandler : IHandleQuery<TQuery, TResult>
        {
            container.RegisterType<IHandleQuery<TQuery, TResult>, THandler>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}