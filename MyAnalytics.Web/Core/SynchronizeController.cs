﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using MyAnalytics.Web.Common.Security;
using MyAnalytics.Web.Http;
using MyAnalytics.Web.Infrastructure.Security;

namespace MyAnalytics.Web.Core
{
    public class SynchronizeController : ApiController
    {
        private readonly AnalyticsDataSynchronizer _analyticsDataSynchronizer;

        public SynchronizeController(AnalyticsDataSynchronizer analyticsDataSynchronizer)
        {
            this._analyticsDataSynchronizer = analyticsDataSynchronizer;
        }

        public async Task<IHttpActionResult> Post(SynchronizeCommand command)
        {
            var session = this.GetSession();

            await this._analyticsDataSynchronizer.Synchronize(command.ProfileId, session.SessionId, session.UserEmail);

            return this.Ok();
        }

        private UserSession GetSession()
        {
            var authenticationCookie = new FormsAuthenticationCookie(new HttpRequestMessageCookieReader(this.Request));
            var session = authenticationCookie.Session;

            return session;
        }
    }
}