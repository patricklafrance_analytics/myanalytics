﻿using System;
using System.Collections.Generic;

using GSoft.G;

using Microsoft.WindowsAzure.Storage.Table;

using MyAnalytics.Web.Common.Azure;
using MyAnalytics.Web.Common.Extensions;

namespace MyAnalytics.Web.Core
{
    public class AnalyticsDataRepository : AzureTableRepository<AnalyticsData>
    {
        #region Methods

        public IEnumerable<AnalyticsData> FetchForDateRange(string profileId, DateTime start, DateTime end)
        {
            profileId.Ensure("profileId").IsNotNullOrEmpty();

            var query = new TableQuery<AnalyticsData>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, profileId), 
                    TableOperators.And, 
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, start.Ticks.ToInvariantString()), 
                        TableOperators.And, 
                        TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, end.Ticks.ToInvariantString()))));

            return this.GetTable().ExecuteQuery(query);
        }

        #endregion
    }
}