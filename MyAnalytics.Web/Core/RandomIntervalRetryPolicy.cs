﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3.Data;

namespace MyAnalytics.Web.Core
{
    public class RandomIntervalRetryPolicy
    {
        #region Fields

        private static readonly Random IntervalGenerator = new Random();

        private readonly GoogleAnalyticsBatchRequest _request;

        private readonly int _numberOfRetries;
        private readonly int _minimumInterval;
        private readonly int _maximumInterval;

        #endregion

        #region Constructors

        public RandomIntervalRetryPolicy(GoogleAnalyticsBatchRequest request)
            : this(request, 30, 100, 500)
        {
        }

        public RandomIntervalRetryPolicy(GoogleAnalyticsBatchRequest request, int numberOfRetries, int minimumInterval, int maximumInterval)
        {
            this._request = request;
            this._numberOfRetries = numberOfRetries;
            this._minimumInterval = minimumInterval;
            this._maximumInterval = maximumInterval;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<GaData>> ExecuteAsync()
        {
            for (var i = 0; i < this._numberOfRetries; i++)
            {
                var result = await this._request.ExecuteAsync();

                if (result.HasRetryableErrors())
                {
                    await Task.Delay(IntervalGenerator.Next(this._minimumInterval, this._maximumInterval));
                }
                else
                {
                    return result.GetData();
                }
            }

            return null;
        }

        #endregion
    }
}