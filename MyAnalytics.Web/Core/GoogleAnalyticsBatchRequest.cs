﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Requests;

namespace MyAnalytics.Web.Core
{
    public class GoogleAnalyticsBatchRequest
    {
        #region Fields

        private readonly AnalyticsService _analyticsService;
        private readonly IEnumerable<DataResource.GaResource.GetRequest> _requests;
        private readonly string _userEmail;

        #endregion

        #region Constructors

        public GoogleAnalyticsBatchRequest(AnalyticsService analyticsService, IEnumerable<DataResource.GaResource.GetRequest> requests, string userEmail)
        {
            this._analyticsService = analyticsService;
            this._requests = requests;
            this._userEmail = userEmail;
        }

        #endregion

        #region Methods

        public async Task<GoogleAnalyticsBatchRequestResultCollection> ExecuteAsync()
        {
            if (!this._requests.Any())
            {
                return GoogleAnalyticsBatchRequestResultCollection.Empty();
            }

            var batch = new BatchRequest(this._analyticsService);
            var batchResults = new GoogleAnalyticsBatchRequestResultCollection();

            foreach (var request in this._requests)
            {
                request.QuotaUser = this._userEmail;
                request.MaxResults = 10000;

                batch.Queue<GaData>(request, (data, requestError, index, message) => batchResults.Add(data, requestError, index));
            }

            await batch.ExecuteAsync();

            return batchResults;
        }

        #endregion
    }
}