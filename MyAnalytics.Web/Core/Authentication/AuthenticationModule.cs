﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;

namespace MyAnalytics.Web.Core.Authentication
{
    /// <summary>
    /// Boostraps the authentication module.
    /// </summary>
    public class AuthenticationModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterType<AuthenticationService>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}