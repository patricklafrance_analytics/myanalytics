﻿using System;

namespace MyAnalytics.Web.Core.Authentication
{
    /// <summary>
    /// Account document projection.
    /// </summary>
    public class AccountInfo
    {
        #region Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        #endregion
    }
}