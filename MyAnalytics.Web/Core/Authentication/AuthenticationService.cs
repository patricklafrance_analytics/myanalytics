﻿using System;
using System.Threading.Tasks;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Plus.v1.Data;

using MyAnalytics.Web.Accounts;
using MyAnalytics.Web.Common.RavenDb;
using MyAnalytics.Web.Common.Security;
using MyAnalytics.Web.Google.Authentication;
using MyAnalytics.Web.Google.Plus;

using Raven.Client;

namespace MyAnalytics.Web.Core.Authentication
{
    /// <summary>
    /// Application authentication service.
    /// </summary>
    public class AuthenticationService
    {
        #region Fields

        private readonly IDocumentStore _documentStore;
        private readonly GoogleAuthenticationService _googleAuthenticationService;
        private readonly GooglePlusServiceProvider _googlePlusServiceProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationService" /> class.
        /// </summary>
        /// <param name="documentStore">The document store.</param>
        /// <param name="googleAuthenticationService">The google authentication service.</param>
        /// <param name="googlePlusServiceProvider">The google plus service provider.</param>
        public AuthenticationService(IDocumentStore documentStore, GoogleAuthenticationService googleAuthenticationService, GooglePlusServiceProvider googlePlusServiceProvider)
        {
            this._documentStore = documentStore;
            this._googleAuthenticationService = googleAuthenticationService;
            this._googlePlusServiceProvider = googlePlusServiceProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Logs the current user in.
        /// </summary>
        /// <returns>A session if the log on succeeds. False if the user could not be authentified.</returns>
        public async Task<UserSession> LogOnAsync()
        {
            var credential = await this.TryAuthenticateToGoogle(UserSession.GenerateSessionId());
            if (credential != null)
            {
                var user = await this.RetrieveUser(credential);

                using (var documentSession = this._documentStore.OpenAsyncSession())
                {
                    var documentKey = DocumentKeyHelpers.GenerateTypedStringKey<Account>(documentSession, user.Id.ToLowerInvariant());
                    var account = await documentSession.LoadProjectionAsync<AccountInfo>(documentKey);

                    if (account != null)
                    {
                        return new UserSession(credential.UderId, account.Email);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Close the specified session.
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns>A task.</returns>
        public async Task LogOff(UserSession session)
        {
            await this._googleAuthenticationService.RevokeAsync(session.SessionId);
        }

        private async Task<Person> RetrieveUser(UserCredential credential)
        {
            var googlePlusService = this._googlePlusServiceProvider.Get(credential);

            return await googlePlusService.GetUserAsync();
        }

        //private async Task<string> RetrieveAccountEmail(UserCredential credential)
        //{
        //    var googlePlusService = this._googlePlusServiceProvider.Get(credential);
            
        //    return await googlePlusService.GetAccountEmailAsync();
        //}

        private async Task<UserCredential> TryAuthenticateToGoogle(string userIdentifier)
        {
            var cancellationDelay = 120 * 1000;

            return await this._googleAuthenticationService.AuthorizeAsync(userIdentifier, cancellationDelay);
        }

        #endregion
    }
}