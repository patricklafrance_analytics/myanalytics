﻿using System;
using System.Collections.Generic;
using System.Linq;

using Google.Apis.Analytics.v3.Data;
using Google.Apis.Requests;

namespace MyAnalytics.Web.Core
{
    public class GoogleAnalyticsBatchRequestResultCollection
    {
        #region Fields

        private readonly IList<GoogleAnalyticsBatchRequestResult> _results = new List<GoogleAnalyticsBatchRequestResult>();

        #endregion

        #region Methods

        public void Add(GaData data, RequestError requestError, int index)
        {
            this._results.Add(new GoogleAnalyticsBatchRequestResult
            {
                Data = data, 
                RequestError = requestError, 
                Index = index
            });
        }

        public bool HasRetryableErrors()
        {
            return this._results.Any(x =>
                x.RequestError != null &&
                x.RequestError.Errors.Any(y => y.Reason == "rateLimitExceeded" || y.Reason == "userRateLimitExceeded"));
        }

        public IEnumerable<GaData> GetData()
        {
            return this._results.Select(x => x.Data);
        }

        public static GoogleAnalyticsBatchRequestResultCollection Empty()
        {
            return new GoogleAnalyticsBatchRequestResultCollection();
        }

        #endregion
    }
}