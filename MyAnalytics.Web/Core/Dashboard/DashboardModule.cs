﻿using System;

using Microsoft.Practices.Unity;

using MyAnalytics.Web.Common.Bootstrapping;
using MyAnalytics.Web.Core.Dashboard.Queries.PagesPerSession;
using MyAnalytics.Web.Core.Dashboard.Queries.Sessions;
using MyAnalytics.Web.Core.Dashboard.Queries.TimeOnSite;
using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard
{
    /// <summary>
    /// Bootstraps dashboard context.
    /// </summary>
    public class DashboardModule : IModule
    {
        #region Methods

        /// <summary>
        /// Loads the module.
        /// </summary>
        /// <param name="container">The IoC container.</param>
        public void Load(IUnityContainer container)
        {
            container.RegisterType<IQueryRunner, UnityQueryRunner>(new ContainerControlledLifetimeManager());
            container.RegisterQueryType<SessionsQuery, SessionsQueryResult, SessionsQueryHandler>();
            container.RegisterQueryType<PagesPerSessionQuery, PagesPerSessionQueryResult, PagesPerSessionQueryHandler>();
            container.RegisterQueryType<TimeOnSiteQuery, TimeOnSiteQueryResult, TimeOnSiteQueryHandler>();
        }

        #endregion
    }
}