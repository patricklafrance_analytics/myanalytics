﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TopSources
{
    /// <summary>
    /// Top sources query.
    /// </summary>
    public class TopSourcesQuery : GoogleAnalyticsQuery
    {
    }
}