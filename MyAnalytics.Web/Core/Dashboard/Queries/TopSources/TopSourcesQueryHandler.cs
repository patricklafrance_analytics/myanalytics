﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

using Google.Apis.Analytics.v3;

using MyAnalytics.Web.Core.Queries;
using MyAnalytics.Web.Google.Analytics;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TopSources
{
    /// <summary>
    /// Handler responsible of running the top sources query against Google analytics.
    /// </summary>
    public class TopSourcesQueryHandler : GoogleAnalyticsQueryHandler<TopSourcesQuery, TopSourceQueryResult>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TopSourcesQueryHandler"/> class.
        /// </summary>
        /// <param name="googleAnalyticsServiceProvider">The google analytics service provider.</param>
        public TopSourcesQueryHandler(GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider)
            : base(googleAnalyticsServiceProvider)
        {
        }

        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        /// <param name="analyticsService">The analytics service.</param>
        /// <returns>A query result.</returns>
        protected override Task<TopSourceQueryResult> HandleQuery(TopSourcesQuery query, AnalyticsService analyticsService)
        {
            throw new NotImplementedException();
        }

        private static double ConvertToDouble(string result)
        {
            return Math.Round(double.Parse(result, CultureInfo.InvariantCulture), 2);
        }
    }
}