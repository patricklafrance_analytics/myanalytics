﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TopSources
{
    /// <summary>
    /// Top sources query controller.
    /// </summary>
    public class TopSourceQueryController : DashboardQueryController<TopSourcesQuery, TopSourceQueryResult>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TopSourceQueryController"/> class.
        /// </summary>
        /// <param name="queryRunner">The query runner.</param>
        public TopSourceQueryController(IQueryRunner queryRunner)
            : base(queryRunner)
        {
        }

        #endregion
    }
}