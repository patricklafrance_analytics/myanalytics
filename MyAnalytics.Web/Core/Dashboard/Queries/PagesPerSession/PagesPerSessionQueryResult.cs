﻿using System;
using System.Collections.Generic;

namespace MyAnalytics.Web.Core.Dashboard.Queries.PagesPerSession
{
    /// <summary>
    /// Pages per session query result.
    /// </summary>
    public class PagesPerSessionQueryResult
    {
        /// <summary>
        /// Gets or sets the pages per session.
        /// </summary>
        public double PagesPerSession { get; set; }

        /// <summary>
        /// Gets or sets the pages per session per days.
        /// </summary>
        public IEnumerable<double> PagesPerSessionPerDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is better.
        /// </summary>
        public bool IsBetter { get; set; }
    }
}