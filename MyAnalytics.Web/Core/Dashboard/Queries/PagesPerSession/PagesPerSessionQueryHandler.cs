﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3;

using MyAnalytics.Web.Core.Queries;
using MyAnalytics.Web.Google.Analytics;

namespace MyAnalytics.Web.Core.Dashboard.Queries.PagesPerSession
{
    /// <summary>
    /// Handler responsible of running the sessions query against Google analytics.
    /// </summary>
    public class PagesPerSessionQueryHandler : GoogleAnalyticsQueryHandler<PagesPerSessionQuery, PagesPerSessionQueryResult>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PagesPerSessionQueryHandler"/> class.
        /// </summary>
        /// <param name="googleAnalyticsServiceProvider">The google analytics service provider.</param>
        public PagesPerSessionQueryHandler(GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider)
            : base(googleAnalyticsServiceProvider)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        /// <param name="analyticsService">The analytics service.</param>
        /// <returns>A query result.</returns>
        protected override async Task<PagesPerSessionQueryResult> HandleQuery(PagesPerSessionQuery query, AnalyticsService analyticsService)
        {
            var selectedPeriodResult = await this.FetchSelectedPeriod(query.ProfileId, query.SelectedPeriod.StartDate, query.SelectedPeriod.EndDate, analyticsService);
            var previousPeriodResult = await this.FetchPreviousPeriod(query.ProfileId, query.PreviousPeriod.StartDate, query.PreviousPeriod.EndDate, analyticsService);
            
            return new PagesPerSessionQueryResult
            {
                PagesPerSession = selectedPeriodResult.Item1, 
                PagesPerSessionPerDays = selectedPeriodResult.Item2, 
                IsBetter = selectedPeriodResult.Item1 >= previousPeriodResult
            };
        }

        private static double ConvertToDouble(string result)
        {
            return double.Parse(result, CultureInfo.InvariantCulture);
        }

        private async Task<Tuple<double, IEnumerable<double>>> FetchSelectedPeriod(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        {
            var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:pageViewsPerSession");
            request.Dimensions = "ga:date";

            var data = await this.ExecuteRequest(request);

            if (data != null && data.Rows != null)
            {
                var pagesPerSessionPerDays = data.Rows.Select(x => ConvertToDouble(x[1]));
                var pagesPerSession = Math.Round(pagesPerSessionPerDays.Average(x => x), 2);

                return new Tuple<double, IEnumerable<double>>(pagesPerSession, pagesPerSessionPerDays);
            }

            return new Tuple<double, IEnumerable<double>>(0.0, Enumerable.Empty<double>());
        }

        private async Task<double> FetchPreviousPeriod(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        {
            var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:pageViewsPerSession");
            request.MaxResults = 1;

            var data = await this.ExecuteRequest(request);

            if (data != null && data.Rows != null)
            {
                return ConvertToDouble(data.Rows[0][0]);
            }

            return 0.0;
        }

        #endregion
    }
}