﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.PagesPerSession
{
    /// <summary>
    /// Pages per sessions query.
    /// </summary>
    public class PagesPerSessionQuery : GoogleAnalyticsQuery
    {
    }
}