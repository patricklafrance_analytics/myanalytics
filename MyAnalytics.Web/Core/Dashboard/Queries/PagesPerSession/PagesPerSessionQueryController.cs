﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.PagesPerSession
{
    /// <summary>
    /// Sessions query controller.
    /// </summary>
    public class PagesPerSessionQueryController : DashboardQueryController<PagesPerSessionQuery, PagesPerSessionQueryResult>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PagesPerSessionQueryController"/> class.
        /// </summary>
        /// <param name="queryRunner">The query runner.</param>
        public PagesPerSessionQueryController(IQueryRunner queryRunner)
            : base(queryRunner)
        {
        }

        #endregion
    }
}