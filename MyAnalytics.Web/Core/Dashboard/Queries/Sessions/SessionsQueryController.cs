﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.Sessions
{
    /// <summary>
    /// Sessions query controller.
    /// </summary>
    public class SessionsQueryController : DashboardQueryController<SessionsQuery, SessionsQueryResult>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionsQueryController"/> class.
        /// </summary>
        /// <param name="queryRunner">The query runner.</param>
        public SessionsQueryController(IQueryRunner queryRunner)
            : base(queryRunner)
        {
        }

        #endregion
    }
}