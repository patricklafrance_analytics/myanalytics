﻿using System;
using System.Collections.Generic;

namespace MyAnalytics.Web.Core.Dashboard.Queries.Sessions
{
    /// <summary>
    /// Sessions query result.
    /// </summary>
    public class SessionsQueryResult
    {
        #region Properties

        /// <summary>
        /// Gets or sets the session count.
        /// </summary>
        public double SessionCount { get; set; }

        /// <summary>
        /// Gets or sets the session count per days.
        /// </summary>
        public IEnumerable<double> SessionCountPerDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is better.
        /// </summary>
        public bool IsBetter { get; set; }

        #endregion
    }
}