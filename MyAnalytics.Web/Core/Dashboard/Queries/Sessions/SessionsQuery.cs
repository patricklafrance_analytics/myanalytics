﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.Sessions
{
    /// <summary>
    /// Sessions query.
    /// </summary>
    public class SessionsQuery : GoogleAnalyticsQuery
    {
    }
}