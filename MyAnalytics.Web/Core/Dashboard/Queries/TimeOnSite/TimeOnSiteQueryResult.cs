﻿using System;
using System.Collections.Generic;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TimeOnSite
{
    /// <summary>
    /// Time on site query result.
    /// </summary>
    public class TimeOnSiteQueryResult
    {
        #region Properties

        /// <summary>
        /// Gets or sets the time on site.
        /// </summary>
        public string TimeOnSite { get; set; }

        /// <summary>
        /// Gets or sets the time on site per days.
        /// </summary>
        public IEnumerable<double> TimeOnSitePerDays { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is better.
        /// </summary>
        public bool IsBetter { get; set; }

        #endregion
    }
}