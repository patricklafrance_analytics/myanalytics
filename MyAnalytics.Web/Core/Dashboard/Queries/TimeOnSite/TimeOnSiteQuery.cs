﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TimeOnSite
{
    /// <summary>
    /// Time on site query.
    /// </summary>
    public class TimeOnSiteQuery : GoogleAnalyticsQuery
    {
    }
}