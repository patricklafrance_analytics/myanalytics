﻿using System;

using MyAnalytics.Web.Core.Queries;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TimeOnSite
{
    /// <summary>
    /// Time on site query controller.
    /// </summary>
    public class TimeOnSiteQueryController : DashboardQueryController<TimeOnSiteQuery, TimeOnSiteQueryResult>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeOnSiteQueryController"/> class.
        /// </summary>
        /// <param name="queryRunner">The query runner.</param>
        public TimeOnSiteQueryController(IQueryRunner queryRunner)
            : base(queryRunner)
        {
        }

        #endregion
    }
}