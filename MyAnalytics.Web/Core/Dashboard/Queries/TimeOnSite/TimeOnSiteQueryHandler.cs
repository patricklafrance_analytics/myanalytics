﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Requests;

using MyAnalytics.Web.Core.Queries;
using MyAnalytics.Web.Google.Analytics;

namespace MyAnalytics.Web.Core.Dashboard.Queries.TimeOnSite
{
    /// <summary>
    /// Handler responsible of running the time on site query against Google analytics.
    /// </summary>
    public class TimeOnSiteQueryHandler : GoogleAnalyticsQueryHandler<TimeOnSiteQuery, TimeOnSiteQueryResult>
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeOnSiteQueryHandler"/> class.
        /// </summary>
        /// <param name="googleAnalyticsServiceProvider">The google analytics service provider.</param>
        public TimeOnSiteQueryHandler(GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider)
            : base(googleAnalyticsServiceProvider)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Handles the specified query.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>A query result.</returns>
        /// <param name="analyticsService">The analytics service.</param>
        /// <returns>A query result.</returns>
        protected override async Task<TimeOnSiteQueryResult> HandleQuery(TimeOnSiteQuery query, AnalyticsService analyticsService)
        {
            var requests = new[]
            {
                this.CreateAverageSessionDurationRequest(query.ProfileId, query.SelectedPeriod.StartDate, query.SelectedPeriod.EndDate, analyticsService),
                this.CreateAverageSessionDurationRequest(query.ProfileId, query.PreviousPeriod.StartDate, query.PreviousPeriod.EndDate, analyticsService)
            };

            var results = await this.ExecuteBatchRequest(requests, query.UserIdentifier, analyticsService);
            if (results != null)
            {
                var selectedPeriodResult = ConvertToDouble(results.ElementAt(0).Rows[0][0]);
                var previousPeriodResult = ConvertToDouble(results.ElementAt(1).Rows[0][0]);

                return new TimeOnSiteQueryResult
                {
                    TimeOnSite = Math.Round(selectedPeriodResult, 2).ToString(CultureInfo.InvariantCulture),
                    TimeOnSitePerDays = new[] { selectedPeriodResult },
                    IsBetter = selectedPeriodResult >= previousPeriodResult
                };
            }

            return new TimeOnSiteQueryResult();
        }

        private static double ConvertToDouble(string result)
        {
            return double.Parse(result, CultureInfo.InvariantCulture);
        }

        private DataResource.GaResource.GetRequest CreateAverageSessionDurationRequest(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        {
            var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:avgSessionDuration");
            request.MaxResults = 1;

            return request;
        }

        //private DataResource.GaResource.GetRequest CreateSelectedPeriodRequest(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        //{
        //    var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:avgSessionDuration");
        //    request.Dimensions = "ga:date";

        //    return request;
        //}

        //private DataResource.GaResource.GetRequest CreatePreviousPeriodRequest(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        //{
        //    var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:avgSessionDuration");
        //    request.MaxResults = 1;

        //    return request;
        //}

        ///// <summary>
        ///// Handles the specified query.
        ///// </summary>
        ///// <param name="query">The query.</param>
        ///// <returns>A query result.</returns>
        ///// <param name="analyticsService">The analytics service.</param>
        ///// <returns>A query result.</returns>
        //protected override async Task<TimeOnSiteQueryResult> HandleQuery(TimeOnSiteQuery query, AnalyticsService analyticsService)
        //{
        //    var selectedPeriodResult = await this.FetchSelectedPeriod(query.ProfileId, query.SelectedPeriod.StartDate, query.SelectedPeriod.EndDate, analyticsService);
        //    var previousPeriodResult = await this.FetchPreviousPeriod(query.ProfileId, query.PreviousPeriod.StartDate, query.PreviousPeriod.EndDate, analyticsService);

        //    return new TimeOnSiteQueryResult
        //    {
        //        TimeOnSite = TimeSpan.FromSeconds(selectedPeriodResult.Item1).ToString(@"m\:ss"), 
        //        TimeOnSitePerDays = selectedPeriodResult.Item2, 
        //        IsBetter = selectedPeriodResult.Item1 >= previousPeriodResult
        //    };
        //}

        //private static double ConvertToDouble(string result)
        //{
        //    return double.Parse(result, CultureInfo.InvariantCulture);
        //}

        //private async Task<Tuple<double, IEnumerable<double>>> FetchSelectedPeriod(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        //{
        //    var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:avgSessionDuration");
        //    request.Dimensions = "ga:date";

        //    var data = await this.ExecuteRequest(request);

        //    if (data != null && data.Rows != null)
        //    {
        //        var timeOnSitePerDays = data.Rows.Select(x => ConvertToDouble(x[1]));
        //        var timeOnSite = Math.Round(timeOnSitePerDays.Average(x => x), 2);

        //        return new Tuple<double, IEnumerable<double>>(timeOnSite, timeOnSitePerDays);
        //    }

        //    return new Tuple<double, IEnumerable<double>>(0.0, Enumerable.Empty<double>());
        //}

        //private async Task<double> FetchPreviousPeriod(string profileId, DateTime startDate, DateTime endDate, AnalyticsService analyticsService)
        //{
        //    var request = analyticsService.Data.Ga.Get(profileId, startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), "ga:avgSessionDuration");
        //    request.MaxResults = 1;

        //    var data = await this.ExecuteRequest(request);

        //    if (data != null && data.Rows != null)
        //    {
        //        return ConvertToDouble(data.Rows[0][0]);
        //    }

        //    return 0.0;
        //}

        #endregion
    }
}