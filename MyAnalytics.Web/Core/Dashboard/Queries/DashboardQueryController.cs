﻿using System;
using System.Threading.Tasks;
using System.Web.Http;

using MyAnalytics.Web.Core.Queries;
using MyAnalytics.Web.Http;
using MyAnalytics.Web.Infrastructure.Security;

namespace MyAnalytics.Web.Core.Dashboard.Queries
{
    /// <summary>
    /// Base query controller.
    /// </summary>
    /// <typeparam name="TQuery">The type of the query.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    public abstract class DashboardQueryController<TQuery, TResult> : ApiController
        where TQuery : GoogleAnalyticsQuery
    {
        #region Fields

        private readonly IQueryRunner _queryRunner;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DashboardQueryController{TQuery,TResult}" /> class.
        /// </summary>
        /// <param name="queryRunner">The query runner.</param>
        protected DashboardQueryController(IQueryRunner queryRunner)
        {
            this._queryRunner = queryRunner;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Execute the query.
        /// </summary>
        /// <param name="profileId">The profile id.</param>
        /// <param name="selectedStartDate">The selected start date.</param>
        /// <param name="selectedEndDate">The selected end date.</param>
        /// <param name="previousStartDate">The previous start date.</param>
        /// <param name="previousEndDate">The previous end date.</param>
        /// <returns>A query result.</returns>
#if !DEBUG
        [CacheOutput(ClientTimeSpan = 30 * (60 * 1000), ServerTimeSpan = 30 * (60 * 1000))]
#endif
        public async Task<IHttpActionResult> Get(string profileId, DateTime? selectedStartDate, DateTime? selectedEndDate, DateTime? previousStartDate, DateTime? previousEndDate)
        {
            var query = Activator.CreateInstance<TQuery>();

            query.UserIdentifier = this.GetUserIdentifier();
            query.SelectedPeriod = new Period(selectedStartDate.Value, selectedEndDate.Value);
            query.PreviousPeriod = new Period(previousStartDate.Value, previousEndDate.Value);

            query.SetProfileId(profileId);

            var result = await this._queryRunner.Run<TQuery, TResult>(query);

            return this.Ok(result);
        }

        private string GetUserIdentifier()
        {
            var authenticationCookie = new FormsAuthenticationCookie(new HttpRequestMessageCookieReader(this.Request));
            var session = authenticationCookie.Session;

            return session.SessionId;
        }

        #endregion
    }
}