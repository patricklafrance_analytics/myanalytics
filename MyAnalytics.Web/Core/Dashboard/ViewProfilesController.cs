﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

using MyAnalytics.Web.Core.Dashboard.Queries;
using MyAnalytics.Web.Google;
using MyAnalytics.Web.Google.Analytics;
using MyAnalytics.Web.Http;
using MyAnalytics.Web.Infrastructure.Security;

using WebApi.OutputCache.V2;

namespace MyAnalytics.Web.Core.Dashboard
{
    /// <summary>
    /// Gets the list of the google analytics profiles available.
    /// </summary>
    public class ViewProfilesController : ApiController
    {
        #region Fields

        private readonly GoogleAnalyticsServiceProvider _googleAnalyticsServiceProvider;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewProfilesController"/> class.
        /// </summary>
        /// <param name="googleAnalyticsServiceProvider">The google analytics service provider.</param>
        public ViewProfilesController(GoogleAnalyticsServiceProvider googleAnalyticsServiceProvider)
        {
            this._googleAnalyticsServiceProvider = googleAnalyticsServiceProvider;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets all the google analytics profiles availables.
        /// </summary>
        /// <returns>A collection.</returns>
        public async Task<IHttpActionResult> Get()
        {
            var authenticationCookie = new FormsAuthenticationCookie(new HttpRequestMessageCookieReader(this.Request));
            var session = authenticationCookie.Session;

            using (var service = await this._googleAnalyticsServiceProvider.GetAsync(session.SessionId))
            {
                var profiles = await service.Management.Profiles.List("~all", "~all").ExecuteAsync(new CancellationTokenSource(GoogleApiConstants.DefaultApiCallCancellationDelay).Token);

                return this.Ok(profiles.Items.Select(x => new
                {
                    AccountId = x.AccountId, 
                    ProfileId = x.Id, 
                    Label = x.Name
                }));
            }
        }

        #endregion
    }
}