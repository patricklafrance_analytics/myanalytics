﻿using System;

using Google.Apis.Analytics.v3.Data;
using Google.Apis.Requests;

namespace MyAnalytics.Web.Core
{
    /// <summary>
    /// Google analytics batch request result.
    /// </summary>
    public class GoogleAnalyticsBatchRequestResult
    {
        #region Properties

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public GaData Data { get; set; }

        /// <summary>
        /// Gets or sets the request error.
        /// </summary>
        public RequestError RequestError { get; set; }

        /// <summary>
        /// Gets or sets the index.
        /// </summary>
        public int Index { get; set; }

        #endregion
    }
}