﻿using System;

using Microsoft.WindowsAzure.Storage.Table;

namespace MyAnalytics.Web.Core
{
    public class AnalyticsData : TableEntity
    {
        #region Properties

        public DateTime Date { get; set; }

        public double PageViewsPerSession { get; set; }

        #endregion
    }
}