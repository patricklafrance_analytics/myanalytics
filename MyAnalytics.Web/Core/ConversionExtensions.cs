﻿using System;
using System.Globalization;

namespace MyAnalytics.Web.Core
{
    public static class ConversionExtensions
    {
        #region Methods

        public static double ToDouble(this string instance)
        {
            return double.Parse(instance, CultureInfo.InvariantCulture);
        }

        public static int ToInt32(this string instance)
        {
            return int.Parse(instance, CultureInfo.InvariantCulture);
        }

        #endregion
    }
}