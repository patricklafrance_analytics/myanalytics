﻿"use strict";

var sequence = require("run-sequence");
var gulp = require("gulp");

// ---------------------------------

require("require-dir")("./Gulp", { 
    recurse: true 
});

// ---------------------------------

gulp.task("dev", function(callback) {
    sequence("quality-tools-dev", callback);
});

gulp.task("default", ["dev"], function() {
});